﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace HardwareTester
{
    public class RuptelaJson
    {
        #region Properties
        private int num_records;
        private string IMEI;
        private string DeviceType;
        //GPS Element
        private DateTime TimeStamp;
        private int Priority;
        private double Latitude;
        private double Longitude;
        private int Altitude;
        private double Angle;
        private int numSat;
        private int Speed; //km/h
        private double HDop;
        //IO Element
        private int IO_Event_Gen_ID; //event generator ID
        private int num_IO;
        private int num_1_byte;
        private int num_2_byte;
        private int num_4_byte;
        private int num_8_byte;
        #endregion
        public void CreateSendMsg(string FM_Msg)
        {
            try
            {
                int i, n;
                int x;
                var sb = new StringBuilder("");

                var devType = "Ruptela";
                //ignition and driver id
                var ign_stat = "0";
                var drv_id = "";
                var _id = "0";
                var _id_2 = "";
                var packet_lengt = Convert.ToInt32(FM_Msg.Substring(0, 4), 16);
                var devID = Convert.ToInt64(FM_Msg.Substring(4, 16), 16);
                var Command = FM_Msg.Substring(20, 2);
                var RecordLeftFlag = Convert.ToInt32(FM_Msg.Substring(22, 2), 16);
                var NiumberOfRecords = Convert.ToInt32(FM_Msg.Substring(24, 2), 16);
                var orig_rawdata = FM_Msg;
                FM_Msg = FM_Msg.Remove(0, 26);
                var lstOfCommonObj = new List<CommonObj>();
                for (n = 0; n < NiumberOfRecords; n++)
                {
                    x = 0;
                    var _CommonObj = new CommonObj();
                    var pos = new Position
                    {
                        Id= devID,
                        DeviceTime = TimeStamp = Helper.UnixTimeStampToDateTimeRuptela(Convert.ToInt64(FM_Msg.Substring(0, 8), 16)).AddMilliseconds(Convert.ToInt32(FM_Msg.Substring(8, 2), 16)),
                        Priority = Priority = Convert.ToInt32(FM_Msg.Substring(10, 2), 16),
                        Protocol = "Ruptela",
                        Longitude = Longitude = Convert.ToInt32(FM_Msg.Substring(12, 8), 16) * 0.0000001,
                        Latitude = Latitude = Convert.ToInt32(FM_Msg.Substring(20, 8), 16) * 0.0000001,
                        Altitude = Convert.ToInt32(FM_Msg.Substring(28, 4), 16),
                        Course = Convert.ToInt32(FM_Msg.Substring(32, 4), 16) * 0.01,
                        Speed = Convert.ToInt32(FM_Msg.Substring(38, 4), 16),
                    };
                    var attrib = new Attributes
                    {
                        sat= Convert.ToInt32(FM_Msg.Substring(36, 2), 16),
                        hdop= Convert.ToInt32(FM_Msg.Substring(42, 2), 16) * 0.1,
                        Event= Convert.ToInt32(FM_Msg.Substring(44, 2), 16).ToString(),
                    };

                   

                    num_1_byte = Convert.ToInt32(FM_Msg.Substring(46, 2), 16); //
                    x = 48;


                    //1 byte elements
                    sb.Append("{");
                    try
                    {
                        var IoId = 0;
                        
                       
                        for (i = 0; i < num_1_byte; i++)
                        {
                            IoId = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);
                            x += 2;
                            sb.Append($"\"io{IoId}\":{Convert.ToInt32(FM_Msg.Substring(x, 2), 16)},");
                             x += 2;

                        }

                        // 2 byte elements
                        num_2_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);

                        for (i = 0; i < num_2_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }
                            IoId = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);
                            x += 2;
                            sb.Append($"\"io{IoId}\":{Convert.ToInt32(FM_Msg.Substring(x, 4), 16)},");
                            x += 4;
                           
                        }
                        if (num_2_byte == 0)
                            x += 2;

                        //4 byte elements
                        num_4_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);

                        for (i = 0; i < num_4_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }

                            IoId = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);
                            x += 2;
                            sb.Append($"\"io{IoId}\":{Convert.ToInt32(FM_Msg.Substring(x, 8), 16)},");
                            x += 8;

                        }
                        if (num_4_byte == 0)
                            x += 2;

                        //8 byte  elements
                        //8 byte data length
                        num_8_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);
                        for (i = 0; i < num_8_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }
                            IoId = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);
                            x += 2;
                            sb.Append($"\"io{IoId}\":{FM_Msg.Substring(x, 8)},");
                            //handle driver id
                            /*
                            if (_id_2 == "78" && FM_Msg.Substring(x, 8) != "00000000")
                            {
                                //var drv_data = new cDriverLog() { DeviceID = devID, ign_stat = int.Parse(ign_stat), tag_id = FM_Msg.Substring(x, 8), tag_time = DateTime.Now };
                                //_list.Add(drv_data);

                                Dictionary<String, String> data = new Dictionary<String, String>();
                                data.Add("device_id", devID);
                                data.Add("ign_status", ign_stat);
                                data.Add("tag_time", DateTime.Now.ToString());
                                data.Add("tag_id", FM_Msg.Substring(x, 8));

                                try
                                {
                                    s_db.Insert("driver_cache", data);
                                }
                                catch (Exception sqlite_ex)
                                {

                                }

                            }
                            if (_id_2 == "78" && FM_Msg.Substring(x, 8) == "00000000")
                            {
                                //check driver list for driver data and append to message..

                                DataTable dt;
                                string query = "select * from driver_cache where device_id='" + devID + "'";

                                dt = s_db.GetDataTable(query);
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow r in dt.Rows)
                                    {
                                        drv_id = r["tag_id"].ToString();
                                    }
                                    //update ignition status
                                    Dictionary<String, String> data = new Dictionary<String, String>();
                                    data.Add("ign_status", ign_stat);

                                    s_db.Update("driver_cache", data, "device_id='" + devID + "'");
                                }

                                x_num_8_byte.InnerText = drv_id;

                            } */
                            x += 16;

                            
                        }
                        
                        sb.Append("}");                   
                        if (num_8_byte == 0)
                            x += 2;
                    }
                    catch (Exception e)
                    {
                    }


                    _CommonObj.Position = pos;
                    //we have the xml now, convert to string
                    var xmlOutput = sb.ToString().Replace(",}", "}");
                    var att = JsonConvert.DeserializeObject<Attributes>(xmlOutput);
                    att.sat = attrib.sat;
                    att.Event = attrib.Event;
                    att.hdop = attrib.hdop;
                    att.di1 = att.io2;
                    att.di2 = att.io3;
                    att.di3 = att.io4;
                    att.di4 = att.io5;
                    att.ignition = Convert.ToBoolean(att.io5);
                    _CommonObj.Attributes = att;
                    xmlOutput = JsonConvert.SerializeObject(_CommonObj);
                    FM_Msg = FM_Msg.Remove(0, x);
                    //Console.WriteLine(xmlOutput);
                    Console.WriteLine("..................................................................................................................................");
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
