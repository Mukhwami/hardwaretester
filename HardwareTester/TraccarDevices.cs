﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class TraccarDevices
    {
        public int Id { get; set; }
        public object Attributes { get; set; }
        public string Name { get; set; }
        public string UniqueId { get; set; }
        public long DeviceId { get; set; }
        public string Status { get; set; }
        
        public override string ToString()
        {
            return string.Format("{0},{1}, {2}",Id, Name.ToUpper(), Status);
        }

    }
    public class DeviceName
    {
        public int deviceId { get; set; }
        public long totalDistance { set; get; }
        public long hours { set; get; }
        public override string ToString()
        {
            return string.Format("{0}", deviceId);
        }
    }
    public class Command
    {
        public string data { get; set; }
        public int? frequency { get; set; }
    }

    public class GprsCommand
    {
        public Command attributes { get; set; }
        public int id { get; set; }
        public string description { get; set; }
        public int deviceId { get; set; }
        public bool textChannel { get; set; }
        public string type { get; set; }
    }
    public enum CommandType
    {
        custom,
        deviceIdentification,
        positionSingle,
        positionPeriodic,
        positionStop,
        engineStop,
        engineResume,
        alarmArm,
        alarmDisarm,
        setTimezone,
        requestPhoto,
        rebootDevice,
        sendSms,
        sendUssd,
        sosNumber,
        silenceTime,
        setPhonebook,
        voiceMessage,
        outputControl,
        voiceMonitoring,
        setAgps,
        setIndicator,
        configuration,
        getVersion,
        firmwareUpdate,
        setConnection,
        setOdometer,
        getModemStatus,
        getDeviceStatus,
        modePowerSaving,
        modeDeepSleep,
        movementAlarm,
        alarmBattery,
        alarmSos,
        alarmRemove,
        alarmClock,
        alarmSpeed,
        alarmFall,
        alarmVibration,
        uniqueId,
        frequency,
        timezone,
        devicePassword,
        radius,
        message,
        enable,
        data,
        index,
        phone,
        server,
        port
    }
}
