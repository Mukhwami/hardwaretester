﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HardwareTester
{
    public class Ruptela
    {
        #region Properties
        private int num_records;
        private string IMEI;
        private string DeviceType;
        //GPS Element
        private DateTime TimeStamp;
        private int Priority;
        private double Latitude;
        private double Longitude;
        private int Altitude;
        private double Angle;
        private int numSat;
        private int Speed; //km/h
        private double HDop;
        //IO Element
        private int IO_Event_Gen_ID; //event generator ID
        private int num_IO;
        private int num_1_byte;
        private int num_2_byte;
        private int num_4_byte;
        private int num_8_byte;
        #endregion
        public void CreateSendMsg(string FM_Msg)
        {
            try
            {
                int i, n;
                int x;
                var sb = new StringBuilder("");

                var devType = "Ruptela";
                //ignition and driver id
                var ign_stat = "0";
                var drv_id = "";
                var _id = "0";
                var _id_2 = "";
                var packet_lengt = Convert.ToInt32(FM_Msg.Substring(0, 4), 16);
                var devID = Convert.ToUInt64(FM_Msg.Substring(4, 16), 16).ToString();
                var Command = FM_Msg.Substring(20, 2);
                var RecordLeftFlag = Convert.ToInt32(FM_Msg.Substring(22, 2), 16);
                var NiumberOfRecords = Convert.ToInt32(FM_Msg.Substring(24, 2), 16);
                var orig_rawdata = FM_Msg;
                FM_Msg = FM_Msg.Remove(0, 26);

                for (n = 0; n < NiumberOfRecords; n++)
                {
                    x = 0;
                   
                    TimeStamp = Helper.UnixTimeStampToDateTimeRuptela(Convert.ToInt64(FM_Msg.Substring(0,8), 16));
                    TimeStamp.AddMilliseconds(Convert.ToInt32(FM_Msg.Substring(8, 2), 16));
                    Priority = Convert.ToInt32(FM_Msg.Substring(10, 2), 16);
                    Longitude = Convert.ToInt32(FM_Msg.Substring(12, 8), 16) * 0.0000001;
                    Latitude = Convert.ToInt32(FM_Msg.Substring(20, 8), 16) * 0.0000001;
                    Altitude = Convert.ToInt32(FM_Msg.Substring(28, 4), 16);
                    Angle = Convert.ToInt32(FM_Msg.Substring(32, 4), 16) * 0.01;
                    numSat = Convert.ToInt32(FM_Msg.Substring(36, 2), 16);
                    Speed = Convert.ToInt32(FM_Msg.Substring(38, 4), 16);
                    HDop=Convert.ToInt32(FM_Msg.Substring(42, 2), 16) * 0.1;
                    IO_Event_Gen_ID = Convert.ToInt32(FM_Msg.Substring(44, 2), 16);
                    num_1_byte = Convert.ToInt32(FM_Msg.Substring(46, 2), 16); //
                     x = 48;

                    //build xml
                    var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
                    var dec = doc.CreateXmlDeclaration("1.0", null, null);
                    doc.AppendChild(dec);

                    // Create the root element
                    var root = doc.CreateElement("RP_Message");
                    doc.AppendChild(root);

                    var deviceInfo = doc.CreateElement("DeviceInfo");
                    root.AppendChild(deviceInfo);

                    var deviceID = doc.CreateElement("DeviceID");
                    deviceID.InnerText = devID;
                    deviceInfo.AppendChild(deviceID);

                    var deviceType = doc.CreateElement("DeviceType");
                    deviceType.InnerText = devType;
                    deviceInfo.AppendChild(deviceType);

                    var gpsElement = doc.CreateElement("GPS_Element");
                    root.AppendChild(gpsElement);

                    var xtimestamp = doc.CreateElement("TimeStamp");
                    xtimestamp.InnerText = TimeStamp.ToShortDateString() + " " + TimeStamp.ToLongTimeString();
                    gpsElement.AppendChild(xtimestamp);

                    var xpriority = doc.CreateElement("Priority");
                    xpriority.InnerText = Priority.ToString();
                    gpsElement.AppendChild(xpriority);

                    var xlatitude = doc.CreateElement("Latitude");
                    xlatitude.InnerText = Latitude.ToString();
                    gpsElement.AppendChild(xlatitude);

                    var xlongitude = doc.CreateElement("Longitude");
                    xlongitude.InnerText = Longitude.ToString();
                    gpsElement.AppendChild(xlongitude);

                    var xaltitude = doc.CreateElement("Altitude");
                    xaltitude.InnerText = Altitude.ToString();
                    gpsElement.AppendChild(xaltitude);

                    var xangle = doc.CreateElement("Angle");
                    xangle.InnerText = Angle.ToString();
                    gpsElement.AppendChild(xangle);

                    var xnumsat = doc.CreateElement("Satellites");
                    xnumsat.InnerText = numSat.ToString();
                    gpsElement.AppendChild(xnumsat);

                    var xspeed = doc.CreateElement("Speed");
                    xspeed.InnerText = Speed.ToString();
                    gpsElement.AppendChild(xspeed);

                    //IO element
                    var ioElement = doc.CreateElement("IO_Element");
                    root.AppendChild(ioElement);

                    //event generator ID
                    var xio_event_genid = doc.CreateElement("IO_EventGen_ID");
                    xio_event_genid.InnerText = IO_Event_Gen_ID.ToString();
                    ioElement.AppendChild(xio_event_genid);

                    //1 byte elements

                    try
                    {

                        for (i = 0; i < num_1_byte; i++)
                        {
                            var x_num_1_byte = doc.CreateElement("IO_1byte");
                            x_num_1_byte.SetAttribute("ID", Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString());
                            if (Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString() == "5")
                                _id = Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString();
                            x += 2;
                            x_num_1_byte.InnerText = Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString();
                            if (_id == "5")
                                ign_stat = Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString();
                            x += 2;

                            ioElement.AppendChild(x_num_1_byte);
                        }

                        // 2 byte elements
                        num_2_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);

                        for (i = 0; i < num_2_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }
                            var x_num_2_byte = doc.CreateElement("IO_2byte");
                            x_num_2_byte.SetAttribute("ID", Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString());
                            x += 2;
                            x_num_2_byte.InnerText = Convert.ToInt32(FM_Msg.Substring(x, 4), 16).ToString();
                            x += 4;

                            ioElement.AppendChild(x_num_2_byte);
                        }
                        if (num_2_byte == 0)
                            x += 2;

                        //4 byte elements
                        num_4_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);

                        for (i = 0; i < num_4_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }
                            var x_num_4_byte = doc.CreateElement("IO_4byte");
                            x_num_4_byte.SetAttribute("ID", Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString());
                            x += 2;
                            x_num_4_byte.InnerText = Convert.ToInt32(FM_Msg.Substring(x, 8), 16).ToString();
                            x += 8;

                            ioElement.AppendChild(x_num_4_byte);
                        }
                        if (num_4_byte == 0)
                            x += 2;

                        //8 byte  elements
                        //8 byte data length
                        num_8_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);
                        for (i = 0; i < num_8_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }
                            var x_num_8_byte = doc.CreateElement("IO_8byte");
                            x_num_8_byte.SetAttribute("ID", Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString());
                            _id_2 = Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString();
                            x += 2;
                            x_num_8_byte.InnerText = FM_Msg.Substring(x, 8);
                            //handle driver id
                            /*
                            if (_id_2 == "78" && FM_Msg.Substring(x, 8) != "00000000")
                            {
                                //var drv_data = new cDriverLog() { DeviceID = devID, ign_stat = int.Parse(ign_stat), tag_id = FM_Msg.Substring(x, 8), tag_time = DateTime.Now };
                                //_list.Add(drv_data);

                                Dictionary<String, String> data = new Dictionary<String, String>();
                                data.Add("device_id", devID);
                                data.Add("ign_status", ign_stat);
                                data.Add("tag_time", DateTime.Now.ToString());
                                data.Add("tag_id", FM_Msg.Substring(x, 8));

                                try
                                {
                                    s_db.Insert("driver_cache", data);
                                }
                                catch (Exception sqlite_ex)
                                {

                                }

                            }
                            if (_id_2 == "78" && FM_Msg.Substring(x, 8) == "00000000")
                            {
                                //check driver list for driver data and append to message..

                                DataTable dt;
                                string query = "select * from driver_cache where device_id='" + devID + "'";

                                dt = s_db.GetDataTable(query);
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow r in dt.Rows)
                                    {
                                        drv_id = r["tag_id"].ToString();
                                    }
                                    //update ignition status
                                    Dictionary<String, String> data = new Dictionary<String, String>();
                                    data.Add("ign_status", ign_stat);

                                    s_db.Update("driver_cache", data, "device_id='" + devID + "'");
                                }

                                x_num_8_byte.InnerText = drv_id;

                            } */
                            x += 16;

                            ioElement.AppendChild(x_num_8_byte);
                        }

                        if (num_8_byte == 0)
                            x += 2;
                    }
                    catch (Exception e)
                    {
                    }

                    //road information data
                    var iMax_speed = 0;
                    var road_info_alertid = "";
                    var bRoadInfoID = false;

                    ////create the position string
                    var txtpos = string.Empty;

                    //ADD to the xml
                    var xTextPos = doc.CreateElement("Ignition_Status");
                    xTextPos.InnerText = ign_stat;
                    root.AppendChild(xTextPos);

                    var xRoadSpeed = doc.CreateElement("max_road_speed");
                    xRoadSpeed.InnerText = iMax_speed.ToString();
                    root.AppendChild(xRoadSpeed);



                    var xRawData = doc.CreateElement("raw_data");
                    xRawData.InnerText = orig_rawdata;
                    root.AppendChild(xRawData);


                    //we have the xml now, convert to string
                    var xmlOutput = doc.OuterXml;
                    FM_Msg = FM_Msg.Remove(0, x);
                    Console.WriteLine(xmlOutput);
                    Console.WriteLine("..................................................................................................................................");
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
