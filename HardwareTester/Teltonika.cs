﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HardwareTester
{
    public class Teltonika
    {
        #region Properties
        private int num_records;
        private string IMEI;
        private string DeviceType;
        //GPS Element
        private DateTime TimeStamp;
        private int Priority;
        private double Latitude;
        private double Longitude;
        private int Altitude;
        private int Angle;
        private int numSat;
        private int Speed; //km/h

        //IO Element
        private int IO_Event_Gen_ID; //event generator ID
        private int num_IO;
        private int num_1_byte;
        private int num_2_byte;
        private int num_4_byte;
        private int num_8_byte;
        #endregion
        public void CreateSendMsg(string FM_Msg, string devID)
        {
            try
            {
                int i, n;
                int x;
                var sb = new StringBuilder("");


                //ignition and driver id
                var ign_stat = "0";
                var drv_id = "";
                var _id = "0";
                var _id_2 = "";

                var orig_rawdata = FM_Msg;

                var devType = FM_Msg.Substring(0, 2);

                num_records = int.Parse(FM_Msg.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
                FM_Msg = FM_Msg.Remove(0, 4);

                for (n = 0; n < num_records; n++)
                {
                    x = 0;
                    TimeStamp = Helper.UnixTimeStampToDateTime(Convert.ToInt64(FM_Msg.Substring(0, 16), 16));
                    Priority = Convert.ToInt32(FM_Msg.Substring(16, 2), 16);
                    Longitude = Convert.ToInt32(FM_Msg.Substring(18, 8), 16) * 0.0000001;
                    Latitude = Convert.ToInt32(FM_Msg.Substring(26, 8), 16) * 0.0000001;
                    Altitude = Convert.ToInt32(FM_Msg.Substring(34, 4), 16);
                    Angle = Convert.ToInt32(FM_Msg.Substring(38, 4), 16);
                    numSat = Convert.ToInt32(FM_Msg.Substring(42, 2), 16);
                    Speed = Convert.ToInt32(FM_Msg.Substring(44, 4), 16);

                    //noma kutoka hapa
                    IO_Event_Gen_ID = Convert.ToInt32(FM_Msg.Substring(48, 2), 16);
                    num_IO = Convert.ToInt32(FM_Msg.Substring(50, 2), 16); //
                    num_1_byte = Convert.ToInt32(FM_Msg.Substring(52, 2), 16);

                    x = 54;

                    //build xml
                    var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
                    var dec = doc.CreateXmlDeclaration("1.0", null, null);
                    doc.AppendChild(dec);

                    // Create the root element
                    var root = doc.CreateElement("FM_Message");
                    doc.AppendChild(root);

                    var deviceInfo = doc.CreateElement("DeviceInfo");
                    root.AppendChild(deviceInfo);

                    var deviceID = doc.CreateElement("DeviceID");
                    deviceID.InnerText = devID;
                    deviceInfo.AppendChild(deviceID);

                    var deviceType = doc.CreateElement("DeviceType");
                    deviceType.InnerText = devType;
                    deviceInfo.AppendChild(deviceType);

                    var gpsElement = doc.CreateElement("GPS_Element");
                    root.AppendChild(gpsElement);

                    var xtimestamp = doc.CreateElement("TimeStamp");
                    xtimestamp.InnerText = TimeStamp.ToShortDateString() + " " + TimeStamp.ToLongTimeString();
                    gpsElement.AppendChild(xtimestamp);

                    var xpriority = doc.CreateElement("Priority");
                    xpriority.InnerText = Priority.ToString();
                    gpsElement.AppendChild(xpriority);

                    var xlatitude = doc.CreateElement("Latitude");
                    xlatitude.InnerText = Latitude.ToString();
                    gpsElement.AppendChild(xlatitude);

                    var xlongitude = doc.CreateElement("Longitude");
                    xlongitude.InnerText = Longitude.ToString();
                    gpsElement.AppendChild(xlongitude);

                    var xaltitude = doc.CreateElement("Altitude");
                    xaltitude.InnerText = Altitude.ToString();
                    gpsElement.AppendChild(xaltitude);

                    var xangle = doc.CreateElement("Angle");
                    xangle.InnerText = Angle.ToString();
                    gpsElement.AppendChild(xangle);

                    var xnumsat = doc.CreateElement("Satellites");
                    xnumsat.InnerText = numSat.ToString();
                    gpsElement.AppendChild(xnumsat);

                    var xspeed = doc.CreateElement("Speed");
                    xspeed.InnerText = Speed.ToString();
                    gpsElement.AppendChild(xspeed);

                    //IO element
                    var ioElement = doc.CreateElement("IO_Element");
                    root.AppendChild(ioElement);

                    //event generator ID
                    var xio_event_genid = doc.CreateElement("IO_EventGen_ID");
                    xio_event_genid.InnerText = IO_Event_Gen_ID.ToString();
                    ioElement.AppendChild(xio_event_genid);

                    //1 byte elements

                    try
                    {

                        for (i = 0; i < num_1_byte; i++)
                        {
                            var x_num_1_byte = doc.CreateElement("IO_1byte");
                            x_num_1_byte.SetAttribute("ID", Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString());
                            if (Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString() == "1")
                                _id = Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString();
                            x += 2;
                            x_num_1_byte.InnerText = Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString();
                            if (_id == "1")
                                ign_stat = Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString();
                            x += 2;

                            ioElement.AppendChild(x_num_1_byte);
                        }

                        // 2 byte elements
                        num_2_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);

                        for (i = 0; i < num_2_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }
                            var x_num_2_byte = doc.CreateElement("IO_2byte");
                            x_num_2_byte.SetAttribute("ID", Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString());
                            x += 2;
                            x_num_2_byte.InnerText = Convert.ToInt32(FM_Msg.Substring(x, 4), 16).ToString();
                            x += 4;

                            ioElement.AppendChild(x_num_2_byte);
                        }
                        if (num_2_byte == 0)
                            x += 2;

                        //4 byte elements
                        num_4_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);

                        for (i = 0; i < num_4_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }
                            var x_num_4_byte = doc.CreateElement("IO_4byte");
                            x_num_4_byte.SetAttribute("ID", Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString());
                            x += 2;
                            x_num_4_byte.InnerText = Convert.ToInt32(FM_Msg.Substring(x, 8), 16).ToString();
                            x += 8;

                            ioElement.AppendChild(x_num_4_byte);
                        }
                        if (num_4_byte == 0)
                            x += 2;

                        //8 byte  elements
                        //8 byte data length
                        num_8_byte = Convert.ToInt32(FM_Msg.Substring(x, 2), 16);
                        for (i = 0; i < num_8_byte; i++)
                        {
                            if (i == 0)
                            {
                                x += 2;
                            }
                            var x_num_8_byte = doc.CreateElement("IO_8byte");
                            x_num_8_byte.SetAttribute("ID", Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString());
                            _id_2 = Convert.ToInt32(FM_Msg.Substring(x, 2), 16).ToString();
                            x += 2;
                            x_num_8_byte.InnerText = FM_Msg.Substring(x, 8);
                            //handle driver id
                            /*
                            if (_id_2 == "78" && FM_Msg.Substring(x, 8) != "00000000")
                            {
                                //var drv_data = new cDriverLog() { DeviceID = devID, ign_stat = int.Parse(ign_stat), tag_id = FM_Msg.Substring(x, 8), tag_time = DateTime.Now };
                                //_list.Add(drv_data);

                                Dictionary<String, String> data = new Dictionary<String, String>();
                                data.Add("device_id", devID);
                                data.Add("ign_status", ign_stat);
                                data.Add("tag_time", DateTime.Now.ToString());
                                data.Add("tag_id", FM_Msg.Substring(x, 8));

                                try
                                {
                                    s_db.Insert("driver_cache", data);
                                }
                                catch (Exception sqlite_ex)
                                {

                                }

                            }
                            if (_id_2 == "78" && FM_Msg.Substring(x, 8) == "00000000")
                            {
                                //check driver list for driver data and append to message..

                                DataTable dt;
                                string query = "select * from driver_cache where device_id='" + devID + "'";

                                dt = s_db.GetDataTable(query);
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow r in dt.Rows)
                                    {
                                        drv_id = r["tag_id"].ToString();
                                    }
                                    //update ignition status
                                    Dictionary<String, String> data = new Dictionary<String, String>();
                                    data.Add("ign_status", ign_stat);

                                    s_db.Update("driver_cache", data, "device_id='" + devID + "'");
                                }

                                x_num_8_byte.InnerText = drv_id;

                            } */
                            x += 16;

                            ioElement.AppendChild(x_num_8_byte);
                        }

                        if (num_8_byte == 0)
                            x += 2;
                    }
                    catch (Exception e)
                    {
                    }

                   
                    var xRawData = doc.CreateElement("raw_data");
                    xRawData.InnerText = orig_rawdata;
                    root.AppendChild(xRawData);


                    //we have the xml now, convert to string
                    var xmlOutput = doc.OuterXml;
                    FM_Msg = FM_Msg.Remove(0, x);
                    Console.WriteLine(xmlOutput);
                    Console.WriteLine("..................................................................................................................................");
                }
            }
            catch (Exception e)
            {

            }
        }

        public void createSendMsg_GH300(string data, string devID)
        {
            int i, n;
            int x;
            var global_mask = "";
            var gps_mask = "";

            var codec_id = string.Empty;
            var num_records = 0;
            var num_1_byte = 0;
            var num_2_byte = 0;
            var num_4_byte = 0;
            var sTimeStamp = string.Empty;
            var sPriority = string.Empty;
            float f_lat = 0;
            float f_lng = 0;
            var alt = 0;
            var angle = 0;
            var speed = 0;
            var sat = 0;
            var LAC = string.Empty;
            var cellID = string.Empty;
            var sig_strength = 0;
            var op_code = 0;


            var sb = new StringBuilder("");
            //loop through the records
            codec_id = data.Substring(0, 2);
            num_records = Convert.ToInt32(data.Substring(2, 2), 16);

            data = data.Remove(0, 4);

            for (n = 0; n < num_records; n++)
            {
                x = 0;
                var binaryval = "";
                binaryval = Convert.ToString(Convert.ToInt32(data.Substring(0, 8), 16), 2).PadLeft(32, '0');
                sTimeStamp = Helper.TimeStampToDateTime(Convert.ToInt64(binaryval.Substring(2, 30), 2)).ToString();
                sPriority = binaryval.Substring(0, 2);

                global_mask = Convert.ToString(Convert.ToInt32(data.Substring(8, 2), 16), 2).PadRight(4, '0');
                gps_mask = Helper.Reverse(Convert.ToString(Convert.ToInt32(data.Substring(10, 2), 16), 2).PadLeft(8, '0'));

                x = 12;
                //latitude
                if (global_mask.Substring(0, 1) == "1") //gps elements available
                {
                    if (gps_mask.Substring(0, 1) == "1")
                    {
                        f_lat = Helper.GetFloatIEE754(Helper.hexStringToByteArray(data.Substring(x, 8)));
                        x = x + 8;
                    }
                    if (gps_mask.Substring(0, 1) == "1")
                    {
                        f_lng = Helper.GetFloatIEE754(Helper.hexStringToByteArray(data.Substring(x, 8)));
                        x = x + 8;
                    }
                    if (gps_mask.Substring(1, 1) == "1")
                    {
                        alt = Convert.ToInt32(data.Substring(x, 4), 16);
                        x += 4;
                    }
                    if (gps_mask.Substring(2, 1) == "1")
                    {
                        angle = Convert.ToInt32(data.Substring(x, 2), 16) * 360 / 256; //x=32
                        x += 2;
                    }
                    if (gps_mask.Substring(3, 1) == "1")
                    {
                        speed = Convert.ToInt32(data.Substring(x, 2), 16);
                        x += 2;
                    }
                    if (gps_mask.Substring(4, 1) == "1")
                    {
                        sat = Convert.ToInt32(data.Substring(x, 2), 16); //x=36
                        x += 2;
                    }
                    if (gps_mask.Substring(5, 1) == "1")
                    {
                        LAC = data.Substring(x, 4);
                        x += 4;
                        cellID = data.Substring(x, 4);
                        x += 4;
                    }
                    if (gps_mask.Substring(6, 1) == "1")
                    {
                        sig_strength = Convert.ToInt32(data.Substring(x, 2), 16);
                        x += 2;
                    }
                    if (gps_mask.Substring(7, 1) == "1")
                    {
                        op_code = Convert.ToInt32(data.Substring(x, 8), 16);
                        x += 8;
                    }
                }
                //create the xml
                var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
                var dec = doc.CreateXmlDeclaration("1.0", null, null);
                doc.AppendChild(dec);

                // Create the root element
                var root = doc.CreateElement("GH300_Message");
                doc.AppendChild(root);

                var deviceInfo = doc.CreateElement("DeviceInfo");
                root.AppendChild(deviceInfo);

                var deviceID = doc.CreateElement("DeviceID");
                deviceID.InnerText = devID;
                deviceInfo.AppendChild(deviceID);

                var deviceType = doc.CreateElement("DeviceType");
                deviceType.InnerText = codec_id;
                deviceInfo.AppendChild(deviceType);

                var gpsElement = doc.CreateElement("GPS_Element");
                root.AppendChild(gpsElement);

                var xtimestamp = doc.CreateElement("TimeStamp");
                xtimestamp.InnerText = sTimeStamp;
                gpsElement.AppendChild(xtimestamp);

                var xpriority = doc.CreateElement("Priority");
                xpriority.InnerText = sPriority;
                gpsElement.AppendChild(xpriority);

                var xlatitude = doc.CreateElement("Latitude");
                xlatitude.InnerText = f_lat.ToString();
                gpsElement.AppendChild(xlatitude);

                var xlongitude = doc.CreateElement("Longitude");
                xlongitude.InnerText = f_lng.ToString();
                gpsElement.AppendChild(xlongitude);

                var xaltitude = doc.CreateElement("Altitude");
                xaltitude.InnerText = alt.ToString(); ;
                gpsElement.AppendChild(xaltitude);

                var xangle = doc.CreateElement("Angle");
                xangle.InnerText = angle.ToString(); ;
                gpsElement.AppendChild(xangle);

                var xnumsat = doc.CreateElement("Satellites");
                xnumsat.InnerText = sat.ToString(); ;
                gpsElement.AppendChild(xnumsat);

                var xspeed = doc.CreateElement("Speed");
                xspeed.InnerText = speed.ToString();
                gpsElement.AppendChild(xspeed);

                var xGSM_Data = doc.CreateElement("GSM_Data");
                root.AppendChild(xGSM_Data);

                var xLAC = doc.CreateElement("LAC");
                xLAC.InnerText = LAC;
                xGSM_Data.AppendChild(xLAC);

                var xCellID = doc.CreateElement("CellID");
                xCellID.InnerText = cellID;
                xGSM_Data.AppendChild(xCellID);

                var xSigStrength = doc.CreateElement("Sig_strength");
                xSigStrength.InnerText = sig_strength.ToString();
                xGSM_Data.AppendChild(xSigStrength);



                //handle IO ... check each element in IO mask
                //IO element
                var ioElement = doc.CreateElement("IO_Element");
                root.AppendChild(ioElement);

                //1 byte elements
                if (global_mask.Substring(1, 1) == "1")
                {
                    num_1_byte = Convert.ToInt32(data.Substring(x, 2), 16);

                    x = x + 2;
                    for (i = 0; i < num_1_byte; i++)
                    {
                        var x_num_1_byte = doc.CreateElement("IO_1byte");
                        x_num_1_byte.SetAttribute("ID", Convert.ToInt32(data.Substring(x, 2), 16).ToString());
                        x += 2;
                        x_num_1_byte.InnerText = Convert.ToInt32(data.Substring(x, 2), 16).ToString();
                        ioElement.AppendChild(x_num_1_byte);
                        x += 2;
                        //check for ignition stat

                    }
                }

                //two-byte elements
                if (global_mask.Substring(2, 1) == "1")
                {
                    num_2_byte = Convert.ToInt32(data.Substring(x, 2), 16);
                    for (i = 0; i < num_2_byte; i++)
                    {
                        if (i == 0)
                        {
                            x += 2;
                        }
                        var x_num_2_byte = doc.CreateElement("IO_2byte");
                        x_num_2_byte.SetAttribute("ID", Convert.ToInt32(data.Substring(x, 2), 16).ToString());
                        x += 2;
                        x_num_2_byte.InnerText = Convert.ToInt32(data.Substring(x, 4), 16).ToString();
                        ioElement.AppendChild(x_num_2_byte);
                        x += 4;

                    }
                }

                //four-byte elements
                if (global_mask.Substring(3, 1) == "1")
                {
                    num_4_byte = Convert.ToInt32(data.Substring(x, 2), 16);
                    for (i = 0; i < num_4_byte; i++)
                    {
                        if (i == 0)
                        {
                            x += 2;
                        }
                        var x_num_4_byte = doc.CreateElement("IO_4byte");
                        x_num_4_byte.SetAttribute("ID", Convert.ToInt32(data.Substring(x, 2), 16).ToString());
                        x += 2;
                        x_num_4_byte.InnerText = Convert.ToInt32(data.Substring(x, 8), 16).ToString();
                        ioElement.AppendChild(x_num_4_byte);
                        x += 8;
                    }
                }
                var xmlOutput = doc.OuterXml;

                data = data.Remove(0, x);
            }
        }

    }
}
