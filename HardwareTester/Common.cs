﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace HardwareTester
{
    public class Common
    {
        public static decimal ConvertHexToDecimal(string s)
        {

            long n = Int64.Parse(s, NumberStyles.HexNumber);

            return n;
        }

        public static string ImmersedString(string rawstring)
        {
            var result = "";

            var indexes = rawstring.Split('-');
            result = indexes[7] + "" + indexes[6] + "" + indexes[5] + "" + indexes[4] + "" + indexes[3] + "" + indexes[2] + "" + indexes[1] + "" + indexes[0];

            return result;
        }
        public static string ConvertBytesToString(Byte[] bytes)
        {
            return BitConverter.ToString(bytes);
        }
        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static string ReverseInSwap(string swap)
        {
            string s_bits = "";
            if (swap != "")
            {
                var s_1 = swap.Substring(0, 2);
                var s_2 = swap.Substring(2, 2);
                s_bits = StringHexToBinary(s_2 + "" + s_1);
            }

            return s_bits;
        }

        public static string StringHexToBinary(string hexstring)
        {
            string binarystring = String.Join(String.Empty,
                hexstring.Select(
                    c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')
                )
            );
            return binarystring;
        }
        public static string ToBinary(Int64 Decimal)
        {
            // Declare a few variables we're going to need
            Int64 BinaryHolder;
            char[] BinaryArray;
            string BinaryResult = "";

            while (Decimal > 0)
            {
                BinaryHolder = Decimal % 2;
                BinaryResult += BinaryHolder;
                Decimal = Decimal / 2;
            }

            // The algoritm gives us the binary number in reverse order (mirrored)
            // We store it in an array so that we can reverse it back to normal
            BinaryArray = BinaryResult.ToCharArray();
            //Array.Reverse(BinaryArray);
            BinaryResult = new string(BinaryArray);

            return BinaryResult.PadRight(8, '0');
        }
        public static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }

        public static int ConvertOneByteToInt(string hexString)
        {
            int num = Int32.Parse(hexString, NumberStyles.HexNumber);
            return num;
        }

        public static int ConvertTwoBytesToInt(string hexString)
        {
            var num = (Int32)(BitConverter.ToInt16(StringToByteArray(hexString), 0));
            return num;
        }

        public static int GetIntFromHex(byte[] hexString)
        {
            string cHexString = BitConverter.ToString(hexString);

            int num = Int32.Parse(cHexString.Replace("-", ""), NumberStyles.HexNumber);
            return num;
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static byte[] StringToByteArray(string hex)
        {

            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }


        public static uint HexToUint(byte[] hexString)
        {
            string hex = BitConverter.ToString(hexString);
            uint val = Convert.ToUInt32(hex.Replace("-", String.Empty), 16);
            return val;
        }
        public static string ReverseStr(string Str)
        {
            string reversestring = "";
            int Length;
            Length = Str.Length - 1;
            while (Length >= 0)
            {
                reversestring = reversestring + Str[Length];
                Length--;
            }
            return reversestring;
        }
        public static string hex2binary(string hexvalue)
        {

            return string.Join(String.Empty, hexvalue.Select(c => Convert.ToString(Convert.ToUInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
        }
        public static IEnumerable<T[]> SplitArray<T>(T[] sourceArray, int rangeLength)
        {
            int startIndex = 0;

            do
            {
                T[] range = new T[Math.Min(rangeLength, sourceArray.Length - startIndex)];
                Array.Copy(sourceArray, startIndex, range, 0, range.Length);
                startIndex += rangeLength;
                yield return range;
            }
            while (startIndex < sourceArray.Length);
        }
    }
}