﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HardwareTester
{
    public class cVT600_900
    {
        private const string queue_path = @".\private$\UTS_MQ_IN";

        private string protocol_id;
        private string device_id;
        private string date_time;
        private string phonenumber;

        private double lat;
        private double lon;
        private double speed;
        private string course_stat;
        private int is_real_time_gps;
        private int gps_positioned;
        private int east_west_lon;
        private int south_north_lat;
        private double heading;

        private string MNC;
        private string LAC;
        private string cellID;
        private string serial_num;
        private string ign_stat;

        private string odometer;
        private double alt;
        private string report_type;
        private string gps_time;
        private string gps_date;
        private string device_status;
        
        public void process_msg(string simei, string alarm, string sMsg, string rept_type, string orData)
        {
            string[] s_init = orData.Split(new string[] { "$$" }, StringSplitOptions.None);

            foreach (string sData in s_init)
            {
                if (sData == "")
                    continue;
                string[] newArray = sData.Split('|');
                if (newArray.Length >= 9)
                {
                    try
                    {//\0?\u0012\u0011a#\u0017X??U115705.000,A,0144.2449,N,10354.3624,E,0.00,43,181217,,*31|0.7|14|2000|0008,0000,021C,02DA|01F600105DFDF8B8|13|0033DE96|0Cdt\r\n
                        device_id = simei.TrimEnd('F');
                        report_type = rept_type;
                        string[] arr_data = sData.Split(',');

                        if (arr_data.Length < 14)
                        {
                            continue;
                        }
                        //06 36 01.000    R063531.000
                        string[] date_stuff = arr_data[0].Split('?');
                        //string[] realDate = Regex.Replace(date_stuff[date_stuff.Length - 1], "[A-Za-z ]", "").Split('.');
                        if (arr_data[0] == ".000")
                        {
                            gps_time = DateTime.UtcNow.ToShortDateString();
                        }
                        else
                        {
                            string[] realDate = date_stuff[date_stuff.Length - 1].Split('.');
                            int ln = realDate[0].Length;
                            gps_time = realDate[0].Substring(ln - 6, 2) + ":" + realDate[0].Substring(ln - 4, 2) + ":" + realDate[0].Substring(ln - 2, 2);

                        }

                        double iLat = double.Parse(arr_data[2].Substring(0, 2)) + double.Parse(arr_data[2].Substring(2, 7)) / 60.0;// +double.Parse(arr_data[2].Substring(5, 4)) / 3600.0;
                        if (arr_data[1] == "V")
                        {//return;
                        }
                        if (arr_data[3] == "S")
                        {
                            lat = -1 * iLat;
                        }
                        else
                        { lat = iLat; }
                        // double iLon = double.Parse(arr_data[4].Substring(0, 3)) + double.Parse(arr_data[4].Substring(3, 2)) / 60.0 + double.Parse(arr_data[4].Substring(6, 4)) / 3600.0;
                        double iLon = double.Parse(arr_data[4].Substring(0, 3)) + double.Parse(arr_data[4].Substring(3, 7)) / 60.0;
                        if (arr_data[5] == "W")
                            lon = -1 * +iLon;
                        else
                            lon = iLon;
                        speed = double.Parse(arr_data[6]) * 1.852;
                        heading = (arr_data[7] == "") ? 0.0 : double.Parse(arr_data[7]);
                        string[] arr_data1 = arr_data[8].Split('|');
                        gps_date = arr_data1[0].Substring(0, 2) + "/" + arr_data1[0].Substring(2, 2) + "/20" + arr_data1[0].Substring(4, 2);

                        string common_event = "124";
                        if (rept_type == "9999")
                            common_event = GetEvent_Type(alarm);
                        date_time = gps_date + " " + gps_time;
                        string[] odo = sData.Split('|');
                        alt = double.Parse(odo[2]);
                        device_status = ToBinary(int.Parse(odo[3].Substring(1, 1)));
                        ign_stat = device_status.Substring(2, 1);
                        if (speed > 1)
                        {
                            if (ign_stat == "0")
                                ign_stat = "1";
                        }
                        odometer = Convert.ToInt32(odo[7], 16).ToString();
                        odometer = (Convert.ToDouble(odometer) / 1000).ToString();
                        string rawdata = sMsg.Replace("-", "");// sData;//.Split('!')[1];
                       // CreateXML(device_id, common_event, rawdata, "VT600VT900", date_time, lat, lon, heading, speed, odometer, ign_stat);
                        //FileIO fio = new FileIO();
                        //fio.WriteDayLog("VT600VT900: " + device_id + "\n" + sData + "\n ");
                    }
                    catch (Exception e)
                    {
                       
                    }
                }
            }

        }
        private string GetEvent_Type(string common_event)
        {
            string eventName = "124";
            switch (common_event)
            {
                case "01"://SOS button is pressed / Input 1 active
                    eventName = "153";
                    break;
                case "10"://Low battery
                    eventName = "28";
                    break;
                case "11"://Over speed
                    eventName = "1";
                    break;
                case "12"://Movement alarm
                    eventName = "12";
                    break;
                case "14"://alarm of tracking unit beeing turned on
                    eventName = "17";
                    break;
                case "57"://GSM jamming alarm
                    eventName = "19";
                    break;
                case "66"://Over TEMPERATURE alarm
                    eventName = "40";
                    break;
                case "71"://Moving report
                    eventName = "12";
                    break;
                default:
                    eventName = "124";
                    break;
                    //case "76"://Accelerate alarm
                    //    break;
                    //case "77"://Deceleration alarm
                    //    break;
                    //case "78"://Idle alarm

                    //    break;
                    //case "79"://Exit idle alarm
                    //    break;
            }

            return eventName;
        }
        private void CreateXML(string simei, string Event_type, string sMsg, string ProtocolID, string datetime, double lat, double lon, double head, double spd, string Mileage, string ign_status, string street_search_svc, string poi_svc, string nearby_svc, string geo_uname)
        {



            //xml stuff
            XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);

            doc.AppendChild(dec);// Create the root element
            XmlElement root = doc.CreateElement("GENERIC_Message");
            doc.AppendChild(root);

            //add the message
            XmlElement xProtocolID = doc.CreateElement("protocol_id");
            xProtocolID.InnerText = ProtocolID;
            root.AppendChild(xProtocolID);

            XmlElement xRawMessage = doc.CreateElement("raw_message");
            xRawMessage.InnerText = sMsg;
            root.AppendChild(xRawMessage);

            XmlElement xDeviceID = doc.CreateElement("device_id");
            xDeviceID.InnerText = simei;
            root.AppendChild(xDeviceID);

            XmlElement xDateTime = doc.CreateElement("datetime");
            xDateTime.InnerText = datetime;
            root.AppendChild(xDateTime);

            XmlElement xalarm_status = doc.CreateElement("alarm_status");
            xalarm_status.InnerText = "000";
            root.AppendChild(xalarm_status);

            XmlElement xcommonevent = doc.CreateElement("commonevent");
            xcommonevent.InnerText = Event_type;
            root.AppendChild(xcommonevent);

            XmlElement xLat = doc.CreateElement("lat");
            xLat.InnerText = lat.ToString();
            root.AppendChild(xLat);

            XmlElement xLon = doc.CreateElement("lon");
            xLon.InnerText = lon.ToString();
            root.AppendChild(xLon);

            XmlElement xHeading = doc.CreateElement("heading");
            xHeading.InnerText = head.ToString();
            root.AppendChild(xHeading);

            XmlElement xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = "xx";
            root.AppendChild(xMCC);

            XmlElement xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = "xx";
            root.AppendChild(xMNC);

            XmlElement xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = "xx";
            root.AppendChild(xLAC);

            XmlElement xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = "xx";
            root.AppendChild(xCellID);

            //text position
            int iMax_speed = 0;

            if (spd > 0) { ign_stat = "1"; }
            else { ign_stat = "0"; }

            XmlElement xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = "";
            root.AppendChild(xTextPos);

            XmlElement xspeed = doc.CreateElement("speed");
            xspeed.InnerText = spd.ToString();
            root.AppendChild(xspeed);

            XmlElement xRoadSpeed = doc.CreateElement("max_road_speed");
            xRoadSpeed.InnerText = iMax_speed.ToString();
            root.AppendChild(xRoadSpeed);

            XmlElement xIgnStat = doc.CreateElement("ign_status");
            xIgnStat.InnerText = ign_status;
            root.AppendChild(xIgnStat);

            XmlElement xMileage = doc.CreateElement("odometer");
            xMileage.InnerText = Mileage;
            root.AppendChild(xMileage);

            string xmlOutput = doc.OuterXml;

            

        }
        private string ToBinary(Int64 Decimal)
        {
            // Declare a few variables we're going to need
            Int64 BinaryHolder;
            char[] BinaryArray;
            string BinaryResult = "";

            while (Decimal > 0)
            {
                BinaryHolder = Decimal % 2;
                BinaryResult += BinaryHolder;
                Decimal = Decimal / 2;
            }

            // The algoritm gives us the binary number in reverse order (mirrored)
            // We store it in an array so that we can reverse it back to normal
            BinaryArray = BinaryResult.ToCharArray();
            //Array.Reverse(BinaryArray);
            BinaryResult = new string(BinaryArray);

            return BinaryResult.PadRight(4, '0');
        }
    }
}
