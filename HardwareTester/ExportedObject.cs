﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class CellTower
    {
        public int? CellId { get; set; }
        public int? LocationAreaCode { get; set; }
        public int? MobileCountryCode { get; set; }
        public int? MobileNetworkCode { get; set; }
    }

    public class Network
    {
        public string RadioType { get; set; }
        public bool ConsiderIp { get; set; }
        public IList<CellTower> CellTowers { get; set; }
    }

   
    public class PositionDecoded
    {
        public string Id { get; set; }
        public string DeviceId { get; set; }
        public string Valid { get; set; }
        public string FixTime { get; set; }
        public string DeviceTime { get; set; }
        public string Protocol { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Altitude { get; set; }
        public string Speed { get; set; }
        public string Course { get; set; }
        public string StatusCode { get; set; }
        public string RawData { get; set; }
    }
    public class Event
    {
        public int? id { get; set; }
        public Attributes attributes { get; set; }
        public int? deviceId { get; set; }
        public string type { get; set; }
        public DateTime serverTime { get; set; }
        public int? positionId { get; set; }
        public int? geofenceId { get; set; }
    }

    public class Device
    {
        public int? id { get; set; }
        public Attributes attributes { get; set; }
        public string name { get; set; }
        public string uniqueId { get; set; }
        public string status { get; set; }
        public object lastUpdate { get; set; }
        public int? positionId { get; set; }
        public int? groupId { get; set; }
        public IList<object> geofenceIds { get; set; }
        public string phone { get; set; }
        public string model { get; set; }
        public string contact { get; set; }
        public string category { get; set; }
    }

    public class clsCommonObect
    {
        public Position position { get; set; }
        public Event Event { get; set; }
        public Device device { get; set; }
    }
    public class clsCommonObectDecoded
    {
        public PositionDecoded Position { get; set; }
        public Attributes Attributes { get; set; }
    }
    public class ExportedObject
    {
        public Location Position { get; set; }
        public Attributes Attributes { get; set; }
        public string OriginalMessage { get; set; }
        public ExportedObject(Location Pos, Attributes Att, string originalmessage)
        {
            Position = Pos; Attributes = Att; OriginalMessage = originalmessage;
        }
    }
    public class ObdParams
    {
        [JsonProperty("Engine_RPM")]
        public string RPM { get; set; }

        [JsonProperty("Coolant_Temperature")]
        public string CTemp { get; set; }

        [JsonProperty("Fuel_Tank_Level_Input")]
        public string FuelL { get; set; }

        [JsonProperty("Fuel_Type")]
        public string FuelT { get; set; }

        [JsonProperty("MAF_Air_Flow_Rate")]
        public string AirFlw { get; set; }

        [JsonProperty("Intake_Air_Temperature")]
        public string IaT { get; set; }

        [JsonProperty("Intake_manifold_pressure")]
        public string IaP { get; set; }
        [JsonProperty("Throttle_Position")]
        public string TPos { get; set; }

        [JsonProperty("Engine_Hours")]
        public string EngHrs { get; set; }

        [JsonProperty("Monitor_Status")]
        public string MStatus { get; set; }
        [JsonProperty("Engine_Fuel_Rate")]
        public string EngFuelRate { get; set; }
        [JsonProperty("Time_Advance")]
        public string TimeAdvance { get; set; }
        [JsonProperty("PIDS_Supported_41_60")]
        public string PIDS_Supported_41_60 { get; set; }
        [JsonProperty("Boost_Pressure_Control")]
        public string Boost_Pressure_Control { get; set; }

        [JsonProperty("Turbocharge_Temperature")]
        public string Turbocharge_Temperature { get; set; }

        [JsonProperty("Oxygen_Sensor_Present")]
        public string Oxygen_Sensor_Present { get; set; }
        [JsonProperty("Accelerator_Pedal_Position_E")]
        public string Accelerator_Pedal_Position_E { get; set; }
        [JsonProperty("Fuel_System_Status")]
        public string Fuel_System_Status { get; set; }

        [JsonProperty("Short_Term_Fuel_Bank1")]
        public string Short_Term_Fuel_Bank1 { get; set; }
        
        [JsonProperty("Long_Term_Fuel_Bank1")]
        public string Long_Term_Fuel_Bank1 { get; set; }

        [JsonProperty("Short_Term_Fuel_Bank2")]
        public string Short_Term_Fuel_Bank2 { get; set; }

        [JsonProperty("Long_Term_Fuel_Bank2")]
        public string Long_Term_Fuel_Bank2 { get; set; }

        [JsonProperty("Cmd_Sec_Air_Status")]
        public string Cmd_Sec_Air_Status { get; set; }
        [JsonProperty("Bank1_Sensor_1_Oxygen_sensor_Voltage")]
        public string Bank1_Sensor_1_Oxygen_sensor_Voltage { get; set; }
        [JsonProperty("Bank1_Sensor_2_Oxygen_sensor_Voltage")]
        public string Bank1_Sensor_2_Oxygen_sensor_Voltage { get; set; }
        [JsonProperty("Bank1_Sensor_3_Oxygen_sensor_Voltage")]
        public string Bank1_Sensor_3_Oxygen_sensor_Voltage { get; set; }
        [JsonProperty("Bank1_Sensor_4_Oxygen_sensor_Voltage")]
        public string Bank1_Sensor_4_Oxygen_sensor_Voltage { get; set; }
        [JsonProperty("Bank2_Sensor_1_Oxygen_sensor_Voltage")]
        public string Bank2_Sensor_1_Oxygen_sensor_Voltage { get; set; }
        [JsonProperty("Bank2_Sensor_2_Oxygen_sensor_Voltage")]
        public string Bank2_Sensor_2_Oxygen_sensor_Voltage { get; set; }
        [JsonProperty("Bank2_Sensor_3_Oxygen_sensor_Voltage")]
        public string Bank2_Sensor_3_Oxygen_sensor_Voltage { get; set; }
        [JsonProperty("Bank2_Sensor_4_Oxygen_sensor_Voltage")]
        public string Bank2_Sensor_4_Oxygen_sensor_Voltage { get; set; }
        [JsonProperty("OBD_Std_This_Vehicle_Conforms_To")]
        public string OBD_Std_This_Vehicle_Conforms_To { get; set; }
        [JsonProperty("Auxillary_Input_Status")]
        public string Auxillary_Input_Status { get; set; }
        [JsonProperty("PIDS_Supported_20_40")]
        public string PIDS_Supported_20_40 { get; set; }
        [JsonProperty("MIL_Distance")]
        public string MIL_Distance { get; set; }

        [JsonProperty("Fuel_Rail_Pressure_Vacuum")]
        public string Fuel_Rail_Pressure_Vacuum { get; set; }
        [JsonProperty("Fuel_Rail_Pressure_Inject")]
        public string Fuel_Rail_Pressure_Inject { get; set; }
        [JsonProperty("O2S1_WR_Lambda")]
        public string O2S1_WR_Lambda { get; set; }
        [JsonProperty("O2S2_WR_Lambda")]
        public string O2S2_WR_Lambda { get; set; }
        [JsonProperty("O2S3_WR_Lambda")]
        public string O2S3_WR_Lambda { get; set; }
        [JsonProperty("O2S4_WR_Lambda")]
        public string O2S4_WR_Lambda { get; set; }
        [JsonProperty("O2S5_WR_Lambda")]
        public string O2S5_WR_Lambda { get; set; }
        [JsonProperty("O2S6_WR_Lambda")]
        public string O2S6_WR_Lambda { get; set; }
        [JsonProperty("O2S7_WR_Lambda")]
        public string O2S7_WR_Lambda { get; set; }
        [JsonProperty("O2S8_WR_Lambda")]
        public string O2S8_WR_Lambda { get; set; }
        [JsonProperty("Commanded_EGR")]
        public string Commanded_EGR { get; set; }
        [JsonProperty("EGR_Error")]
        public string EGR_Error { get; set; }
        [JsonProperty("Commanded_Evaporative_purge")]
        public string Commanded_Evaporative_purge { get; set; }
        [JsonProperty("Warmups_Since_Codes_Cleared")]
        public string Warmups_Since_Codes_Cleared { get; set; }
        [JsonProperty("Distance_Since_Codes_Cleared")]
        public string Distance_Since_Codes_Cleared { get; set; }
        [JsonProperty("Evap_System_Vapor_Pressure")]
        public string Evap_System_Vapor_Pressure { get; set; }
        [JsonProperty("Barometer_Pressure")]
        public string Barometer_Pressure { get; set; }

        [JsonProperty("Caltalyst_Temp_Bank1_Sensor1")]
        public string Caltalyst_Temp_Bank1_Sensor1 { get; set; }
        [JsonProperty("Caltalyst_Temp_Bank1_Sensor2")]
        public string Caltalyst_Temp_Bank1_Sensor2 { get; set; }
        [JsonProperty("Caltalyst_Temp_Bank2_Sensor1")]
        public string Caltalyst_Temp_Bank2_Sensor1 { get; set; }
        [JsonProperty("Caltalyst_Temp_Bank2_Sensor2")]
        public string Caltalyst_Temp_Bank2_Sensor2 { get; set; }
        [JsonProperty("Monitor_Status_Driver")]
        public string Monitor_Status_Driver { get; set; }
        [JsonProperty("Ctl_Module_Voltage")]
        public string Ctl_Module_Voltage { get; set; }
        [JsonProperty("Absolute_Load_Value")]
        public string Absolute_Load_Value { get; set; }
        [JsonProperty("Cmd_Equivalent_Ratio")]
        public string Cmd_Equivalent_Ratio { get; set; }
        [JsonProperty("Relative_Throttle_position")]
        public string Relative_Throttle_position { get; set; }

        [JsonProperty("Ambient_Air_Temperature")]
        public string Ambient_Air_Temperature { get; set; }
        [JsonProperty("Abolute__Throttle_position_B")]
        public string Abolute__Throttle_position_B { get; set; }
        [JsonProperty("Abolute__Throttle_position_C")]
        public string Abolute__Throttle_position_C { get; set; }
        [JsonProperty("Accellerator_Pedal_Position_D")]
        public string Accellerator_Pedal_Position_D { get; set; }
        [JsonProperty("Accellerator_Pedal_Position_E")]
        public string Accellerator_Pedal_Position_E { get; set; }
        [JsonProperty("Accellerator_Pedal_Position_F")]
        public string Accellerator_Pedal_Position_F { get; set; }
        [JsonProperty("Commanded_Throttle_Actuator")]
        public string Commanded_Throttle_Actuator { get; set; }
        [JsonProperty("Time_Run_With_MIL_On")]
        public string Time_Run_With_MIL_On { get; set; }
        [JsonProperty("Time_Since_Codes_Cleared")]
        public string Time_Since_Codes_Cleared { get; set; }
        [JsonProperty("Max_Val_For_equivalence_Ratio")]
        public string Max_Val_For_equivalence_Ratio { get; set; }
        [JsonProperty("Max_Value_Air_Low_Rate")]
        public string Max_Value_Air_Low_Rate { get; set; }
        [JsonProperty("Engine_Load")]
        public string Engine_Load { get; set; }
    }
    public class OBD
    {
        public string IaP { get; set; }
        public string EngLoad { get; set; }
        public string RPM { get; set; }
        public string CTemp { get; set; }
        public string FuelL { get; set; }
        public string FuelT { get; set; }
        public string AirFlw { get; set; }
        public string IaT { get; set; }
        public string TPos { get; set; }
        public string EngHrs { get; set; }//Runtime since engine start
        public string MStatus { get; set; }
        public string EngFuelRate { get; set; }
        public string TimeAdvance { get; set; }
        public string PIDS_Supported_41_60 { get; set; }
        public string BPC { get; set; }//Boost_Pressure_Control
        public string LTFBank1 { get; set; }//Long_Term_Fuel_Bank1
        public string TbCTemperature { get; set; }//Turbocharge_Temperature
        public string OxygenSensorPresent { get; set; }//Oxygen Sensor Present
        public string AcceleratorPedalPositionE { get; set; }//Accelerator Pedal Position E
        public string Fuel_SystemStatus { get; set; }
        public string ShortTermFuelBank1 { get; set; }
        public string LongTermFuelBank1 { get; set; }
        public string ShortTermFuelBank2 { get; set; }
        public string LongTermFuelBank2 { get; set; }
        public string CmdSecAirStatus { get; set; }
        public string Bank1Sensor1OxygenSensorV { get; set; }
        public string Bank1Sensor2OxygenSensorV { get; set; }
        public string Bank1Sensor3OxygenSensorV { get; set; }
        public string Bank1Sensor4OxygenSensorV { get; set; }
        public string Bank2Sensor1OxygenSensorV { get; set; }
        public string Bank2Sensor2OxygenSensorV { get; set; }
        public string Bank2Sensor3OxygenSensorV { get; set; }
        public string Bank2Sensor4OxygenSensorV { get; set; }
        public string OBDStdVehicleConformsTo { get; set; }
        public string AuxillaryInputStatus { get; set; }
        public string MILDistance { get; set; }//Distance Travelled with mulfauction
        public string FuelRailPressureVacuum { get; set; }
        public string FuelRailPressureInject { get; set; }
        public string O2S1WRLambda { get; set; }
        public string O2S2WRLambda { get; set; }
        public string O2S3WRLambda { get; set; }
        public string O2S4WRLambda { get; set; }
        public string O2S5WRLambda { get; set; }
        public string O2S6WRLambda { get; set; }
        public string O2S7WRLambda { get; set; }
        public string O2S8WRLambda { get; set; }
        public string CommandedEGR { get; set; }
        public string EGRError { get; set; }
        public string CommandedEvaporativepurge { get; set; }
        public string WarmupsSinceCodesCleared { get; set; }
        public string DistanceSinceCodesCleared { get; set; }
        public string EvapSystemVaporPressure { get; set; }
        public string BarometerPressure { get; set; }
        public string CaltalystTempBank1Sensor1 { get; set; }
        public string CaltalystTempBank1Sensor2 { get; set; }
        public string CaltalystTempBank2Sensor1 { get; set; }
        public string CaltalystTempBank2Sensor2 { get; set; }
        public string MonitorStatusDriver { get; set; }
        public string CtlModuleVoltage { get; set; }
        public string AbsoluteLoadValue { get; set; }
        public string CmdEquivalentRatio { get; set; }
        public string RelativeThrottleposition { get; set; }
        public string AmbientAirTemperature { get; set; }
        public string AboluteThrottlepositionB { get; set; }
        public string AboluteThrottlepositionC { get; set; }
        public string AccelleratorPedalPositionD { get; set; }
        public string AccelleratorPedalPositionE { get; set; }
        public string AccelleratorPedalPositionF { get; set; }
        public string CommandedThrottleActuator { get; set; }
        public string TimeRunWithMILOn { get; set; }
        public string TimeSinceCodesCleared { get; set; }
        public string MaxValForequivalenceRatio { get; set; }
        public string MaxValueAirLowRate { get; set; }
        public string Vehiclespeed { get; set; }
        public string FuelConsumption { get; set; }
        public string ChargerPressure { get; set; }
        public string Axle { get; set; }
        public string ObdOdometer { get; set; }
        public string Temerature { get; set; }
        public string EngOilTemperature { get; set; }
        public string FuelinjectionTiming { get; set; }

        public string FuelPressure { get; set; }
        public string RelativeFuelRailPressure { get; set; }
        public string AbsoluteFuelRailPressure { get; set; }

    }
}
