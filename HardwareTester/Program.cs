﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using uPLibrary;
using System.Configuration;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Concurrent;

namespace HardwareTester
{


    class Program
    {
        //Traccar Variables for fetching and updating devices odometer
        public static string DbConnectionString = ConfigurationManager.ConnectionStrings["HardwareTester.Properties.Settings.DbConnectionString"].ConnectionString.ToString();
        public static ConcurrentDictionary<long, long> LastTramigoDictionary = new ConcurrentDictionary<long, long>();
        public static ConcurrentDictionary<string, string> LastTramigoEventId = new ConcurrentDictionary<string, string>();
        public static ConcurrentDictionary<long, DateTime> lastMessageTimesTamp = new ConcurrentDictionary<long, DateTime>();

        public static ConcurrentDictionary<long, string> TramigoLastEventStatus = new ConcurrentDictionary<long, string>();
        public static ConcurrentDictionary<long, bool> LastIgnitionStatusDictionary = new ConcurrentDictionary<long, bool>();
        public static ConcurrentDictionary<long, TramigoStatus> TramigoDictionary = new ConcurrentDictionary<long, TramigoStatus>();
        public static ConcurrentDictionary<long, string> TramigoLastAddressLocationDictionary = new ConcurrentDictionary<long, string>();
        public static ConcurrentDictionary<long, string> Gosafe_Devices = new ConcurrentDictionary<long, string>();


        public static ConcurrentDictionary<long, TramigoLastLocationStatus> TramigoLastLocationDictionary = new ConcurrentDictionary<long, TramigoLastLocationStatus>();
        public static ConcurrentDictionary<long, int> TramigoLastNumber = new ConcurrentDictionary<long, int>();

        private static byte Xor_Check(byte[] pdata, ushort len)
        {
            ushort i = 0;
            var addition = string.Empty;
            var xorcheck = pdata[0];
            while (++i < len)
            {
                xorcheck ^= pdata[i];
            }
            return xorcheck;
        }
        public static IEnumerable<string[]> Split(string[] value, int bufferLength)
        {
            var countOfArray = value.Length / bufferLength;
            if (value.Length % bufferLength > 0)
                countOfArray++;
            for (var i = 0; i < countOfArray; i++)
            {
                yield return value.Skip(i * bufferLength).Take(bufferLength).ToArray();

            }
        }
        public static IEnumerable<IEnumerable<T>> Split<T>(T[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }
        public bool OnlyHexInString(string val)
        {
            // For C-style hex notation (0xFF) you can use @"\A\b(0[xX])?[0-9a-fA-F]+\b\Z"
            return System.Text.RegularExpressions.Regex.IsMatch(val, @"\A\b[0-9a-fA-F]+\b\Z");
        }
        private static void DealWithGt06()
        {
            string data = "78782516130a0e15111acf0023235003f3548000505b09027f020ffa001187440604ff020017a9240d0a";
            data = "78782526130a10060e0dc702b0635805ec511000543e0901a8020fbe005e434506040f0201014f950d0a";
            data = "78782526130a10031919cf0294c70805bbf4004b55170901a80257500067c14800040302002ce4880d0a";
            data = "78780a1305000400020003deef0d0a";
            data = "78780a1346050400020035f1b50d0a";
            byte[] bData = Helper.hexStringToByteArray(data);
            data = BitConverter.ToString(bData);

            var x = new Gt06();

            x.processMsg(data, "865733028232049");
        }

        public static Dictionary<string, string> OBD_PID = new Dictionary<string, string>
            {
                {"00","PIDs_Supported"},
                {"01","Monitor_Status"},
                {"02","Freeze_DTC"},
                {"04","Engine_Load"},
                {"05", "Coolant_Temperature"},
                {"0A", "Fuel_Pressure"},
                {"0B","Intake_manifold_pressure"},
                {"0C","Engine_RPM"},
                {"0D", "Vehicle_Speed"},
                {"10", "MAF_Air_Flow_Rate"},
                {"11", "Throttle_Position"},
                {"21", "MIL_Distance"},
                {"2F", "Fuel_Tank_Level_Input"},
                {"51", "Fuel_Type"},
                {"5C", "Oil_Temperature"},
                {"1F","Engine_Hours"},
                {"0F", "Intake_Air_Temperature"},
                {"5E","Engine fuel rate" }
            };

       

        private static void DealWithTramigo()
        {
            int index;
            cTramigo ctramigo = new cTramigo();
            string sData = "047300169177095C401902455054004F79ED5C000000000000008000D2EAFDFFA51038000000F3003A00600000000000001000B60000007EA3ED5C0001370000000000000000001800050007004B4553742E204D61746865777320436875726368204B6172656E4B6172656E4E6169726F6269";
            sData = "043C0073B51A025C401902F6F24C0028C7815D00000000000000800049EC0900DB1CA7FF000045014000640000000000007CFF3A00000027C7815D00";
            string protocol = sData.Substring(0, 2);
            //not supported
            if (protocol == "02")
            {
                //protocol version 02
                ctramigo.ProcessTramigoM2mV2(sData.Replace("-", ""));
            }
            else if (protocol == "04")
            {
                //protocol version 04
                ctramigo.ProcessTramigoM2mV2FullImei(sData);
            }
            //not supported
            else if (protocol == "01")
            {
                //check 
                var protocol01 = sData.Replace("-", "");
                ctramigo.ProcessTramigoMessageV1(protocol01);
                var messageIdPos = protocol01.Substring(8, 4);
                var replyText = protocol01.Replace(messageIdPos, "00FF");
                var replyHex = Encoding.ASCII.GetBytes(replyText);
            }
            //not supported
            else if (protocol == "80")
            {
                ctramigo.ProcessTramigoLegacy(sData);
            }



        }
        static void Main(string[] args)
        {

            var test = new Test
            {
                Id=1,firstName="ombasa",LastName="Mukhwami"
            };
            Dictionary<string, object> paramDict = new Dictionary<string, object>();
            paramDict.Add($"Id", test.Id);
            paramDict.Add($"firstName", test.firstName);
            paramDict.Add($"LastName", test.LastName);
            var repo = new GenericRepository<Test>();
            repo.SaveChanges(paramDict,test);
            //DealWithTramigo();
            //string txt = "OMBASA";
            //char[] arr = txt.ToCharArray();
            //Array.Reverse(arr);
            // txt = new string(arr);
            //txt = txt.Reverse();

            //DealWithGt06();
            //Task.Run(() => MethodWithParameter("1", "2"));
            // DealWithGosafe();

            //TestBullkInsert();
            //var lst = (from m in LazyCacheManager.GetGeofenceMaster()                       
            //           select new GeoMID
            //           {
            //               geoMID = (int)m.ipkGeoMID,
            //               GeoName = m.vGeoName,
            //               iMaxSpeed = (int)m.iMaxSpeed,
            //               ZoneTypeId = (int)m.ZoneTypeId,
            //               GeofenceType = m.vGeofenceType,
            //               Loc = (from d in LazyCacheManager.GetGeofenceDetail() where d.ifkGeoMID==m.ipkGeoMID
            //                          select new Loc
            //                          {
            //                              Lg = (double)d.vLongitude ,
            //                              Lt =(double)d.vLatitude
            //                          }).ToList()
            //           }).ToList();
            //DealWithPortman();
            //string sData = "29-29-80-00-54-23-D3-94-49-19-01-09-06-41-29-03-75-68-24-02-34-61-15-00-00-00-00-FA-04-76-47-7F-FF-00-00-AA-55-0A-1D-00-00-00-FF-FF-FF-FF-07-01-00-CA-00-01-00-26-14-1C-34-00-26-14-16-29-07-FC-64-29-23-07-FC-08-53-1D-07-FC-21-86-1C-07-FC-63-D1-1C-07-FC-21-8B-1B-F4-0D";
            //string[] arr_Data = sData.Split('-');
            //StringBuilder sb = new StringBuilder();
            //sb.Append(sData.Replace("-", string.Empty));
            //byte[] dataPacket = Helper.hexStringToByteArray(sb.ToString());

            //DealWithTr11();
            //string reply = sb.ToString();
            //DealwithDevices();
            //DealWithCellocator();
            //DealWithRuptela();
            //double adc1 = 95674230.0;
            //double battery = 0;
            //double power = 0;
            //string x = adc1.ToString().Substring(2, 2);
            //string y = adc1.ToString().Substring(0, 2);
            //battery = 0.0474599 * Convert.ToInt32(adc1.ToString().Substring(2, 2));
            //power = 0.1217320 * Convert.ToInt32(adc1.ToString().Substring(0, 2));
            // DealWithVt600900();

            // DealWithNoran();
            // string gl300data = "+RESP:GTERI,250D05,868789025102954,,00000001,24018,10,1,1,0.0,0,1928.7,36.753092,-1.177440,20190101221603,0639,0002,0FE2,31FB,00,46532.0,,F0,30,92,110000,1,,20190101221656,564C$";
            //string data = "+RESP:GTFRI,380502,869606020226134,,,50,2,1,7.5,312,1804.5,36.762657,-1.298697,20181106100127,0639,0003,004C,AF90,00,1,15.7,67,1803.4,36.762792,-1.298536,20181106100139,0639,0003,004C,AF90,00,8484.3,,,,100,220100,,,,20181106100149,E18D$";
            //string data = "78780c13040601000000800041a9f00d0a";
            //string xx = data.Substring(6, 2);

            //DealWithTeltonika();
            //cQL_GV65 ql65 = new cQL_GV65();
            //ql65.getMsgType(data);
            //GeocodeNominatimXml("http://178.62.235.108", "-2.6761295", "38.1476856666667");

            //cQL_GV300 dl = new cQL_GV300();
            //dl.getMsgType(gl300data);
            //DealWithTraccarAPI();
            Console.ReadLine();


        }
        private static void TestBullkInsert()
        {
            var lst = new List<Employee>();
            lst.Add(new Employee { Id = 1, Firstname = "Mark", MiddleName = "Davis", Email = "mark.davis@whitelabeltracking.com", Amount = 1100 });
            lst.Add(new Employee { Id = 2, Firstname = "David", MiddleName = "Guto", Email = "david.guto@whitelabeltracking.com", Amount = 2200 });
            lst.Add(new Employee { Id = 3, Firstname = "Ombasa", MiddleName = "geoffrey", Email = "geoffre.ombasa@whitelabeltracking.com", Amount = 3300 });
            lst.Add(new Employee { Id = 4, Firstname = "Paul", MiddleName = "Zibbaras", Email = "paul.zibbaras@whitelabeltracking.com", Amount = 4400 });
            lst.Add(new Employee { Id = 5, Firstname = "Daniel", MiddleName = "Kinei", Email = "daniel.kinei@whitelabeltracking.com", Amount = 5500 });
            lst.Add(new Employee { Id = 6, Firstname = "Anthony", MiddleName = "rono", Email = "anthony.rono@whitelabeltracking.com", Amount = 6600 });
            lst.Add(new Employee { Id = 7, Firstname = "Philip", MiddleName = "Kitamura", Email = "phillip.kiamuras@whitelabeltracking.com", Amount = 7700 });
            lst.Add(new Employee { Id = 8, Firstname = "Lawrence", MiddleName = "Mugambi", Email = "lawrence.mugambi@whitelabeltracking.com", Amount = 8800 });

            using (SqlConnection con = new SqlConnection(DbConnectionString))
            {
                con.Open();
                SqlCommand cmd;
                SqlTransaction transactSql = con.BeginTransaction();
                try
                {

                    foreach (var item in lst)
                    {
                        cmd = new SqlCommand("UPDATE EMPLOYEE SET amount=@Amount WHERE Id=@Id;", con, transactSql);
                        //cmd = new SqlCommand("INSERT INTO EMPLOYEE(firstname,middlename,email) VALUES(@Firstname,@MiddleName,@Email);", con, transactSql);
                        cmd.CommandType = System.Data.CommandType.Text;
                        //cmd.Parameters.AddWithValue("@Firstname", item.Firstname);
                        //cmd.Parameters.AddWithValue("@MiddleName", item.MiddleName);
                        //cmd.Parameters.AddWithValue("@Email", item.Email);
                        cmd.Parameters.AddWithValue("@Amount", item.Amount);
                        cmd.Parameters.AddWithValue("@Id", item.Id);
                        cmd.ExecuteNonQuery();

                    }

                    transactSql.Commit();
                }
                catch (Exception ex)
                {
                    transactSql.Rollback();
                }
            }
        }
        public static string GeocodeNominatimXml(string geocodeurl, string lat, string lon)
        {
            var lstOfTraccarDevices = "";
            Console.Write("Loading Traccar Devices.");
            try
            {
                var url = $"{geocodeurl}/nominatim/reverse?q=format=json&lat={lat}&lon={lon}&zoom=16&addressdetails=1&extratags=1";
                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(url).Result;
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            {

                                response.EnsureSuccessStatusCode();
                                lstOfTraccarDevices = response.Content.ReadAsStringAsync().Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    if (!lstOfTraccarDevices.StartsWith("{"))
                                    {
                                        var obj = new NominatimRootobject();
                                        var doc = new XmlDocument();
                                        doc.LoadXml(lstOfTraccarDevices);

                                        var messageType = doc.DocumentElement?.Name;
                                        obj.display_name = doc.SelectNodes("//reversegeocode/result")[0]?.InnerText;
                                        foreach (XmlNode node in doc.DocumentElement)
                                        {
                                            try
                                            {
                                                var name = node.Name;
                                                switch (name)
                                                {
                                                    case "result":
                                                        obj.place_id = node.Attributes?["place_id"]?.Value;
                                                        obj.osm_type = node.Attributes?["osm_type"]?.Value;
                                                        obj.osm_id = node.Attributes?["osm_id"]?.Value;
                                                        obj.lat = node.Attributes?["lat"]?.Value;
                                                        obj.lon = node.Attributes?["lon"]?.Value;
                                                        obj.licence = node.Attributes?["licence"]?.Value;
                                                        break;
                                                    case "addressparts":
                                                        obj.address = new Address
                                                        {
                                                            suburb = node?["suburb"]?.InnerText,
                                                            county = node?["county"]?.InnerText,
                                                            state_district = node?["state_district"]?.InnerText,
                                                            state = node?["state"]?.InnerText,
                                                            postcode = node?["postcode"]?.InnerText,
                                                            country = node?["country"]?.InnerText,
                                                            country_code = node?["country_code"]?.InnerText,
                                                            house_number = node?["house_number"]?.InnerText,
                                                            road = node?["road"]?.InnerText,
                                                            residential = node?["residential"]?.InnerText,
                                                            village = node?["village"]?.InnerText,
                                                            restaurant = node?["restaurant"]?.InnerText
                                                        };
                                                        break;
                                                    case "extratags":
                                                        obj.extratags = new Extratags { maxspeed = node?["maxspeed"]?.InnerText };
                                                        break;

                                                }
                                            }
                                            catch (Exception)
                                            {
                                                continue;
                                            }
                                        }
                                        lstOfTraccarDevices = JsonConvert.SerializeObject(obj);
                                    }

                                }

                            }
                            break;
                        default:
                            lstOfTraccarDevices = "";
                            break;

                    }
                }
            }
            catch (Exception e)
            {
                lstOfTraccarDevices = "";
            }
            return lstOfTraccarDevices;
        }
        private static void DealWithPortman()
        {
            var data = "$BUSV1,100000000007131,t1,190227052139,,,,,,,,,,,,,,,860";
            data = "100000000007131,L,00,190228073046,S3157.2991E11551.8484,000,000,NA,4E024900,110,GNA,CFG:0.00|  $BUSV1,100000000007131,t2,190228073052,,,,,,,,,,,,,,,,,861  $BUSV1,100000000007131,t3,190228073052,0,19,16312.06,3214.78,1774996,637548,,,,,,,,861  ";
            data = "100000000005081,781,A,07,190310230759,S3323.1193E11540.1697,000,112,NA,4E020908,142,GNA,CFG:14400| $100000000005081,A,10,190307021210,S3323.1204E11540.1590,003,150,NA,4E020809,142,GNA,CFG:14400|";
            data = "%%100000000007031,A,04,190417123428,S3155.3076E11552.9871,000,128,NA,4E000816,110,GNA,CFG:0.00|";
            data = "%%100000000007031,A,09,190423032226,S3155.3165E11553.0058,000,000,NA,4E000815,110,GNA,CFG:0.00|";
            data = "%%100000000008570,A,06,190423085400,S3209.1963E11555.3359,068,079,NA,0E02C830,110,GNA,CFG:0.91|";
            var portman = new Portman();
            portman.process_msg(data, data);
        }
        private static void DealWithTr111()
        {
            var data = "29-29-81-00-28-37-B7-37-B8-08-07-22-15-29-57-02-23-25-71-11-40-52-80-00-00-00-00-F8-00-27-2C-7F-FC-3F-00-00-1E-00-00-00-00-00-30-77-0D";
            data = "29-29-80-00-54-23-D3-94-49-19-01-09-06-41-29-03-75-68-24-02-34-61-15-00-00-00-00-FA-04-76-47-7F-FF-00-00-AA-55-0A-1D-00-00-00-FF-FF-FF-FF-07-01-00-CA-00-01-00-26-14-1C-34-00-26-14-16-29-07-FC-64-29-23-07-FC-08-53-1D-07-FC-21-86-1C-07-FC-63-D1-1C-07-FC-21-8B-1B-F4-0D";
            var arr_data = data.Split('-');
            var header = arr_data.Take(2).ToArray();
            var cmd = arr_data[2];
            var packetlength = arr_data.Skip(3).Take(2).ToArray();
            var trackerId = arr_data.Skip(5).Take(4).ToArray();
            var positions = arr_data.Skip(9).ToArray();
            var lstPdata = new List<Position>();
            var l = positions.Length / 34;
            var size = 34;
            var countOfArray = positions.Length / size;
            if (positions.Length % size > 0)
                countOfArray++;
            for (var i = 0; i < countOfArray; i++)
            {
                var pos = new Position();
                var r = string.Join("-", positions.Skip(i * size).Take(size).ToArray());
                var r1 = r.Replace("-", "");
                var r_Array = r.Split('-');
                var b_yte = Helper.hexStringToByteArray(r1);
                if (r_Array.Length == 34)
                {
                    var date = string.Join("-", positions.Take(6).ToArray());
                    pos.latitude = BitConverter.ToSingle(b_yte.Skip(6).Take(4).ToArray(), 0);
                    pos.longitude = BitConverter.ToSingle(b_yte.Skip(10).Take(4).ToArray(), 0);
                    lstPdata.Add(pos);
                }
                else
                {

                }

            }
            var checksum = positions[positions.Length - 2];
            var packetEnder = positions[positions.Length - 1];



            Console.WriteLine(data);
        }
        private static void DealWithTr11()
        {
            var data = "29-29-81-00-28-37-B7-37-B8-08-07-22-15-29-57-02-23-25-71-11-40-52-80-00-00-00-00-F8-00-27-2C-7F-FC-3F-00-00-1E-00-00-00-00-00-30-77-0D";
            data = "29-29-80-00-54-23-D3-94-49-19-01-09-06-41-29-03-75-68-24-02-34-61-15-00-00-00-00-FA-04-76-47-7F-FF-00-00-AA-55-0A-1D-00-00-00-FF-FF-FF-FF-07-01-00-CA-00-01-00-26-14-1C-34-00-26-14-16-29-07-FC-64-29-23-07-FC-08-53-1D-07-FC-21-86-1C-07-FC-63-D1-1C-07-FC-21-8B-1B-F4-0D";
            var arr_data = data.Split('-');
            var header = arr_data.Take(2).ToArray();
            var cmd = arr_data[2];

            var packetlength = arr_data.Skip(3).Take(2).ToArray();
            var SN = string.Join("", arr_data.Skip(5).Take(4).ToArray());
            var trackerId = Convert.ToUInt64(string.Join("", arr_data.Skip(5).Take(4).ToArray()), 16).ToString();
            var positions = arr_data.Skip(9).ToArray();
            var b_yte = Helper.hexStringToByteArray(string.Join("", positions));
            var date = positions.Take(6).ToArray();//2019-01-18 06:37:05.253
            var DeviceDateTime = Convert.ToDateTime($"20{date[0]}-{date[1]}-{date[2]} {date[3]}:{date[4]}:{date[5]}");
            var lat = string.Join("", positions.Skip(6).Take(4).ToArray());
            var latno = $"{lat.Substring(3, 2)}.{lat.Substring(5, 3)}";
            var iLat = double.Parse(lat.Substring(0, 3)) + double.Parse(latno) / 60.0;
            var lon = string.Join("", positions.Skip(10).Take(4).ToArray());
            var lonno = $"{lon.Substring(3, 2)}.{lon.Substring(5, 3)}";
            var iLon = double.Parse(lon.Substring(0, 3)) + double.Parse(lonno) / 60.0;
            var speed = Convert.ToInt32(string.Join("", positions.Skip(14).Take(2).ToArray()), 16).ToString();
            var Direction = Convert.ToInt32(string.Join("", positions.Skip(16).Take(2).ToArray()), 16).ToString();
            var locAntPowre = string.Join("", positions.Skip(18).Take(1).ToArray());
            var mileage = Convert.ToInt32(string.Join("", positions.Skip(19).Take(4).ToArray()), 16).ToString();
            var A = Convert_from_Hex_Ascii.ToBinary(Convert.ToInt64(string.Join("", positions.Skip(23).Take(1).ToArray()), 16));
            int bIgnition = Convert.ToInt16(A.Substring(7));//D7=1 ACC off,D7=0  ACCon 
            var B = Convert_from_Hex_Ascii.ToBinary(Convert.ToInt64(string.Join("", positions.Skip(24).Take(1).ToArray()), 16));
            var SOS = Convert.ToInt32(B.Substring(7));// D7=0 SOS alarm,D7=1 Normal   
            var OverSpeed = Convert.ToInt32(B.Substring(6));//D6=0 Over speed alarm,D7=1 normal
            var Idle = Convert.ToInt32(B.Substring(5)); //D5 = 0 Parking overtime alarm,D5 = 1 Normal
            var C = Convert_from_Hex_Ascii.ToBinary(Convert.ToInt64(string.Join("", positions.Skip(25).Take(1).ToArray()), 16));
            var Valid = Convert.ToBoolean(Convert.ToInt16(C.Substring(7)));//0 GPS Registered
            var D = Convert_from_Hex_Ascii.ToBinary(Convert.ToInt64(string.Join("", positions.Skip(26).Take(1).ToArray()), 16));
            var WWERTYUI = string.Join("", positions.Skip(27).Take(8).ToArray());


            var checksum = arr_data[arr_data.Length - 2];
            var packetEnder = arr_data[arr_data.Length - 1];



            Console.WriteLine(data);
        }
        private static void DealWithCellocator()
        {
            var data = "4d4347500048a20700c86104d81f0400f9002101204000009ef36db601d53604000000000000d0180004020ad2b4d60387bfdcfff5820200e7030000e709351003030ce20763";
            var aryRet = Convert_from_Hex_Ascii.HexStringToByteArray(data);
            var sRcvd = BitConverter.ToString(aryRet);
            var arr_data = sRcvd.Split('-');
            var byte1 = Convert_from_Hex_Ascii.ToBinary(Convert.ToInt32(arr_data[20], 16));
            int ign = Convert.ToInt16(byte1.Substring(5, 1));
            var bIsIgnitionOn = Convert.ToBoolean(ign);
        }
        private static void DealwithDevices()
        {
            var data = "id=356496046507695&deviceId=5713&valid=true&fixTime=1544767524000&deviceTime=1544767524000&protocol=gosafe&latitude=-26.825044&longitude=22.776112&altitude=1025.0&speed=11.339097&course=127.0&statusCode=0xF11C&attributes=%7B%22sat%22%3A11%2C%22hdop%22%3A0.76%2C%22odometer%22%3A8096370%2C%22power%22%3A11.97%2C%22battery%22%3A4.14%2C%22status%22%3A17540%2C%22ignition%22%3Atrue%2C%22in1%22%3Afalse%2C%22in2%22%3Afalse%2C%22in3%22%3Afalse%2C%22in4%22%3Afalse%2C%22out1%22%3Afalse%2C%22out2%22%3Afalse%2C%22out3%22%3Afalse%2C%22geofence%22%3A%2200%22%2C%22eventStatus%22%3A%220%22%2C%22packetType%22%3A%221%24060623141218%22%2C%22raw%22%3A%222a475330362c3335363439363034363530373639352c3036303532343134313231382c2c5359533a4736533b56322e35323b56312e312e352c4750533a413b31303b5332362e3832333930383b4532322e3737343436363b32363b3132363b313031303b302e37392c434f543a383039363134362c4144433a31322e31323b342e31342c4454543a343438343b313b303b303b303b37243036303535333134313231382c2c5359533a4736533b56322e35323b56312e312e352c4750533a413b31313b5332362e3832353034343b4532322e3737363131323b32313b3132373b313032353b302e37362c434f543a383039363337302c4144433a31312e39373b342e31342c4454543a343438343b313b303b303b303b31243036303632333134313231382c2c5359533a4736533b56322e35323b56312e312e352c4750533a413b31313b5332362e3832363134343b4532322e3737373731323b32353b3132373b313032343b302e37392c434f543a383039363538362c4144433a31322e30373b342e31352c4454543a343438343b313b303b303b303b31%22%2C%22distance%22%3A206.71%2C%22totalDistance%22%3A1527642.73%2C%22motion%22%3Atrue%2C%22hours%22%3A640186000%7D";
            var exportedObject = cProgram.GetExportedObject(data);
            Console.WriteLine(exportedObject.Attributes.io1);
            Console.WriteLine($"{exportedObject.Position.DeviceId} {exportedObject.Position.Id}");
        }
        private static void DealWithTraccarAPI()
        {

            var traccarOdoUpdated = TraccarApi.UpdateOdometerOnTraccar(869586033445501, 100);
            //bool traccarOdoUpdated = TraccarApi.UpdateTraccaOdometer(351535059295675, 20279);

            //bool traccarGprsCommands = TraccarApi.SendGprsCommand(354869053580114, "STP;3600", "custom");
        }
        private static void MethodWithParameter(string param, string param2)
        {
            Console.WriteLine("Testing method");
        }
        public static List<TraccarDevices> _cacheTraccarDevices()
        {
            return LazyCacheManager.CacheGetTraccarDevices();
        }
        private static void DealWithGosafe()
        {
            var data = "*GS06,356496046507695,043245111218,1C,SYS:G6S;V2.52;V1.1.5,GPS:A;5;S26.841596;E22.802012;0;0;998;8.32,COT:8009833,ADC:11.89;4.18,DTT:4484;0;0;0;10000000;0";
            data = "*GS06,354940080315053,121911280519,,SYS:8033;V4.00;V05,GPS:A;7;S31.994974;E115.917896;0;0;11;1.03,COT:79618197,ADC:23.92;4.23,DTT:180;E0;0;0;0;1";
            data = "*GS46,358173051076993,120145260718,,SYS:76993;V8.37;V1.2.4,GPS:A;10;N14.924172;W23.497488;0;0;111;0.79,GSM:;;625;1;C1C;5331;-83,COT:;,ADC:0.04;3.73;0.00,DTT:12001;8;0;0;0;1#";

            var lstOfPositions = new List<clsCommon>();
            lstOfPositions = Convert_from_Hex_Ascii.GetListOfGosafePostions(data);
            Console.WriteLine("Testing.............");
        }
        private static void DealWithVt600900()
        {
            var rData = "2424008B121171038602FF99553036353135322E3030302C412C303133372E393132362C4E2C31303334392E363231392C452C35302E38352C36342C3035313131392C2C2A30467C302E387C34317C323430307C303030312C303030302C303132362C303241397C303146363030313035453039464230357C30427C30343742413538397C303954F10D0A2424008B121171038602FF99553036353232332E3030302C412C303133382E323736372C4E2C31303334392E383337302C452C35302E39342C32362C3035313131392C2C2A30427C302E387C35327C323430307C303030312C303030302C303132362C303241367C303146363030313035444644463843327C30447C30343742413839327C3039ADC30D0A24240011121171038602FF500046910D0A2424008B121171038602FF99553036353235332E3030302C412C303133382E363830322C4E2C31303334392E373831372C452C35302E31382C31382C3035313131392C2C2A30387C302E387C36397C323430307C303030312C303030302C303132362C303241377C303146363030313035444644463843327C31417C30343742414239417C3038A92D0D0A2424008B121171038602FF99553036353332332E3030302C412C303133382E393835332C4E2C31303335302E303932382C452C35372E31302C36362C3035313131392C2C2A30317C302E387C34327C323430307C303030312C303030302C303132362C303241387C303146363030313035444644463843327C31337C30343742414545357C3039ABF50D0A";
            var xdata = rData;
            byte[] bData = Helper.hexStringToByteArray(rData);
            rData = BitConverter.ToString(bData);
            var arr_params = rData.Split('-');
            var MobiID = xdata.Substring(8,14);
            string alarm = xdata.Substring(14, 4);
            var vt600 = new cVT600_900();
            string sRcvd2 = Encoding.ASCII.GetString(bData);
            vt600.process_msg(MobiID, alarm, rData, rData, sRcvd2);

        }
        private static void DealWithNoran()
        {

            var rData = "3400030001095800000000004B434572164231B3EA3E4E523039423039393830000031382D31302D32342031313A30333A313900";
            rData = "28003200C38000260024_035840654BD0402EF5F8324E5230394730353130_300000001900A94A4749";
            rData = "41542B4353510D0F0000004E523039";
            rData = "34000800010C0000000000008343564C13428A5CA1BF4E523039423036323136000031392D31312D30352031303A35363A333900";
            rData = "3B000980000000000000010B140000000080B0431FD21242BB34ABBF4E523039423039313631000031372D31302D30322031303A34363A32350014";
            rData = "29000980000000000000010B0EB900F51913428B8EA2BF4B6786464E52303946303132393700000014";
            byte[] bData = Helper.hexStringToByteArray(rData);
            rData = BitConverter.ToString(bData);
            var originalData = rData.Split('-');
            var MobiID = string.Empty;
            var noran = new cNoran();
            if (originalData.Length == 24)
            {
                noran.CreateMessage061(rData);
            }
            else if (originalData.Length == 34)
            {
                noran.CreateMessage06(rData);

            }
            else if (originalData.Length == 41)
            {
                noran.CreateMessage_06(rData);

            }
            else if (originalData.Length == 52)
            {
                noran.CreateMessage08(rData);
            }
            else if (originalData.Length == 59)
            {
                noran.CreateMessage_08(rData);
            }
            else
            {
                var x = int.Parse(originalData[0].ToString());
                MobiID = "";
                for (var i = 4; i < x - 1; i++)
                    MobiID = MobiID + originalData[i];
            }
        }
        private static void DealWithRuptela()
        {
            var data = "03cb000314f82b10db5401010c5bfd450e00001262411bf17636eb12dc459c13000d05080a05010200030086008f0087008800a90087001b13061e0ff71d39fa8b00028900001600101d39fa03af00008f7541000563dd960000ffe6005bfd45100000126240faf176346112dc479a13000c05080a05010200030086008f0087008800a90087001b13061e0ffa1d39fe8b00028900001600101d39fe03af00008f7541000563e5960000ffe6005bfd45120000126240faf17631e812dc44f213000a05080a05010200030086008f0087008800a90087001b13061e0ff81d39f98b00028900001600101d39f903af00008f7541000563ed960000ffe6005bfd45140000126240d8f1762fd212db477c13000a05080a05010200030086008f0087008800a90087001b13061e0ff61d3a018b00028900001600101d3a0103af00008f7541000563f3960000ffe6005bfd45160000126240d8f1762dac12db46b413000b05080a05010200030086008f0087008800a90087001b13061e0ff71d3a078b00028900001600101d3a0703af00008f7541000563f9960000ffe6005bfd45180000126240d8f1762b3312db461e13000c05080a05010200030086008f0087008800a90087001b13061e0ff61d39fe8b00028900001600101d39fe03af00008f754100056400960000ffe6005bfd451b0000126240d8f176287712db466413000d05080a05010200030086008f0087008800a90087001b13061e0ff71d39fe8b00038900001600221d39fe03af00008f754100056408960000ffe6005bfd451d00001262410af176256812dc447012001005080a05010200030086008f0087008800a90087001b13061e0ff71d39ff8b00028900001600221d39ff03af00008f754100056411960000ffe6005bfd451f00001262412cf17621e412de45e213001205080a05010200030086008f0087008800a90087001b13061e0ff71d39fc8b00028900001600221d39fc03af00008f75410005641b960000ffe6005bfd452000001262412cf1761fbe12de46e613001405080a05010200030086008f0087008800a90087001b13061e0ff71d39fa8b00018900001600101d39fa03af00008f754100056420960000ffe6005bfd452100001262411bf1761d8712df469613001505080a05010200030086008f0087008800a90087001b13061e0ff71d39fc8b00018900001600101d39fc03af00008f754100056426960000ffe6005bfd452200001262411bf1761b7212df468213001405080a05010200030086008f0087008800a90087001b13061e0ff51d39fa8b00018900001600101d39fa03af00008f75410005642c960000ffe600d147";
            var rup = new RuptelaJson();
            //  data = "007900000b1a2a5585c30100024e9c036900000f101733208ff45e07b31b570a001009090605011b1a020003001c01ad01021d338e16000002960000601a41014bc16d004e9c038400000f104fdf20900d20075103b00a001308090605011b1a020003001c01ad01021d33b116000002960000601a41014bc1ea0028f9";
            rup.CreateSendMsg(data);
        }
        static void DealWithTeltonika()
        {
            string CODEC_ID; //type of unit sending data
            int NUM_RECS; //number of records in data packet
            long REC_LENGTH; //AVL data array length
            var s_IMEI = string.Empty;
            var RCVD_DATA = "00000000000004d1081e0000016e9315607900ffb3ac5a1f62b72c003800c0130000f70401f7050311fd7f12018b13019900000000016e9315606f00ffb3ac5a1f62b72c003800c0130000f70401f7050311fd7d12018a13019500000000016e9315606500ffb3ac5a1f62b72c003800c0130000f70401f7050311fd7d12018913019900000000016e9315605b00ffb3ac5a1f62b72c003800c0130000f70401f7050311fd7f12018b13019500000000016e9315605100ffb3ac5a1f62b72c003800c0130000f70401f7050311fd7e12018913019100000000016e9315604700ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018813019500000000016e9315603d00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7c12018a13019400000000016e9315603300ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7b12018a13019500000000016e9315602900ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7e12018c13019600000000016e9315601f00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018913019700000000016e9315601500ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7e12018d13019500000000016e9315600b00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018913019b00000000016e9315600100ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7a12018a13019600000000016e93155ff700ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018b13019400000000016e93155fed00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7e12018c13019400000000016e93155fe300ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7c12018c13019600000000016e93155fd900ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7c12018613019400000000016e93155fcf00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7f12018c13019a00000000016e93155fc500ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7c12018c13019500000000016e93155fbb00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018a13019700000000016e93155fb100ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7f12018913019500000000016e93155fa700ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018c13019400000000016e93155f9d00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018a13019800000000016e93155f9300ffb3ac5a1f62b72c003800c0120000f70401f7050311fd8112018b13019800000000016e93155f8900ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018a13019800000000016e93155f7f00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd8112018b13019800000000016e93155f7500ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018c13019700000000016e93155f6b00ffb3ac5a1f62b72c003800c0120000f70401f7050311fd8112018a13019300000000016e93155f6100ffb3ac5a1f62b72c003800c0120000f70401f7050311fd7d12018b13019300000000016e93155f5700ffb3ac5a1f62b72c003800c0120000f70401f7050311fd8112018b13019500001e00000261";
            RCVD_DATA = RCVD_DATA.Replace("-", "");
            string recLength = RCVD_DATA.Substring(0, 16);
            REC_LENGTH = long.Parse(recLength, System.Globalization.NumberStyles.HexNumber);
            RCVD_DATA = RCVD_DATA.Remove(0, 16);
            CODEC_ID = RCVD_DATA.Substring(0, 2);
            NUM_RECS = int.Parse(RCVD_DATA.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            var DEVICE_ID = "352093081839820";

            //process the received data
            var cfm = new Teltonika();
            if (CODEC_ID == "08")
            {

                cfm.CreateSendMsg(RCVD_DATA, DEVICE_ID);
            }
            if (CODEC_ID == "07")
            {
                cfm.createSendMsg_GH300(RCVD_DATA, DEVICE_ID);

            }
        }

        public static Dictionary<string, string> OBD_PIDS = new Dictionary<string, string>
        {
            {"00","PIDs_Supported"},
            {"01","Monitor_Status"},
            {"02","Freeze_DTC"},
            {"03","Fuel_System_Status"},
            {"04","Engine_Load"},
            {"05","Coolant_Temperature"},
            {"06","Short_Term_Fuel_Bank1"},
            {"07","Long_Term_Fuel_Bank1"},
            {"08","Short_Term_Fuel_Bank2"},
            {"09","Long_Term_Fuel_Bank2"},
            {"0A","Fuel_Pressure"},
            {"0B","Intake_manifold_pressure"},
            {"0C","Engine_RPM"},
            {"0D","Vehicle_Speed"},
            {"0E","Time_Advance"},
            {"0F","Intake_Air_Temperature"},
            {"10","MAF_Air_Flow_Rate"},
            {"11","Throttle_Position"},
            {"12","Cmd_Sec_Air_Status"},
            {"13","Oxygen_Sensor_Present"},
            {"14","Bank1_Sensor_1_Oxygen_sensor_Voltage"},
            {"15","Bank1_Sensor_2_Oxygen_sensor_Voltage"},
            {"16","Bank1_Sensor_3_Oxygen_sensor_Voltage"},
            {"17","Bank1_Sensor_4_Oxygen_sensor_Voltage"},
            {"18","Bank2_Sensor_1_Oxygen_sensor_Voltage"},
            {"19","Bank2_Sensor_2_Oxygen_sensor_Voltage"},
            {"1A","Bank2_Sensor_3_Oxygen_sensor_Voltage"},
            {"1B","Bank2_Sensor_4_Oxygen_sensor_Voltage"},
            {"1C","OBD_Std_This_Vehicle_Conforms_To"},
            {"1D","Oxygen_Sensor_Present"},
            {"1E","Auxillary_Input_Status"},
            {"1F","Engine_Hours"},
            {"20","PIDS_Supported_20_40"},
            {"21","MIL_Distance"},
            {"22","Fuel_Rail_Pressure_Vacuum"},
            {"23","Fuel_Rail_Pressure_Inject"},
            {"24","O2S1_WR_Lambda"},
            {"25","O2S2_WR_Lambda"},
            {"26","O2S3_WR_Lambda"},
            {"27","O2S4_WR_Lambda"},
            {"28","O2S5_WR_Lambda"},
            {"29","O2S6_WR_Lambda"},
            {"2A","O2S7_WR_Lambda"},
            {"2B","O2S8_WR_Lambda"},
            {"2C","Commanded_EGR"},
            {"2D","EGR_Error"},
            {"2E","Commanded_Evaporative_purge"},
            {"2F","Fuel_Tank_Level_Input"},
            {"30","Warmups_Since_Codes_Cleared"},
            {"31","Distance_Since_Codes_Cleared"},
            {"32","Evap_System_Vapor_Pressure"},
            {"33","Barometer_Pressure"},
            {"34","O2S1_WR_Lambda"},
            {"35","O2S2_WR_Lambda"},
            {"36","O2S3_WR_Lambda"},
            {"37","O2S4_WR_Lambda"},
            {"38","O2S5_WR_Lambda"},
            {"39","O2S6_WR_Lambda"},
            {"3A","O2S7_WR_Lambda"},
            {"3B","O2S8_WR_Lambda"},
            {"3C","Caltalyst_Temp_Bank1_Sensor1"},
            {"3D","Caltalyst_Temp_Bank2_Sensor1"},
            {"3E","Caltalyst_Temp_Bank2_Sensor1"},
            {"3F","Caltalyst_Temp_Bank2_Sensor2"},
            {"40","PIDS_Supported_41_60"},
            {"41","Monitor_Status_Driver"},
            {"42","Ctl_Module_Voltage"},
            {"43","Absolute_Load_Value"},
            {"44","Cmd_Equivalent_Ratio"},
            {"45","Relative_Throttle_position"},
            {"46","Ambient_Air_Temperature"},
            {"47","Abolute__Throttle_position_B"},
            {"48","Abolute__Throttle_position_C"},
            {"49","Accellerator_Pedal_Position_D"},
            {"4A","Accellerator_Pedal_Position_E"},
            {"4B","Accellerator_Pedal_Position_F"},
            {"4C","Commanded_Throttle_Actuator"},
            {"4D","Time_Run_With_MIL_On"},
            {"4E","Time_Since_Codes_Cleared"},
            {"4F","Max_Val_For_equivalence_Ratio"},
            {"50","Max_Value_Air_Low_Rate"},
            {"51","Fuel_Type"},
            {"52","Ethanol_Fuel"},
            {"53","Absolute_Evapo_System_Vapour_Pressure"},
            {"54","Evap_System_Vapor_Pressure"},
            {"55","Short_Term_Sec_Oxygen_Bank1_Bank3"},
            {"56","Long_Term_Sec_Oxygen_Bank1_Bank3"},
            {"57","Short_Term_Sec_Oxygen_Bank1_Bank3"},
            {"58","Long_Term_Sec_Oxygen_Bank2_Bank4"},
            {"59","Absolute_Fuel_Rail_Pressure"},
            {"5A", "Relative_Accelerator_Pedal_Position"},
            {"5B", "Hybrid_Battery_Pack_Remaining_Life"},
            {"5C", "Oil_Temperature"},
            {"5D", "Fuel_Injection_Timing"},
            {"5E", "Engine_Fuel_Rate"},
            {"5F", "Emission_Requirement"},
            {"60", "PIDS_Supported_61_80"},
            {"61","Drivers_Demand_Eng_Torque"},
            {"62","Actual_Eng_Torque"},
            {"63","Eng_Reference_Torque"},
            {"64","Eng_Torque_Data"},
            {"65","Aux_IO_Supported"},
            {"66","Mass_Air_Floor_Sensor"},
            {"67","Eng_Coolant_Tempearure"},
            {"68","Intake_Air_Temp_Sensor"},
            {"69","Absolute_Fuel_Rail_Pressure"},
            {"6A","Commanded_Diesel_Intake_AirFlow_Ctl"},
            {"6B","Exhaust_Gas_Recirculation_Temperature"},
            {"6C","Commanded_Throttle_Actuator_Ctl"},
            {"6D","Fuel_Pressure_Ctl_System"},
            {"6E","Injection_Pressure_Ctl_System"},
            {"6F","Turbocharge_Compressor_Inlet_Pressure"},
            {"70","Boost_Pressure_Control"},
            {"71","Variable_Geometry_Turbo"},
            {"72","Wastegate_Ctl"},
            {"73","Exhaust_Pressure"},
            {"74","Turbopcharge_RPM"},
            {"75","Turbopcharge_Temp"},
            {"76","Boost_Pressure_Control"},
            {"77","Charge_Air_Cooler_Tempearure"},
            {"78","Exhaust_Gas_Temp_Bank1"},
            {"79","Exhaust_Gas_Temp_Bank2"},
            {"7A","Diesel_Particulate_Filter"},
            {"7B","Diesel_Particulate_Filter"},
            {"7C","Diesel_Particulate_Filter_Temp"},
            {"7D","NOx_NTE_Ctl_Area_Status"},
            {"7E","PM_NTE_Ctl_Area_Status"},
            {"7F","Eng_Runtime"},
            {"80","PIDs_Supported_81-A0"},
            {"81","Eng_Runtime"},
            {"82","Eng_Runtime"},
            {"83","NOx_Sensor"},
            {"84","Manifold_Surface_Temp"},
            {"85","NOx_Reagent_System"},
            {"86","Particulate_Matter_Sensor"},
            {"87","Inatke_Manifold_Absolute_Pressure"},
            {"A0","PIDs_Supported_A1_C0"},
            {"C0","PIDs_Supported_C1_E0"},

        };
    }
    public static class Verify_Data
    {
        public static byte Verify(byte[] b)
        {
            byte a = 0;
            for (var i = 0; i < b.Length; i++)//35832073
            {
                a = (byte)(a ^ b[i]);
            }
            return a;
        }
    }
   
    public class PositionTest
    {
        public string DateTime { get; set; }//YMDHMS
        public string Latitute { get; set; }//WWWW
        public string Longitute { get; set; } //JJJJ
        public string Speed { get; set; } //SS
        public string Diresction { get; set; } //FF
        public string Status { get; set; }//T
        public string Mileage { get; set; }//LLL
        public string VehicleStatus { get; set; }//ABCD
        public string TrackingStatus { get; set; }//W W E R T Y U I
    }
}
