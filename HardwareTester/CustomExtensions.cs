﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace HardwareTester
{
    
    public static class TrackingHelper
    {

        public static void AddOrUpdate<K, V>(this ConcurrentDictionary<K, V> dictionary, K key, V value)
        {
            if (dictionary.ContainsKey(key))
            {

                dictionary[key] = value;
            }
            else
            {
                dictionary.TryAdd(key, value);
            }
        }
        private static double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180.0;
        }

        public static double CalculateDistance(Location location1, Location location2)
        {
            double circumference = 40000.0; // Earth's circumference at the equator in km
            double distance = 0.0;

            //Calculate radians
            double latitude1Rad = DegreesToRadians((double)location1.Latitude);
            double longitude1Rad = DegreesToRadians((double)location1.Longitude);
            double latititude2Rad = DegreesToRadians((double)location2.Latitude);
            double longitude2Rad = DegreesToRadians((double)location2.Longitude);

            double logitudeDiff = Math.Abs(longitude1Rad - longitude2Rad);

            if (logitudeDiff > Math.PI)
            {
                logitudeDiff = 2.0 * Math.PI - logitudeDiff;
            }

            double angleCalculation =
                Math.Acos(
                  Math.Sin(latititude2Rad) * Math.Sin(latitude1Rad) +
                  Math.Cos(latititude2Rad) * Math.Cos(latitude1Rad) * Math.Cos(logitudeDiff));

            distance = circumference * angleCalculation / (2.0 * Math.PI);

            return distance;
        }

        public static double CalculateDistance(params Location[] locations)
        {
            double totalDistance = 0.0;

            for (int i = 0; i < locations.Length - 1; i++)
            {
                Location current = locations[i];
                Location next = locations[i + 1];

                totalDistance += CalculateDistance(current, next);
            }

            return totalDistance;
        }

        public static string GetTextPositionBetween(this string text, string sValue, string terminator)
        {
            var splitPattern = text.Split(new[] { sValue }, StringSplitOptions.None);

            var lastOfArray = splitPattern.LastOrDefault();
            var readString = lastOfArray?.Substring(0, lastOfArray.IndexOf(terminator, StringComparison.Ordinal));
            return readString;
        }
    }
    public class CustomExtensions
    {
        private double course;

        static string format = "yyyy-MM-dd HH:mm:ss";
        private static String[] DIRECTIONS = new String[] { "N", "NE", "E", "SE", "S", "SW", "W", "NW" };

        static string[] GetDateFormat()
        {
            var formats = new string[]
            {
                "dd/MM/yyyy",
                "d/M/yyyy",
                "dd/mm/yy",
                "d/m/yy",

                "dd-MM-yyyy",
                "d-M-yyyy",
                "dd-MM-yy",
                "d-M-yy",

                "dd.MM.yyyy",
                "d.M.yyyy",
                "dd.MM.yy",
                "d.M.yy",

                "dd MM yyyy",
                "d M yyyy",
                "dd MM yy",
                "d M yy",
                "HH:mm:ss MMM d",
            };

            return formats;
        }
        public static string GetTriggerEventStatus(string triggerCode)
        {
            var status = 0;
            int value = BitConverter.ToInt16(hexStringToByteArray(triggerCode.Replace("-", "")), 0);
            var statusCode = "";
            status = value;
            switch (status)
            {

                //time based,status
                case 0:
                    statusCode = "124";
                    break;
                //distance Trigger
                case 1:
                    statusCode = "124";
                    break;
                //heading changed
                case 2:
                    statusCode = "124";
                    break;
                //landmark changed but rare
                case 3:
                    statusCode = "124";
                    break;

                //iO Event
                case 4:
                    statusCode = "3";
                    break;
                //ignition On
                case 10:
                    statusCode = "7";
                    break;
                //ignition Off
                case 11:
                    statusCode = "8";
                    break;
                //trip Start
                case 12:
                    statusCode = "7";
                    break;
                //trip End
                case 13:
                    statusCode = "3";
                    break;
                //panic button
                case 20:
                    statusCode = "153";
                    break;
                //over speeding
                case 21:
                    statusCode = "1";
                    break;
                //idling
                case 22:
                    statusCode = "4";
                    break;
                //tilt alarm
                case 23:
                    statusCode = "3";
                    break;

                //temp alarm
                case 24:
                    statusCode = "3";
                    break;

                //motion sensor
                case 26:
                    statusCode = "12";
                    break;
                //ignition disabled
                case 27:
                    statusCode = "3";
                    break;
                //ignition enabled
                case 28:
                    statusCode = "3";
                    break;
                //listen activated
                case 30:
                    statusCode = "3";
                    break;
                //temp shutdown
                case 32:
                    statusCode = "3";
                    break;
                // boot reset
                case 33:
                    statusCode = "3";
                    break;
                //x button Press
                case 40:
                    statusCode = "3";
                    break;
                //power button press
                case 41:
                    statusCode = "3";
                    break;
                //call button
                case 42:
                    statusCode = "3";
                    break;
                //low power
                case 50:
                    statusCode = "3";
                    break;
                //power cut(external)
                case 51:
                    statusCode = "3";
                    break;

                //external power connected
                case 52:
                    statusCode = "3";
                    break;
                //batery level ok
                case 53:
                    statusCode = "3";
                    break;
                //batery removed
                case 54:
                    statusCode = "3";
                    break;

            }

            return statusCode;
        }
        public static DateTime GetFixTimestamp(string pData)
        {

            DateTime dt = DateTime.Now;
            string[] arr_data = pData.Split(',');
            string _test = arr_data[arr_data.Length - 1].Replace("EOF", "") + " " + DateTime.Now.ToString("yyyy");
            // var regex = new Regex(@"([a-zA-Z]+) (\d+)");
            _test = Convert.ToDateTime(_test).ToString();
            dt = Convert.ToDateTime(_test);
            // var regex = new Regex(@"([a-zA-Z]+) (\d+)");
            //var regex = new Regex(@"(?:[01]\d|2[0123]):(?:[012345]\d):(?:[012345]\d) ([a-zA-Z]+) (\d+)");
            //foreach (Match m in regex.Matches(pData))
            //{               
            //    DateTime.TryParseExact(m.Value, "HH:mm:ss MMM d", null, DateTimeStyles.None, out dt);             
            //}

            return dt;
        }

        public static string GetFixTimestampProtocol80(string pData)
        {
            var formats = new string[]
            {

                "HH:mm:ss MMM d",
                "HH:mm MMM d",
            };
            var regexOne = new Regex(@"(?<time>\d{1,2}\:\d{1,2}\:\d{2}\s)(?<month>[a-zA-Z]+) (?<day>\d+)");
            var regexTwo = new Regex(@"(?<time>\d{1,2}\:\d{2}\s)(?<month>[a-zA-Z]+) (?<day>\d+)");
            var regexes = new List<Regex> { regexOne, regexTwo };

            string dateReturned = "";
            foreach (var regex in regexes)
            {
                Match vm = regex.Match(pData);
                if (vm.Success)
                {

                    var month = vm.Groups["month"].Value;
                    var day = vm.Groups["day"].Value;
                    var time = vm.Groups["time"].Value;

                    var gValue = time + "" + month + " " + day;

                    foreach (var dtm in formats)
                    {
                        bool isTrue = DateTime.TryParseExact(gValue, dtm, CultureInfo.InvariantCulture, DateTimeStyles.None, out var dt);
                        if (isTrue)
                        {
                            dateReturned = dt.ToString("yyyy-MM-dd HH:mm:ss");
                        }
                    }
                    break;
                }
            }
            return dateReturned;
        }
        //public static string GetFixTimestamp1(string pData)
        //{

        //    DateTime dt = new DateTime();

        //    // var regex = new Regex(@"([a-zA-Z]+) (\d+)");
        //    var regex = new Regex(@"(?:[01]\d|2[0123]):(?:[012345]\d):(?:[012345]\d) ([a-zA-Z]+) (\d+)");
        //    foreach (Match m in regex.Matches(pData))
        //    {
        //        DateTime.TryParseExact(m.Value, "HH:mm:ss MMM d", null, DateTimeStyles.None, out dt);
        //    }
        //    var time = dt.Subtract(TimeSpan.FromHours(3));
        //    return time.ToString(format);
        //}

        public static string GetStandardGpsFix(DateTime messageTimeStamp, DateTime fixTimestamp)
        {
            var time = "";
            var timeStampDifference = (fixTimestamp - messageTimeStamp).TotalMinutes;
            double hourDifference = timeStampDifference / 60;
            var timespan = Math.Ceiling(hourDifference);
            var utcTime = fixTimestamp.Subtract(TimeSpan.FromHours(timespan));
            time = utcTime.ToString(format);
            return time;
        }
        public static DateTime GetmessageTimestamp(string value)
        {
            var convert = BitConverter.ToUInt32(CustomExtensions.hexStringToByteArray(value), 0);

            DateTime date = CustomExtensions.UnixTimeStampToDateTime(convert);

            return date;
        }

        public static string[] GetCoordinates(string pData)
        {
            string[] coordinates = new string[] { };

            var regex = "(-?\\d+\\.\\d+), (-?\\d+\\.\\d+)";
            var results = Regex.Matches(pData, regex);
            foreach (var m in results)
            {
                coordinates = m.ToString().Split(',');
            }
            return coordinates;
        }

        public void SetCourse(double course)
        {
            this.course = course;
        }

        public double GetCourse()
        {
            return course;
        }
        public string[] GetSpeedCourse(string pData)
        {

            Regex regex1 = new Regex("([NSWE]{1,2}) with speed (\\d+) km/h");
            Match match = regex1.Match(pData);
            string[] speedCourse = new string[] { };
            if (match.Success)
            {
                Console.WriteLine(match.Value);

                var data1 = match.Groups[0];
                var data2 = match.Groups[1].ToString();

                for (int i = 0; i < DIRECTIONS.Length; i++)
                {
                    if (data2.Equals(DIRECTIONS[i]))
                    {
                        SetCourse(i * 45.0);
                        break;
                    }
                }
                var speedpart = Regex.Match(data1.ToString(), "\\d+").Value;
                string sc = GetCourse() + "," + speedpart;
                speedCourse = sc.Split(',');
            }



            return speedCourse;
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
            return dtDateTime;
        }

        public static string Between(string value, string a, string b)
        {
            int posA = value.IndexOf(a);
            int posB = value.LastIndexOf(b);
            if (posA == -1)
            {
                return "";
            }
            if (posB == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= posB)
            {
                return "";
            }
            return value.Substring(adjustedPosA, posB - adjustedPosA);
        }
        public static Byte[] hexStringToByteArray(string hexinput)
        {

            if (hexinput == String.Empty)
                return null;
            if (hexinput.Length % 2 == 1)
                hexinput = "0" + hexinput;
            int arr_size = hexinput.Length / 2;
            Byte[] myBytes = new Byte[arr_size];
            for (int i = 0; i < arr_size; i++)
                myBytes[i] = Convert.ToByte(hexinput.Substring(i * 2, 2), 16);
            return myBytes;
        }

    }
}