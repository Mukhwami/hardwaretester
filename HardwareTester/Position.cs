﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class CommonObj
    {
        public Position Position { get; set; }
        public Attributes Attributes { get; set; }        
    }
    public class Position
    {
        public long Id { get; set; }
        public DateTime DeviceTime { get; set; }
        public int Priority { set; get; }
        public string Protocol { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public double Speed { get; set; }
        public double Course { get; set; }
        public Attributes attributes { get; set; }
        public int deviceId { get; set; }
        public string type { get; set; }
        public string protocol { get; set; }
        public object serverTime { get; set; }
        public DateTime deviceTime { get; set; }
        public DateTime fixTime { get; set; }
        public bool outdated { get; set; }
        public bool valid { get; set; }
        public double? latitude { get; set; }
        public double? longitude { get; set; }
        public double? altitude { get; set; }
        public double speed { get; set; }
        public double? course { get; set; }
        public string address { get; set; }
        public double? accuracy { get; set; }
        public Network network { get; set; }

    }
    
}
