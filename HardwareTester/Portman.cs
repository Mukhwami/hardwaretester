﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class Portman
    {
        private const string queue_path = @".\private$\UTS_MQ_IN";
        private string device_id;
        private string date_time;
        private double lat;
        private double lon;
        private int speed;
        private double heading;
        private string ign_stat;
        private string odometer;
        private string device_status;
        
        public string process_msg(string sMsg, string orData)
        {
            var s_init = orData.Split(new string[] { "%%","$" }, StringSplitOptions.None);
            var DevId = "";
            foreach (var sData in s_init)
            {
                if (sData == "")
                    continue;
                var arr_data = sData.Split(',');
                if (arr_data[0] == "BUSV1")
                {
                    //helper.HeartBeat(arr_data[1]);
                    date_time= "20" + arr_data[3].Substring(0, 2) + "/" + arr_data[3].Substring(2, 2) + "/" + arr_data[3].Substring(4, 2) + " " + arr_data[3].Substring(6, 2) + ":" + arr_data[3].Substring(8, 2) + ":" + arr_data[3].Substring(10, 2);

                    continue;
                }
                if (arr_data.Length >= 11)
                {
                    try
                    {
                        var i = 0;
                        int n;
                        var isNumeric = int.TryParse(arr_data[1], out n);
                        if (isNumeric)
                            i = 1;
                        //%%GST8000,A,050916070549,N2240.8887E11359.2994,0,000,NA,D3800000,406,CFG:Y06,GST8000_B_PTM_5.2.7_1.03|
                        //%%[ID],[GPS Valid],[Date & Time],[Loc],[Speed],[Dir],[Temp],[Status],[Event],CFG:[Message]|[CR][LF]
                        //7066,A,09,171215092413,S3228.6780E11545.9639,000,174,NA,0E000D00,110,GNA,CFG:0.00|\r\n
                        device_id = Regex.Replace(arr_data[0], "[A-Za-z ]", "");
                        DevId = device_id;
                        date_time = "20" + arr_data[3+i].Substring(0, 2) + "/" + arr_data[3+i].Substring(2, 2) + "/" + arr_data[3 + i].Substring(4, 2) + " " + arr_data[3 + i].Substring(6, 2) + ":" + arr_data[3 + i].Substring(8, 2) + ":" + arr_data[3 + i].Substring(10, 2);
                        var ltitute = arr_data[4 + i].Substring(1, 9);//S3228.6780
                        var lngitute = arr_data[4 + i].Substring(11, 10); //E11545.9639
                        var iLat = double.Parse(ltitute.Substring(0, 2)) + double.Parse(ltitute.Substring(2, 7)) / 60.0;
                        if (arr_data[1 + i] == "V")
                        {//return;
                        }
                        if (arr_data[4 + i].Substring(0, 1) == "S")
                        {
                            lat = -1 * iLat;
                        }
                        else
                        { lat = iLat; }

                        var iLon = double.Parse(lngitute.Substring(0, 3)) + double.Parse(lngitute.Substring(3, 7)) / 60.0;
                        if (arr_data[4 + i].Substring(10, 1) == "W")
                            lon = -1 * +iLon;
                        else
                            lon = iLon;
                        speed = int.Parse(arr_data[5 + i]);
                        heading = double.Parse(arr_data[6 + i]);


                        device_status = Hex2binary(arr_data[8 + i]);
                        ign_stat = device_status[14].ToString();
                        var cEvent = new cCommon_Event() { cInput_1 = 0, cInput_2 = 0, cInput_3 = 0, common_event = "124" };
                        var eventID = Convert.ToInt32(Regex.Replace(arr_data[9+i], "[A-Za-z ]", ""));
                        switch (eventID)
                        {
                            case 47://harsh accelleration
                                cEvent.common_event = "3";
                                break;
                            case 48://harsh breaking
                                cEvent.common_event = "2";
                                break;
                            case 110:
                                cEvent.common_event = "124";
                                break;
                            case 101:
                                cEvent.common_event = "1";
                                break;
                            case 111:
                                cEvent.common_event = "28";
                                break;
                            case 112:
                                cEvent.common_event = "25";
                                break;
                            case 118:
                                cEvent.common_event = "163";
                                break;
                            case 142:
                                cEvent.common_event = "13";
                                break;
                            case 143:
                                cEvent.common_event = "2";
                                break;
                            case 113:
                            case 150:
                                cEvent.common_event = "153";
                                break;
                            case 249:
                                cEvent.cInput_2 = 1;
                                cEvent.common_event = "136";
                                break;
                            case 250:
                                cEvent.cInput_2 = 1;
                                cEvent.common_event = "137";
                                break;
                            case 251://cinput1 door open
                                cEvent.cInput_1 = 1;
                                cEvent.common_event = "134";
                                break;
                            case 252://input1 door close
                                cEvent.cInput_1 = 0;
                                cEvent.common_event = "135";
                                break;
                            case 253://ACC ON
                                cEvent.cInput_2 = 1;
                                cEvent.common_event = "7";
                                break;
                            case 254://ACC OFF
                                cEvent.cInput_2 = 0;
                                cEvent.common_event = "8";
                                break;
                            case 255://temperature sensor out of present range
                                cEvent.cInput_3 = 0;
                                cEvent.common_event = "136";
                                break;
                            case 256://temperature sensor within of present range
                                cEvent.cInput_3 = 1;
                                cEvent.common_event = "137";
                                break;
                            case 178://S178 is Iridium position report..
                                cEvent.common_event = "124";
                                break;
                            default: //get event identifier	
                                cEvent.common_event = "124";// getEventID(eventID);
                                break;
                        }
                        //odometer = getOdometer.GetOdommeter(device_id,lat,lon,speed).ToString();
                        odometer = "0"; //moving odo to listener - make sure port is specified in generic xml
                        var rawdata = sData;
                        

                    }
                    catch (Exception e)
                    {
                       
                    }
                }
            }
            return DevId;
        }
        private string getEventID(int eventID)
        {
            var event_type = "";
            switch (eventID)
            {
                case 560://harsh accelleration report
                    event_type = "3";
                    break;
                case 561://harsh break
                    event_type = "1";
                    break;
                case 111://low battery
                    event_type = "28";
                    break;
                case 112://battery removed
                    event_type = "30";
                    break;
                case 143://crash report
                    event_type = "20";
                    break;
                case 113:
                case 304://sos
                    event_type = "153";
                    break;
                default:
                    event_type = "124";
                    break;
            }
            return event_type;
        }
        public static string Hex2binary(string hexvalue)
        {

            return String.Join(String.Empty, hexvalue.Select(c => Convert.ToString(Convert.ToUInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
        }
    }
    internal class cCommon_Event
    {
        public string common_event { set; get; }
        public int cInput_1 { set; get; }
        public int cInput_2 { set; get; }
        public int cInput_3 { set; get; }
    }
}
