﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HardwareTester
{
    public class TramigoStatus
    {
        public long Imei { get; set; }
        public string Gps { get; set; }
        public string Gsm { get; set; }
        public string Battery { get; set; }
        public bool IgnitionStatus { get; set; }
        public string GeoAddress { get; set; }

        public DateTime LastUpDatedTime { get; set; }

    }

    public class TramigoLastLocationStatus
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string GpsDateTime { get; set; }
        public string TextMessage { get; set; }
        public bool IgnitionStatus { get; set; }
        public string LastEventStatus { get; set; }
        public string RawData { get; set; }
    }
    public class cTramigo
    {
        private const string queue_path = @".\private$\UTS_MQ_IN";

        private string s_Imei;
        private string s_lon;
        private string s_lat;
        private string s_speed;
        private string s_heading;
        private string s_odometer;
        private bool s_ignition;
        private string s_eventId;
        private string s_gps;
        private string s_gsm;
        private string s_message_timestamp;
        private string s_battery;
        private string s_gps_fixtimestamp;

        private string s_sequenceNumber;
        private bool s_IgnoreGeoCode;
        private string s_countryCode;
        private string s_textMessage;
        private string s_headergpsTime;


        //thread counter
        private static int m_counter;
        public static int Counter
        {
            get { return m_counter; }
        }
       

        public string ProcessTramigoM2mV2(string xdata)
        {

            try
            {



                byte[] tramigoDataBytes = HexStringToByteArray(xdata);

                List<byte[]> bMessageHeaderProtocolList = new List<byte[]>();
                var nextheaderPayload = tramigoDataBytes;
                for (int i = 0; i < tramigoDataBytes.Length; i++)
                {
                    if (nextheaderPayload.Length == 0)
                        break;
                    int nextDataSize = BitConverter.ToInt16(nextheaderPayload.Skip(1).Take(2).ToArray(), 0);
                    var bRecord = nextheaderPayload.Take(nextDataSize).ToArray();
                    nextheaderPayload = nextheaderPayload.Skip(nextDataSize).ToArray();
                    bMessageHeaderProtocolList.Add(bRecord);
                }

                foreach (var vm in bMessageHeaderProtocolList)
                {
                    var header = vm.Take(15).ToArray();
                    var payload = vm.Skip(15).ToArray();

                    var headerProtocol = header.Take(1);
                    var headerCrc = header.Skip(3).Take(2);
                    var headerSequenceNumber = header.Skip(5).Take(2).ToArray();
                    var headerSenderId = header.Skip(7).Take(4).ToArray();
                    var headerMessageTimesamp = header.Skip(11).Take(4).ToArray();


                    var conv = headerSenderId;
                    //cannot process without textpos,to discuss this
                    //if (payload.Length <= 41)
                    //{
                    //    return;
                    //}

                    var headertimeStamp =
                        UnixTimeStampToDateTime(BitConverter.ToInt32(headerMessageTimesamp.ToArray().ToArray(), 0));
                    var imei = BitConverter.ToInt32(headerSenderId.ToArray(), 0);

                    //<Messages under PayLoad>//

                    //Basic 0
                    //Tid 1
                    //Compact Location 10
                    //Compact heartBeat 11
                    //Extended 20
                    //Trip 21
                    //Zone 22
                    //System  status 30
                    //Zonelist 31
                    //Analog 40
                    //Serial Communication 41
                    //Console 50
                    //Acknowledgement 255


                    List<byte[]> bpayloadList = new List<byte[]>();
                    var nextPayload = payload;
                    var recordSize = 0;
                    var nextRecordSize = 0;
                    var nextMessageId = 0;
                    for (var i = 0; i < payload.Length; i++)
                    {
                        if (nextPayload.Length == 0)
                            break;
                        nextMessageId = int.Parse(BitConverter.ToString(nextPayload.Take(1).ToArray()),
                            NumberStyles.HexNumber);
                        if (nextMessageId == 0)
                        {
                            if (nextPayload.Length == 0)
                                return null;
                            //Basic Id 0
                            recordSize = 40;
                            nextRecordSize = recordSize + 1;
                            var basicPayLoad = nextPayload.Take(recordSize).ToArray();
                            var bmessageValueId = basicPayLoad.Take(1);
                            var bTrigger = basicPayLoad.Skip(1).Take(2);
                            var bTriggerReserveValue = basicPayLoad.Skip(3).Take(4);
                            var bStateFlags = basicPayLoad.Skip(7).Take(2);
                            var bLatitude = basicPayLoad.Skip(9).Take(4);
                            var bLongitude = basicPayLoad.Skip(13).Take(4);
                            var bSpeed = basicPayLoad.Skip(17).Take(2);
                            var bDirection = basicPayLoad.Skip(19).Take(2);
                            var bGsmSignal = basicPayLoad.Skip(21).Take(1);
                            var bGpsSignal = basicPayLoad.Skip(22).Take(1);
                            var bBatteryLevel = basicPayLoad.Skip(23).Take(1);
                            var bCurrentTriOdometer = basicPayLoad.Skip(24).Take(2);
                            var bTldReferenceId = basicPayLoad.Skip(26).Take(4);
                            var bBearingToTld = basicPayLoad.Skip(30).Take(2);
                            var bDistanceToTld = basicPayLoad.Skip(32).Take(4);
                            var bFixTimeStamp = basicPayLoad.Skip(36).Take(4);
                            var bReserve = basicPayLoad.Skip(40).Take(1);


                            var latitude = BitConverter.ToInt32(bLatitude.ToArray(), 0) * 0.00001;
                            var longitude = BitConverter.ToInt32(bLongitude.ToArray(), 0) * 0.00001;
                            var speed = BitConverter.ToInt16(bSpeed.ToArray(), 0);
                            var direction = BitConverter.ToInt16(bDirection.ToArray(), 0);

                            var gsmSignal = int.Parse(BitConverter.ToString(bGsmSignal.ToArray()),
                                NumberStyles.HexNumber);
                            var gpsSignal = int.Parse(BitConverter.ToString(bGpsSignal.ToArray()),
                                NumberStyles.HexNumber);
                            var batteryLevel = int.Parse(BitConverter.ToString(bBatteryLevel.ToArray()),
                                NumberStyles.HexNumber);

                            var curentTripOdometer = BitConverter.ToInt16(bCurrentTriOdometer.ToArray(), 0);

                            var tldReferenceId = BitConverter.ToInt32(bTldReferenceId.ToArray().ToArray(), 0);
                            var bearingToTld = BitConverter.ToInt16(bBearingToTld.ToArray(), 0);
                            var distanceToTld = BitConverter.ToInt16(bDistanceToTld.ToArray(), 0);

                            var fixtimeStamp =
                                UnixTimeStampToDateTime(BitConverter.ToInt32(bFixTimeStamp.ToArray().ToArray(), 0));

                            var gpsTime = fixtimeStamp.ToString("yyyy-MM-dd HH:mm:ss");


                            var headergpsTime = headertimeStamp.ToString("yyyy-MM-dd HH:mm:ss");
                            var sequnceNumber = BitConverter.ToInt16(headerSequenceNumber.ToArray(), 0);
                            s_sequenceNumber = sequnceNumber.ToString();

                            //common Events & Ignition States                         

                            var trC = BitConverter.ToInt16(bTrigger.ToArray(), 0);
                            //check  trigger codes

                            if (trC == 0 || trC == 1)
                            {
                                var stateflagsValues = GetRangeStates(bStateFlags);

                                //ignition States
                                if (stateflagsValues == 101)
                                {
                                    //charging
                                    s_eventId = "26";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 102)
                                {
                                    //power connected
                                    s_eventId = "24";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 103)
                                {
                                    //battery low
                                    s_eventId = "28";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 104)
                                {
                                    //no battery
                                    s_eventId = "30";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 105)
                                {
                                    //bad battery
                                    s_eventId = "43";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 106)
                                {
                                    //ignition on/off
                                    //check last status
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;

                                }
                                else if (stateflagsValues == 107)
                                {
                                    //Shock sensor
                                    s_ignition = true;
                                    s_eventId = "7";
                                }
                                else if (stateflagsValues == 108)
                                {
                                    //parked
                                    s_ignition = false;
                                    s_eventId = "124";
                                }
                                else if (stateflagsValues == 109)
                                {
                                    //moving
                                    s_ignition = true;
                                    s_eventId = "124";


                                }
                                else if (stateflagsValues == 110)
                                {
                                    //Stopped



                                    if (Program.TramigoLastNumber.ContainsKey(imei))
                                    {
                                        Program.TramigoLastNumber.TryGetValue(imei, out var timeOutNumber);

                                        if (timeOutNumber == 1)
                                        {
                                            s_eventId = "124";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 2, 1);
                                        }
                                        else if (timeOutNumber == 2)
                                        {
                                            s_eventId = "124";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 3, 2);
                                        }
                                        else if (timeOutNumber == 3)
                                        {
                                            s_eventId = "124";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 4, 3);
                                        }
                                        else if (timeOutNumber == 4)
                                        {
                                            s_eventId = "124";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 5, 4);
                                        }
                                        else if (timeOutNumber == 5)
                                        {
                                            s_eventId = "8";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 6, 5);
                                        }
                                        else
                                        {
                                            s_eventId = "124";
                                            s_ignition = false;
                                        }

                                    }
                                    else
                                    {

                                        Program.TramigoLastNumber.TryAdd(imei, 1);
                                        s_eventId = "124";
                                        s_ignition = true;

                                    }

                                }
                                else if (stateflagsValues == 111)
                                {
                                    //gps on/off
                                    s_eventId = "124";
                                }
                                else if (stateflagsValues == 112)
                                {
                                    //ingition on off
                                    //check last ignition status
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var value);

                                    if (value)
                                    {
                                        s_eventId = "8";
                                    }
                                    else
                                    {
                                        s_eventId = "7";

                                    }
                                }
                                else
                                {
                                    s_eventId = "124";
                                    if (speed > 1)
                                    {
                                        s_ignition = true;

                                    }
                                    else
                                    {
                                        s_ignition = false;

                                    }


                                }
                            }
                            else if (trC == 2)
                            {
                                //124
                                s_ignition = true;
                                s_eventId = "124";
                            }
                            else if (trC == 10)
                            {
                                //124
                                var stateflagsValues = GetRangeStates(bStateFlags);
                                if (stateflagsValues != 109)
                                {
                                    s_eventId = "7";
                                }
                                else
                                {
                                    s_ignition = true;
                                    s_eventId = "124";
                                }

                                s_ignition = true;
                            }
                            else if (trC == 12)
                            {
                                if (speed > 1)
                                {
                                    s_eventId = "124";
                                }
                                else
                                {
                                    s_eventId = "7";
                                }
                                s_ignition = true;

                            }
                            else if (trC == 11 || trC == 13)
                            {
                                //124
                                s_ignition = false;
                                s_eventId = "8";
                                speed = 0;
                            }
                            else if (trC == 20)
                            {
                                //124

                                //check states for ignition on off/

                                s_eventId = "153";
                                s_ignition = true;
                            }
                            else if (trC == 21)
                            {

                                //124
                                s_ignition = true;
                                s_eventId = "1";
                            }
                            else if (trC == 22)
                            {
                                //124
                                s_ignition = true;
                                s_eventId = "4";
                            }

                            else if (trC == 23)
                            {
                                //124
                                s_ignition = true;
                                s_eventId = "14";

                            }
                            else if (trC == 24)
                            {
                                //124
                                s_ignition = true;
                                s_eventId = "124";
                            }
                            else if (trC == 50)
                            {

                                s_eventId = "22";
                            }
                            else if (trC == 51)
                            {

                                s_eventId = "25";
                            }
                            else if (trC == 52)
                            {

                                s_eventId = "24";
                            }
                            else
                            {
                                if (speed > 1)
                                {
                                    s_ignition = true;
                                }
                                else
                                {
                                    s_ignition = false;
                                }

                                s_eventId = "124";
                            }


                            var currentOdo = curentTripOdometer * 0.001;

                            s_Imei = imei.ToString();
                            s_lon = longitude.ToString(CultureInfo.InvariantCulture);
                            s_lat = latitude.ToString(CultureInfo.InvariantCulture);
                            s_speed = speed.ToString();
                            s_heading = direction.ToString();
                            s_odometer = currentOdo.ToString(CultureInfo.InvariantCulture);
                            s_gps = gpsSignal.ToString();
                            s_gsm = gsmSignal.ToString();
                            s_battery = batteryLevel.ToString();
                            s_gps_fixtimestamp = headergpsTime.ToString();




                        }
                        else if (nextMessageId == 1)
                        {
                            //Message Id 1 variable length.
                            var vlength = int.Parse(BitConverter.ToString(nextPayload.Skip(1).Take(1).ToArray()),
                                NumberStyles.HexNumber);
                            recordSize = vlength;
                            nextRecordSize = recordSize;
                            var tramigolandMarkData = nextPayload.Take(vlength).ToArray();
                            var vTldLanguage = tramigolandMarkData.Skip(3).Take(2);
                            var vEncodingCompresion = tramigolandMarkData.Skip(5).Take(1);
                            var vclassTypeReserve = tramigolandMarkData.Skip(6).Take(1);
                            var vtldReferenceId = tramigolandMarkData.Skip(7).Take(4);
                            var vName1Length = tramigolandMarkData.Skip(11).Take(2);
                            var vName2Length = tramigolandMarkData.Skip(13).Take(2);
                            var vName3Length = tramigolandMarkData.Skip(15).Take(2);
                            var vCountryCode = tramigolandMarkData.Skip(17).Take(2);
                            var unknownLength = tramigolandMarkData.Skip(19);

                            var countryCode = Encoding.ASCII.GetString(vCountryCode.ToArray());
                            var textMessage = Encoding.ASCII.GetString(unknownLength.ToArray());

                            s_IgnoreGeoCode = false;
                            s_countryCode = countryCode;
                            s_textMessage = textMessage;

                            Program.TramigoLastAddressLocationDictionary.AddOrUpdate(imei, textMessage,
                                (key, value) => textMessage);
                        }
                        else if (nextMessageId == 10)
                        {
                            //Compact location

                            recordSize = 13;
                            nextRecordSize = recordSize;
                            var compactLocationByte = nextPayload.Take(recordSize).ToArray();
                            var lat = compactLocationByte.Skip(1).Take(4);
                            var lon = compactLocationByte.Skip(5).Take(4);
                            var speed = compactLocationByte.Skip(9).Take(2);
                            var direction = compactLocationByte.Skip(11).Take(2);
                        }

                        else if (nextMessageId == 11)
                        {
                            //Compact Heartbeat
                            //all units in percentage
                            recordSize = 4;
                            nextRecordSize = recordSize;
                            var compactHeartBeat = nextPayload.Take(recordSize).ToArray();
                            var hgsmSignalQuality = compactHeartBeat.Skip(1).Take(1);
                            var hgpsSignalQuality = compactHeartBeat.Skip(2).Take(1);
                            var hbatteryLevel = compactHeartBeat.Skip(3).Take(1);
                        }

                        else if (nextMessageId == 20)
                        {

                            //Extended to be added

                        }

                        else if (nextMessageId == 40)
                        {

                            //Analog/One Wire
                            var oneWireBytes = nextPayload.Take(41).ToArray();
                            var driverId = oneWireBytes.Skip(1).Take(8);
                            var tempSensor1Id = oneWireBytes.Skip(9).Take(8);
                            var tempSensor1Value = oneWireBytes.Skip(17).Take(2);

                            var tempSensor2Id = oneWireBytes.Skip(19).Take(8);
                            var tempSensor2Value = oneWireBytes.Skip(27).Take(2);

                            var tempSensor3Id = oneWireBytes.Skip(29).Take(8);
                            var tempSensor3Value = oneWireBytes.Skip(37).Take(2);
                            var analogueData = oneWireBytes.Skip(39).Take(2);

                        }

                        var bRecord = nextPayload.Take(recordSize).ToArray();
                        nextPayload = nextPayload.Skip(nextRecordSize).ToArray();
                        bpayloadList.Add(bRecord);
                    }



                    if (string.IsNullOrEmpty(s_textMessage))
                    {
                        //uses inbuilt addressing
                        Program.TramigoLastAddressLocationDictionary.TryGetValue(imei, out var message);
                        s_textMessage = message;
                    }

                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        //        // Create the XML Declaration, and append it to XML document
                        XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
                        doc.AppendChild(dec);

                        // Create the root element
                        XmlElement root = doc.CreateElement("Tramigo_Message");
                        doc.AppendChild(root);

                        XmlElement xImei = doc.CreateElement("device_Id");
                        xImei.InnerText = s_Imei;
                        root.AppendChild(xImei);


                        XmlElement xGpsUtc = doc.CreateElement("GPSDateTime");
                        xGpsUtc.InnerText = s_gps_fixtimestamp.ToString();
                        root.AppendChild(xGpsUtc);


                        XmlElement xLat = doc.CreateElement("Lat");
                        xLat.InnerText = s_lat.ToString(CultureInfo.InvariantCulture);
                        root.AppendChild(xLat);

                        XmlElement xlon = doc.CreateElement("Lon");
                        xlon.InnerText = s_lon.ToString(CultureInfo.InvariantCulture);
                        root.AppendChild(xlon);

                        var xSpd = doc.CreateElement("Speed");
                        xSpd.InnerText = s_speed;
                        root.AppendChild(xSpd);

                        XmlElement xHead = doc.CreateElement("Heading");
                        xHead.InnerText = s_heading;
                        root.AppendChild(xHead);

                        XmlElement xEvent = doc.CreateElement("event_id");
                        xEvent.InnerText = s_eventId;
                        root.AppendChild(xEvent);


                        XmlElement xgpStatus = doc.CreateElement("GpStatus");
                        xgpStatus.InnerText = s_gps;
                        root.AppendChild(xgpStatus);


                        XmlElement xgpgsm = doc.CreateElement("Gsm");
                        xgpgsm.InnerText = s_gsm;
                        root.AppendChild(xgpgsm);

                        //XmlElement xOdometer = doc.CreateElement("Odometer");
                        //xOdometer.InnerText = s_odometer;
                        //root.AppendChild(xOdometer);

                        XmlElement xIgn = doc.CreateElement("Ign_Status");
                        xIgn.InnerText = s_ignition.ToString();
                        root.AppendChild(xIgn);

                        XmlElement xiVoltage = doc.CreateElement("battery");
                        xiVoltage.InnerText = s_battery;
                        root.AppendChild(xiVoltage);

                        XmlElement xSequenceNumber = doc.CreateElement("sequenceNumber");
                        xSequenceNumber.InnerText = s_sequenceNumber;
                        root.AppendChild(xSequenceNumber);
                        //text position

                        XmlElement xcountryCode = doc.CreateElement("countryCode");
                        xcountryCode.InnerText = s_countryCode;
                        root.AppendChild(xcountryCode);

                        XmlElement xGeonamesenabled = doc.CreateElement("IgnoreGeoCode");
                        xGeonamesenabled.InnerText = Convert.ToBoolean(s_IgnoreGeoCode).ToString();
                        root.AppendChild(xGeonamesenabled);


                        XmlElement xtextMessage = doc.CreateElement("textMessage");
                        xtextMessage.InnerText = s_textMessage;
                        root.AppendChild(xtextMessage);


                        XmlElement xsoftwareOdo = doc.CreateElement("useSoftwareOdo");
                        bool useSoftOdo = true;
                        xsoftwareOdo.InnerText = useSoftOdo.ToString();
                        root.AppendChild(xsoftwareOdo);

                        var rawData = BitConverter.ToString(vm).Replace("-", "");
                        XmlElement xraw = doc.CreateElement("raw_data");
                        xraw.InnerText = rawData;
                        root.AppendChild(xraw);
                        string xmlOutput = doc.OuterXml;
                        var viewXml = xmlOutput;


                        #region DictionaryUpadte

                      

                        var lastDetails = new TramigoLastLocationStatus
                        {
                            GpsDateTime = s_gps_fixtimestamp,
                            TextMessage = s_textMessage,
                            IgnitionStatus = s_ignition,
                            Latitude = s_lat,
                            Longitude = s_lon,
                            LastEventStatus = s_eventId,
                            RawData = BitConverter.ToString(vm)
                        };
                        Program.TramigoLastLocationDictionary.
                            AddOrUpdate(imei, lastDetails, (key, value) => lastDetails);
                        #endregion

                       
                    }
                    catch (Exception ex)
                    {
                        
                    }




                }
            }
            catch (Exception ex)
            {
               
            }

            return s_Imei;
        }
        public string ProcessTramigoM2mV2FullImei(string mdata)
        {
            string xdata = mdata;
            try
            {

                byte[] tramigoDataBytes = HexStringToByteArray(xdata);

                List<byte[]> bMessageHeaderProtocolList = new List<byte[]>();
                var nextheaderPayload = tramigoDataBytes;
                for (int i = 0; i < tramigoDataBytes.Length; i++)
                {
                    if (nextheaderPayload.Length == 0)
                        break;
                    int nextDataSize = BitConverter.ToInt16(nextheaderPayload.Skip(1).Take(2).ToArray(), 0);
                    var bRecord = nextheaderPayload.Take(nextDataSize).ToArray();
                    nextheaderPayload = nextheaderPayload.Skip(nextDataSize).ToArray();
                    bMessageHeaderProtocolList.Add(bRecord);
                }

                foreach (var vm in bMessageHeaderProtocolList)
                {
                    var header = vm.Take(19).ToArray();
                    var payload = vm.Skip(19).ToArray();

                    var headerProtocol = header.Take(1);
                    var headerLength = header.Skip(1).Take(2);

                    var headerCrc = header.Skip(3).Take(2);
                    var headerSequenceNumber = header.Skip(5).Take(2).ToArray();
                    var headerSenderIdPart1 = header.Skip(7).Take(4).ToArray();
                    var headerSenderIdPart2 = header.Skip(11).Take(4).ToArray();
                    var headerMessageTimesamp = header.Skip(15).Take(4).ToArray();

                    //cannot process without textpos,to discuss this
                    //if (payload.Length <= 41)
                    //{
                    //    return;
                    //}

                    var headertimeStamp =
                        UnixTimeStampToDateTime(BitConverter.ToInt32(headerMessageTimesamp.ToArray().ToArray(), 0));

                    var imeiPart1 = BitConverter.ToInt32(headerSenderIdPart1.ToArray(), 0).ToString().PadLeft(8, '0');
                    var imeiPart2 = BitConverter.ToInt32(headerSenderIdPart2.ToArray(), 0).ToString().PadLeft(7, '0');


                    var cimei = $"{imeiPart1}{imeiPart2}";
                    long imei = long.Parse(cimei);
                    //most
                    //<Messages under PayLoad>//                    
                    //Basic 0
                    //Tid 1
                    //Compact Location 10
                    //Compact heartBeat 11
                    //Extended 20
                    //Trip 21
                    //Zone 22
                    //System  status 30
                    //Zonelist 31
                    //Analog 40
                    //Serial Communication 41
                    //Console 50
                    //Acknowledgement 255


                    List<byte[]> bpayloadList = new List<byte[]>();
                    var nextPayload = payload;
                    var recordSize = 0;
                    var nextRecordSize = 0;
                    var nextMessageId = 0;
                    for (var i = 0; i < payload.Length; i++)
                    {
                        if (nextPayload.Length == 0)
                            break;
                        nextMessageId = int.Parse(BitConverter.ToString(nextPayload.Take(1).ToArray()),
                            NumberStyles.HexNumber);
                        if (nextMessageId == 0)
                        {
                            if (nextPayload.Length == 0)
                                return null;
                            //Basic Id 0
                            recordSize = 40;
                            nextRecordSize = recordSize + 1;
                            var basicPayLoad = nextPayload.Take(recordSize).ToArray();
                            var bmessageValueId = basicPayLoad.Take(1);
                            var bTrigger = basicPayLoad.Skip(1).Take(2);
                            var bTriggerReserveValue = basicPayLoad.Skip(3).Take(4);
                            var bStateFlags = basicPayLoad.Skip(7).Take(2);
                            var bLatitude = basicPayLoad.Skip(9).Take(4);
                            var bLongitude = basicPayLoad.Skip(13).Take(4);
                            var bSpeed = basicPayLoad.Skip(17).Take(2);
                            var bDirection = basicPayLoad.Skip(19).Take(2);
                            var bGsmSignal = basicPayLoad.Skip(21).Take(1);
                            var bGpsSignal = basicPayLoad.Skip(22).Take(1);
                            var bBatteryLevel = basicPayLoad.Skip(23).Take(1);
                            var bCurrentTriOdometer = basicPayLoad.Skip(24).Take(2);
                            var bTldReferenceId = basicPayLoad.Skip(26).Take(4);
                            var bBearingToTld = basicPayLoad.Skip(30).Take(2);
                            var bDistanceToTld = basicPayLoad.Skip(32).Take(4);
                            var bFixTimeStamp = basicPayLoad.Skip(36).Take(4);
                            var bReserve = basicPayLoad.Skip(40).Take(1);


                            var latitude = BitConverter.ToInt32(bLatitude.ToArray(), 0) * 0.00001;
                            var longitude = BitConverter.ToInt32(bLongitude.ToArray(), 0) * 0.00001;
                            var speed = BitConverter.ToInt16(bSpeed.ToArray(), 0);
                            var direction = BitConverter.ToInt16(bDirection.ToArray(), 0);

                            var gsmSignal = int.Parse(BitConverter.ToString(bGsmSignal.ToArray()),
                                NumberStyles.HexNumber);
                            var gpsSignal = int.Parse(BitConverter.ToString(bGpsSignal.ToArray()),
                                NumberStyles.HexNumber);
                            var batteryLevel = int.Parse(BitConverter.ToString(bBatteryLevel.ToArray()),
                                NumberStyles.HexNumber);

                            var curentTripOdometer = BitConverter.ToInt16(bCurrentTriOdometer.ToArray(), 0);

                            var tldReferenceId = BitConverter.ToInt32(bTldReferenceId.ToArray().ToArray(), 0);
                            var bearingToTld = BitConverter.ToInt16(bBearingToTld.ToArray(), 0);
                            var distanceToTld = BitConverter.ToInt16(bDistanceToTld.ToArray(), 0);

                            var fixtimeStamp =
                                UnixTimeStampToDateTime(BitConverter.ToInt32(bFixTimeStamp.ToArray().ToArray(), 0));

                            var gpsTime = fixtimeStamp.ToString("yyyy-MM-dd HH:mm:ss");


                            var headergpsTime = headertimeStamp.ToString("yyyy-MM-dd HH:mm:ss");
                            var sequnceNumber = BitConverter.ToInt16(headerSequenceNumber.ToArray(), 0);
                            s_sequenceNumber = sequnceNumber.ToString();

                            //common Events & Ignition States                         

                            var trC = BitConverter.ToInt16(bTrigger.ToArray(), 0);
                            //check  trigger codes

                            if (trC == 0 || trC == 1)
                            {
                                var stateflagsValues = GetRangeStates(bStateFlags);

                                //ignition States
                                if (stateflagsValues == 101)
                                {
                                    //charging
                                    s_eventId = "26";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 102)
                                {
                                    //power connected
                                    s_eventId = "24";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 103)
                                {
                                    //battery low
                                    s_eventId = "28";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 104)
                                {
                                    //no battery
                                    s_eventId = "30";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 105)
                                {
                                    //bad battery
                                    s_eventId = "43";
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;
                                }
                                else if (stateflagsValues == 106)
                                {
                                    //ignition on/off
                                    //check last status
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var status);
                                    s_ignition = status;

                                }
                                else if (stateflagsValues == 107)
                                {
                                    //Shock sensor
                                    s_ignition = true;
                                    s_eventId = "7";
                                }
                                else if (stateflagsValues == 108)
                                {
                                    //parked
                                    s_ignition = false;
                                    s_eventId = "124";
                                }
                                else if (stateflagsValues == 109)
                                {
                                    //moving
                                    s_ignition = true;
                                    s_eventId = "124";


                                }
                                else if (stateflagsValues == 110)
                                {
                                    //Stopped



                                    if (Program.TramigoLastNumber.ContainsKey(imei))
                                    {
                                        Program.TramigoLastNumber.TryGetValue(imei, out var timeOutNumber);

                                        if (timeOutNumber == 1)
                                        {
                                            s_eventId = "124";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 2, 1);
                                        }
                                        else if (timeOutNumber == 2)
                                        {
                                            s_eventId = "124";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 3, 2);
                                        }
                                        else if (timeOutNumber == 3)
                                        {
                                            s_eventId = "124";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 4, 3);
                                        }
                                        else if (timeOutNumber == 4)
                                        {
                                            s_eventId = "124";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 5, 4);
                                        }
                                        else if (timeOutNumber == 5)
                                        {
                                            s_eventId = "8";
                                            s_ignition = true;
                                            Program.TramigoLastNumber.TryUpdate(imei, 6, 5);
                                        }
                                        else
                                        {
                                            s_eventId = "124";
                                            s_ignition = false;
                                        }

                                    }
                                    else
                                    {

                                        Program.TramigoLastNumber.TryAdd(imei, 1);
                                        s_eventId = "124";
                                        s_ignition = true;

                                    }

                                }
                                else if (stateflagsValues == 111)
                                {
                                    //gps on/off
                                    s_eventId = "124";
                                }
                                else if (stateflagsValues == 112)
                                {
                                    //ingition on off
                                    //check last ignition status
                                    Program.LastIgnitionStatusDictionary.TryGetValue(imei, out var value);

                                    if (value)
                                    {
                                        s_eventId = "8";
                                    }
                                    else
                                    {
                                        s_eventId = "7";

                                    }
                                }
                                else
                                {
                                    s_eventId = "124";
                                    if (speed > 1)
                                    {
                                        s_ignition = true;

                                    }
                                    else
                                    {
                                        s_ignition = false;

                                    }


                                }
                            }
                            else if (trC == 2)
                            {
                                //124
                                s_ignition = true;
                                s_eventId = "124";
                            }
                            else if (trC == 10)
                            {
                                //124
                                var stateflagsValues = GetRangeStates(bStateFlags);
                                if (stateflagsValues != 109)
                                {
                                    s_eventId = "7";
                                }
                                else
                                {
                                    s_ignition = true;
                                    s_eventId = "124";
                                }

                                s_ignition = true;
                            }
                            else if (trC == 12)
                            {
                                if (speed > 1)
                                {
                                    s_eventId = "124";
                                }
                                else
                                {
                                    s_eventId = "7";
                                }
                                s_ignition = true;

                            }
                            else if (trC == 11 || trC == 13)
                            {
                                //124
                                s_ignition = false;
                                s_eventId = "8";
                                speed = 0;
                            }
                            else if (trC == 20)
                            {
                                //124

                                //check states for ignition on off/

                                s_eventId = "153";
                                s_ignition = true;
                            }
                            else if (trC == 21)
                            {

                                //124
                                s_ignition = true;
                                s_eventId = "1";
                            }
                            else if (trC == 22)
                            {
                                //124
                                s_ignition = true;
                                s_eventId = "4";
                            }

                            else if (trC == 23)
                            {
                                //124
                                s_ignition = true;
                                s_eventId = "14";

                            }
                            else if (trC == 24)
                            {
                                //124
                                s_ignition = true;
                                s_eventId = "124";
                            }
                            else if (trC == 50)
                            {

                                s_eventId = "22";
                            }
                            else if (trC == 51)
                            {

                                s_eventId = "25";
                            }
                            else if (trC == 52)
                            {

                                s_eventId = "24";
                            }
                            else
                            {
                                if (speed > 1)
                                {
                                    s_ignition = true;
                                }
                                else
                                {
                                    s_ignition = false;
                                }

                                s_eventId = "124";
                            }


                            var currentOdo = curentTripOdometer * 0.001;

                            s_Imei = imei.ToString();
                            s_lon = longitude.ToString(CultureInfo.InvariantCulture);
                            s_lat = latitude.ToString(CultureInfo.InvariantCulture);
                            s_speed = speed.ToString();
                            s_heading = direction.ToString();
                            s_odometer = currentOdo.ToString(CultureInfo.InvariantCulture);
                            s_gps = gpsSignal.ToString();
                            s_gsm = gsmSignal.ToString();
                            s_battery = batteryLevel.ToString();
                            s_gps_fixtimestamp = headergpsTime.ToString();



                        }
                        else if (nextMessageId == 1)
                        {
                            //Message Id 1 variable length.
                            var vlength = int.Parse(BitConverter.ToString(nextPayload.Skip(1).Take(1).ToArray()),
                                NumberStyles.HexNumber);
                            recordSize = vlength;
                            nextRecordSize = recordSize;
                            var tramigolandMarkData = nextPayload.Take(vlength).ToArray();
                            var vTldLanguage = tramigolandMarkData.Skip(3).Take(2);
                            var vEncodingCompresion = tramigolandMarkData.Skip(5).Take(1);
                            var vclassTypeReserve = tramigolandMarkData.Skip(6).Take(1);
                            var vtldReferenceId = tramigolandMarkData.Skip(7).Take(4);
                            var vName1Length = tramigolandMarkData.Skip(11).Take(2);
                            var vName2Length = tramigolandMarkData.Skip(13).Take(2);
                            var vName3Length = tramigolandMarkData.Skip(15).Take(2);
                            var vCountryCode = tramigolandMarkData.Skip(17).Take(2);
                            var unknownLength = tramigolandMarkData.Skip(19);

                            var countryCode = Encoding.ASCII.GetString(vCountryCode.ToArray());
                            var textMessage = Encoding.ASCII.GetString(unknownLength.ToArray());

                            s_IgnoreGeoCode = false;
                            s_countryCode = countryCode;
                            s_textMessage = textMessage;

                            Program.TramigoLastAddressLocationDictionary.AddOrUpdate(imei, textMessage,
                                (key, value) => textMessage);
                        }
                        else if (nextMessageId == 10)
                        {
                            //Compact location

                            recordSize = 13;
                            nextRecordSize = recordSize;
                            var compactLocationByte = nextPayload.Take(recordSize).ToArray();
                            var lat = compactLocationByte.Skip(1).Take(4);
                            var lon = compactLocationByte.Skip(5).Take(4);
                            var speed = compactLocationByte.Skip(9).Take(2);
                            var direction = compactLocationByte.Skip(11).Take(2);
                        }

                        else if (nextMessageId == 11)
                        {
                            //Compact Heartbeat
                            //all units in percentage
                            recordSize = 4;
                            nextRecordSize = recordSize;
                            var compactHeartBeat = nextPayload.Take(recordSize).ToArray();
                            var hgsmSignalQuality = compactHeartBeat.Skip(1).Take(1);
                            var hgpsSignalQuality = compactHeartBeat.Skip(2).Take(1);
                            var hbatteryLevel = compactHeartBeat.Skip(3).Take(1);
                        }

                        else if (nextMessageId == 20)
                        {

                            //Extended to be added

                        }

                        else if (nextMessageId == 40)
                        {

                            //Analog/One Wire
                            var oneWireBytes = nextPayload.Take(41).ToArray();
                            var driverId = oneWireBytes.Skip(1).Take(8);
                            var tempSensor1Id = oneWireBytes.Skip(9).Take(8);
                            var tempSensor1Value = oneWireBytes.Skip(17).Take(2);

                            var tempSensor2Id = oneWireBytes.Skip(19).Take(8);
                            var tempSensor2Value = oneWireBytes.Skip(27).Take(2);

                            var tempSensor3Id = oneWireBytes.Skip(29).Take(8);
                            var tempSensor3Value = oneWireBytes.Skip(37).Take(2);
                            var analogueData = oneWireBytes.Skip(39).Take(2);

                        }
                        else
                        {
                            //check messageId
                            var messageid = nextMessageId;
                        }

                        var bRecord = nextPayload.Take(recordSize).ToArray();
                        nextPayload = nextPayload.Skip(nextRecordSize).ToArray();
                        bpayloadList.Add(bRecord);
                    }



                    if (string.IsNullOrEmpty(s_textMessage))
                    {
                        //uses inbuilt addressing
                        Program.TramigoLastAddressLocationDictionary.TryGetValue(imei, out var message);
                        s_textMessage = message;
                    }

                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        //        // Create the XML Declaration, and append it to XML document
                        XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
                        doc.AppendChild(dec);

                        // Create the root element
                        XmlElement root = doc.CreateElement("Tramigo_Message");
                        doc.AppendChild(root);

                        XmlElement xImei = doc.CreateElement("device_Id");
                        xImei.InnerText = s_Imei;
                        root.AppendChild(xImei);


                        XmlElement xGpsUtc = doc.CreateElement("GPSDateTime");
                        xGpsUtc.InnerText = s_gps_fixtimestamp.ToString();
                        root.AppendChild(xGpsUtc);


                        XmlElement xLat = doc.CreateElement("Lat");
                        xLat.InnerText = s_lat.ToString(CultureInfo.InvariantCulture);
                        root.AppendChild(xLat);

                        XmlElement xlon = doc.CreateElement("Lon");
                        xlon.InnerText = s_lon.ToString(CultureInfo.InvariantCulture);
                        root.AppendChild(xlon);

                        var xSpd = doc.CreateElement("Speed");
                        xSpd.InnerText = s_speed;
                        root.AppendChild(xSpd);

                        XmlElement xHead = doc.CreateElement("Heading");
                        xHead.InnerText = s_heading;
                        root.AppendChild(xHead);

                        XmlElement xEvent = doc.CreateElement("event_id");
                        xEvent.InnerText = s_eventId;
                        root.AppendChild(xEvent);


                        XmlElement xgpStatus = doc.CreateElement("GpStatus");
                        xgpStatus.InnerText = s_gps;
                        root.AppendChild(xgpStatus);


                        XmlElement xgpgsm = doc.CreateElement("Gsm");
                        xgpgsm.InnerText = s_gsm;
                        root.AppendChild(xgpgsm);

                        //XmlElement xOdometer = doc.CreateElement("Odometer");
                        //xOdometer.InnerText = s_odometer;
                        //root.AppendChild(xOdometer);

                        XmlElement xIgn = doc.CreateElement("Ign_Status");
                        xIgn.InnerText = s_ignition.ToString();
                        root.AppendChild(xIgn);

                        XmlElement xiVoltage = doc.CreateElement("battery");
                        xiVoltage.InnerText = s_battery;
                        root.AppendChild(xiVoltage);

                        XmlElement xSequenceNumber = doc.CreateElement("sequenceNumber");
                        xSequenceNumber.InnerText = s_sequenceNumber;
                        root.AppendChild(xSequenceNumber);
                        //text position

                        XmlElement xcountryCode = doc.CreateElement("countryCode");
                        xcountryCode.InnerText = s_countryCode;
                        root.AppendChild(xcountryCode);

                        XmlElement xGeonamesenabled = doc.CreateElement("IgnoreGeoCode");
                        xGeonamesenabled.InnerText = Convert.ToBoolean(s_IgnoreGeoCode).ToString();
                        root.AppendChild(xGeonamesenabled);


                        XmlElement xtextMessage = doc.CreateElement("textMessage");
                        xtextMessage.InnerText = s_textMessage;
                        root.AppendChild(xtextMessage);


                        XmlElement xsoftwareOdo = doc.CreateElement("useSoftwareOdo");
                        bool useSoftOdo = true;
                        xsoftwareOdo.InnerText = useSoftOdo.ToString();
                        root.AppendChild(xsoftwareOdo);

                        var rawData = BitConverter.ToString(vm).Replace("-", "");
                        XmlElement xraw = doc.CreateElement("raw_data");
                        xraw.InnerText = rawData;
                        root.AppendChild(xraw);
                        string xmlOutput = doc.OuterXml;
                        var viewXml = xmlOutput;


                        #region DictionaryUpadte

                       
                        var lastDetails = new TramigoLastLocationStatus
                        {
                            GpsDateTime = s_gps_fixtimestamp,
                            TextMessage = s_textMessage,
                            IgnitionStatus = s_ignition,
                            Latitude = s_lat,
                            Longitude = s_lon,
                            LastEventStatus = s_eventId,
                            RawData = BitConverter.ToString(vm)
                        };
                        Program.TramigoLastLocationDictionary.
                            AddOrUpdate(imei, lastDetails, (key, value) => lastDetails);
                        #endregion

                        
                    }
                    catch (Exception ex)
                    {
                       
                    }




                }
            }
            catch (Exception ex)
            {
                
            }
            return s_Imei;

        }
        public string ProcessTramigoLegacy(string hexData)
        {

            CustomExtensions custom = new CustomExtensions();
            var vimei = "";
            var vlat = "";
            var vlong = "";
            var vgpsdate = "";
            var vspeed = "";
            var vOdometer = "";
            var vevent = "";
            var vignition = false;
            var vcountryCode = "";
            var vtextMessage = "";
            var vheading = "";
            var vgsm = "";
            var vbattery = "";
            var vgps = "";


            try
            {
                byte[] tramigoDataBytes = HexStringToByteArray(hexData.Replace("-", ""));

                List<byte[]> bMessageHeaderProtocolList = new List<byte[]>();
                var nextheaderPayload = tramigoDataBytes;
                for (int i = 0; i < tramigoDataBytes.Length; i++)
                {
                    if (nextheaderPayload.Length == 0)
                        break;
                    int nextDataSize = BitConverter.ToInt16(nextheaderPayload.Skip(6).Take(2).ToArray(), 0);
                    var bRecord = nextheaderPayload.Take(nextDataSize).ToArray();
                    nextheaderPayload = nextheaderPayload.Skip(nextDataSize).ToArray();
                    bMessageHeaderProtocolList.Add(bRecord);
                }
                foreach (var vm in bMessageHeaderProtocolList)
                {
                    var payloadHeader = vm.Take(20).ToArray();
                    var hprotocolVersion = payloadHeader.Take(1);
                    var hversionId = payloadHeader.Skip(1).Take(1);
                    var hsequenceNumber = payloadHeader.Skip(2).Take(2);
                    var hmessageId = payloadHeader.Skip(4).Take(2);
                    var hpacketLength = payloadHeader.Skip(6).Take(2);
                    var hstatus = payloadHeader.Skip(8).Take(2);
                    var paylodChecksum = payloadHeader.Skip(10).Take(2);
                    var hsenderId = payloadHeader.Skip(12).Take(4);
                    var hmessageTimestamp = payloadHeader.Skip(6).Take(4);

                    var sValueAsInt = long.Parse(BitConverter.ToString(hsenderId.ToArray()).Replace("-", ""), System.Globalization.NumberStyles.HexNumber);
                    vimei = sValueAsInt.ToString();
                    //  var headertimeStamp = UnixTimestampToDateTimeNew(BitConverter.ToInt32(hmessageTimestamp.ToArray(), 0));
                    var payloadBody = vm.Skip(20);
                    var pdata = System.Text.Encoding.ASCII.GetString(payloadBody.ToArray());
                    var data = pdata.Split(new string[] { " EOF" }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var plainText in data)
                    {                        

                        if (plainText.Contains("Status"))
                        {
                            var indexOfGps = plainText.IndexOf("GPS", StringComparison.Ordinal);
                            var indexOfGsm = plainText.IndexOf("GSM", StringComparison.Ordinal);
                            var indexOfBattery = plainText.IndexOf("battery", StringComparison.Ordinal);
                            vgps = plainText.Substring(indexOfGps + 5, 2);
                            vgsm = plainText.Substring(indexOfGsm + 5, 2);
                            vbattery = plainText.Substring(indexOfBattery + 9, 2);

                            int.TryParse(vgps, out int value);


                            var tramigoStatus = new TramigoStatus
                            {
                                Battery = vbattery,
                                Gps = value.ToString(),
                                Gsm = vgsm
                            };


                            bool imeiExist = Program.TramigoDictionary.ContainsKey(sValueAsInt);
                            if (imeiExist)
                            {
                                //abort status message here
                                Program.TramigoDictionary[sValueAsInt] = tramigoStatus;
                                return null;
                            }
                            else
                            {

                                Program.TramigoDictionary.TryAdd(sValueAsInt, tramigoStatus);
                                //abort status message                 
                                return null;

                            }


                        }

                        else
                        {

                            if (plainText.Contains("Ignition on detected"))
                            {
                                vevent = "7";
                                vignition = true;
                            }
                            else if (plainText.Contains("Trip started"))
                            {
                                vevent = "7";
                                vignition = true;
                            }
                            else if (plainText.Contains("Trip report"))
                            {
                                vevent = "8";
                                vignition = false;
                            }
                            else if (plainText.Contains("Ignition off detected"))
                            {
                                vevent = "8";
                                vignition = false;

                            }

                            else if (plainText.Contains("Moving"))
                            {
                                vignition = true;
                                vevent = "124";

                            }

                            else if (plainText.Contains("Parked"))
                            {
                                vevent = "124";
                                vignition = false;
                                Program.TramigoLastNumber.TryRemove(sValueAsInt, out var _);
                            }
                            else if (plainText.Contains("Stopped"))
                            {
                                if (Program.TramigoLastNumber.ContainsKey(sValueAsInt))
                                {
                                    Program.TramigoLastNumber.TryGetValue(sValueAsInt, out var timeOutNumber);

                                    if (timeOutNumber == 1)
                                    {
                                        vevent = "128";
                                        vignition = true;
                                        Program.TramigoLastNumber.TryUpdate(sValueAsInt, 2, 1);
                                    }
                                    else if (timeOutNumber == 2)
                                    {
                                        vevent = "128";
                                        vignition = true;
                                        Program.TramigoLastNumber.TryUpdate(sValueAsInt, 3, 2);
                                    }
                                    else if (timeOutNumber == 3)
                                    {
                                        vevent = "128";
                                        vignition = true;
                                        Program.TramigoLastNumber.TryUpdate(sValueAsInt, 4, 3);
                                    }
                                    else if (timeOutNumber == 4)
                                    {
                                        vevent = "128";
                                        vignition = true;
                                        Program.TramigoLastNumber.TryUpdate(sValueAsInt, 5, 4);
                                    }
                                    else if (timeOutNumber == 5)
                                    {
                                        vevent = "8";
                                        vignition = true;
                                        Program.TramigoLastNumber.TryUpdate(sValueAsInt, 6, 5);
                                    }
                                    else
                                    {
                                        vevent = "124";
                                        vignition = false;
                                    }

                                }
                                else
                                {

                                    Program.TramigoLastNumber.TryAdd(sValueAsInt, 1);
                                    vevent = "128";
                                    vignition = true;

                                }
                            }

                            else if (plainText.Contains("Speed limit detected"))
                            {
                                vevent = "1";
                                vignition = true;
                            }
                            else if (plainText.Contains("panic button detected"))
                            {
                                vevent = "153";
                                if (plainText.Contains("moving"))
                                {
                                    vignition = true;
                                }
                                else
                                {
                                    vignition = false;
                                }
                            }

                            else if (plainText.Contains("low power"))
                            {
                                vevent = "22";
                                vignition = true;
                            }

                            else if (plainText.Contains("low power"))
                            {
                                vevent = "22";
                                vignition = true;
                            }
                            else if (plainText.Contains("Ignition (off)"))
                            {
                                vevent = "8";
                                vignition = false;
                            }
                            else if (plainText.Contains("Ignition (on)"))
                            {
                                vevent = "7";
                                vignition = true;
                            }
                            else
                            {
                                vevent = "124";
                                vignition = false;
                            }

                            var coordinates = CustomExtensions.GetCoordinates(plainText);
                            if (coordinates.Length == 0)
                                return null;
                            vlat = coordinates[0];
                            vlong = coordinates[1];

                            vgpsdate = CustomExtensions.GetFixTimestampProtocol80(plainText);

                            var results = custom.GetSpeedCourse(plainText);
                            if (results.Length == 0)
                            {
                                vspeed = "0";
                                vheading = "0";
                                //read from dictionary
                            }
                            else
                            {
                                vheading = results[0];
                                vspeed = results[1];
                            }

                            //get street address
                            var textValue = plainText.GetTextPositionBetween("of", vlat);

                            vtextMessage = textValue;
                            s_countryCode = textValue.Split(',').Last();

                            //read battery status

                            Program.TramigoDictionary.TryGetValue(sValueAsInt, out var vStatus);

                            if (vStatus != null)
                            {
                                vgps = vStatus.Gps;
                                vbattery = vStatus.Battery;
                                vgsm = vStatus.Gsm;
                            }
                            else
                            {
                                vgps = "";
                                vbattery = "";
                                vgsm = "";
                            }


                            #region xmlData

                            //send xml here
                            s_Imei = sValueAsInt.ToString();
                            s_lat = vlat;
                            s_lon = vlong;
                            s_gps_fixtimestamp = vgpsdate;
                            s_odometer = vOdometer;
                            s_eventId = vevent;
                            s_ignition = vignition;
                            s_countryCode = vcountryCode;
                            s_textMessage = vtextMessage;
                            s_battery = vbattery;
                            s_heading = vheading;
                            s_speed = vspeed;
                            s_gsm = vgsm;
                            s_gps = vgps;



                            //create xml and send
                            XmlDocument doc = new XmlDocument();
                            // Create the XML Declaration, and append it to XML document
                            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
                            doc.AppendChild(dec);

                            // Create the root element
                            XmlElement root = doc.CreateElement("Tramigo_Message");
                            doc.AppendChild(root);


                            XmlElement xImei = doc.CreateElement("device_Id");
                            xImei.InnerText = s_Imei;
                            root.AppendChild(xImei);


                            XmlElement xGpsUtc = doc.CreateElement("GPSDateTime");
                            xGpsUtc.InnerText = s_gps_fixtimestamp;
                            root.AppendChild(xGpsUtc);



                            XmlElement xLat = doc.CreateElement("Lat");
                            xLat.InnerText = s_lat.ToString(CultureInfo.InvariantCulture);
                            root.AppendChild(xLat);

                            XmlElement xlon = doc.CreateElement("Lon");
                            xlon.InnerText = s_lon.ToString(CultureInfo.InvariantCulture);
                            root.AppendChild(xlon);




                            var xSpd = doc.CreateElement("Speed");
                            xSpd.InnerText = s_speed;
                            root.AppendChild(xSpd);

                            XmlElement xHead = doc.CreateElement("Heading");
                            xHead.InnerText = s_heading;
                            root.AppendChild(xHead);

                            XmlElement xEvent = doc.CreateElement("event_id");
                            xEvent.InnerText = s_eventId;
                            root.AppendChild(xEvent);


                            XmlElement xOdometer = doc.CreateElement("Odometer");
                            xOdometer.InnerText = s_odometer;
                            root.AppendChild(xOdometer);

                            XmlElement xIgn = doc.CreateElement("Ign_Status");
                            xIgn.InnerText = s_ignition.ToString();
                            root.AppendChild(xIgn);

                            XmlElement xiVoltage = doc.CreateElement("battery");
                            xiVoltage.InnerText = s_battery;
                            root.AppendChild(xiVoltage);

                            XmlElement xgpStatus = doc.CreateElement("GpStatus");
                            xgpStatus.InnerText = s_gps;
                            root.AppendChild(xgpStatus);


                            XmlElement xgpgsm = doc.CreateElement("Gsm");
                            xgpgsm.InnerText = s_gsm;
                            root.AppendChild(xgpgsm);

                            //text position
                            string txtpos = string.Empty;
                            txtpos = s_textMessage;
                            XmlElement xTextPos = doc.CreateElement("textMessage");
                            xTextPos.InnerText = txtpos;
                            root.AppendChild(xTextPos);
                            XmlElement xcountryCode = doc.CreateElement("countryCode");
                            xcountryCode.InnerText = s_countryCode;
                            root.AppendChild(xcountryCode);

                            XmlElement xGeonamesenabled = doc.CreateElement("IgnoreGeoCode");
                            xGeonamesenabled.InnerText = Convert.ToBoolean(s_IgnoreGeoCode).ToString();
                            root.AppendChild(xGeonamesenabled);

                            XmlElement xsoftwareOdo = doc.CreateElement("useSoftwareOdo");

                            xsoftwareOdo.InnerText = true.ToString();
                            root.AppendChild(xsoftwareOdo);

                            XmlElement xraw = doc.CreateElement("raw_data");

                            var raw = BitConverter.ToString(vm);
                            xraw.InnerText = "*" + raw.Replace("-", "") + "*";
                            root.AppendChild(xraw);
                            string xmlOutput = doc.OuterXml;


                            Program.LastIgnitionStatusDictionary.AddOrUpdate(sValueAsInt, s_ignition,
                                (key, value) => s_ignition);


                           

                            var tramigoLastLocationStatus =
                                new TramigoLastLocationStatus
                                {
                                    RawData = BitConverter.ToString(vm),
                                    GpsDateTime = s_gps_fixtimestamp,
                                    TextMessage = s_textMessage,
                                    Latitude = s_lat,
                                    Longitude = s_lon
                                };
                           



                            var viewXml = xmlOutput;
                            var viewData = viewXml;
                           

                            #endregion
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                
            }

            return s_Imei;
        }

        void SendInstantData(string sValueAsInt, string vlat, string vlong, string vgpsdate, string vOdometer, string vevent, bool vignition,
            string vtextMessage, string vbattery, string vheading, string vspeed, string vgsm, string vgps, string raw)
        {

            long.TryParse(vevent, out var passableValue);



            #region xmlData
            //send xml here
            s_Imei = sValueAsInt.ToString();
            s_lat = vlat;
            s_lon = vlong;
            s_gps_fixtimestamp = vgpsdate;
            s_odometer = vOdometer;
            s_eventId = vevent;
            s_ignition = vignition;
            s_textMessage = vtextMessage;
            s_battery = vbattery;
            s_heading = vheading;
            s_speed = vspeed;
            s_gsm = vgsm;
            s_gps = vgps;



            //create xml and send
            XmlDocument doc = new XmlDocument();
            // Create the XML Declaration, and append it to XML document
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            XmlElement root = doc.CreateElement("Tramigo_Message");
            doc.AppendChild(root);


            XmlElement xImei = doc.CreateElement("device_Id");
            xImei.InnerText = s_Imei;
            root.AppendChild(xImei);


            XmlElement xGpsUtc = doc.CreateElement("GPSDateTime");
            xGpsUtc.InnerText = s_gps_fixtimestamp;
            root.AppendChild(xGpsUtc);



            XmlElement xLat = doc.CreateElement("Lat");
            xLat.InnerText = s_lat.ToString(CultureInfo.InvariantCulture);
            root.AppendChild(xLat);

            XmlElement xlon = doc.CreateElement("Lon");
            xlon.InnerText = s_lon.ToString(CultureInfo.InvariantCulture);
            root.AppendChild(xlon);




            var xSpd = doc.CreateElement("Speed");
            xSpd.InnerText = s_speed;
            root.AppendChild(xSpd);

            XmlElement xHead = doc.CreateElement("Heading");
            xHead.InnerText = s_heading;
            root.AppendChild(xHead);

            XmlElement xEvent = doc.CreateElement("event_id");
            xEvent.InnerText = s_eventId;
            root.AppendChild(xEvent);


            XmlElement xOdometer = doc.CreateElement("Odometer");
            xOdometer.InnerText = s_odometer;
            root.AppendChild(xOdometer);

            XmlElement xIgn = doc.CreateElement("Ign_Status");
            xIgn.InnerText = s_ignition.ToString();
            root.AppendChild(xIgn);

            XmlElement xiVoltage = doc.CreateElement("battery");
            xiVoltage.InnerText = s_battery;
            root.AppendChild(xiVoltage);

            XmlElement xgpStatus = doc.CreateElement("GpStatus");
            xgpStatus.InnerText = s_gps;
            root.AppendChild(xgpStatus);


            XmlElement xgpgsm = doc.CreateElement("Gsm");
            xgpgsm.InnerText = s_gsm;
            root.AppendChild(xgpgsm);

            //text position
            string txtpos = string.Empty;
            txtpos = s_textMessage;
            XmlElement xTextPos = doc.CreateElement("textMessage");
            xTextPos.InnerText = txtpos;
            root.AppendChild(xTextPos);
            XmlElement xcountryCode = doc.CreateElement("countryCode");
            xcountryCode.InnerText = s_countryCode;
            root.AppendChild(xcountryCode);

            XmlElement xGeonamesenabled = doc.CreateElement("IgnoreGeoCode");
            xGeonamesenabled.InnerText = Convert.ToBoolean(s_IgnoreGeoCode).ToString();
            root.AppendChild(xGeonamesenabled);

            XmlElement xsoftwareOdo = doc.CreateElement("useSoftwareOdo");

            xsoftwareOdo.InnerText = true.ToString();
            root.AppendChild(xsoftwareOdo);

            XmlElement xraw = doc.CreateElement("raw_data");


            xraw.InnerText = "*" + raw.Replace("-", "") + "*";
            root.AppendChild(xraw);
            string xmlOutput = doc.OuterXml;

            long.TryParse(sValueAsInt, out var svalue);
            Program.LastIgnitionStatusDictionary.
                AddOrUpdate(svalue, s_ignition, (key, value) => s_ignition);


            
            #endregion

        }


        public string ProcessTramigoMessageV1(string xdata)
        {
            byte[] v1ByteArray = HexStringToByteArray(xdata.Replace("-", ""));
            byte[] header = v1ByteArray.Take(20).ToArray();
            byte[] payload = v1ByteArray.Skip(20).ToArray();

            var payloadBody = Common.SplitArray(payload, 162).ToArray();

            foreach (var vm in payloadBody)
            {

                var headerProtocol = header.Take(1);
                var headerVersionId = header.Skip(1).Take(1);

                var headerSequenceNumber = header.Skip(2).Take(2);
                var headerMessageId = header.Skip(4).Take(2);
                var headerPacketLength = header.Skip(6).Take(2);
                var headerStatus = header.Skip(8).Take(2);

                var ss = BitConverter.ToString(headerStatus.ToArray());
                var headerpayloadChecksum = header.Skip(10).Take(2);

                var headerSenderId = header.Skip(12).Take(4);
                var headerMessageTimesamp = header.Skip(16).Take(4);


                var imei = BitConverter.ToInt32(headerSenderId.ToArray(), 0);
                var headertimeStamp = UnixTimeStampToDateTime(BitConverter.ToInt32(headerMessageTimesamp.ToArray().ToArray(), 0));


                var basicPayLoad = vm.Take(64).ToArray();
                var additionalPayload = vm.Skip(64).ToArray();

                var breportTrigger = basicPayLoad.Take(2).ToArray();


                int value = BitConverter.ToInt16(breportTrigger, 0);
                var bStateFlags = basicPayLoad.Skip(2).Take(2);
                var bLatitude = basicPayLoad.Skip(4).Take(4);
                var bLongitude = basicPayLoad.Skip(8).Take(4);

                var bgsm = basicPayLoad.Skip(12).Take(2);
                var bsatelitesInFix = basicPayLoad.Skip(14).Take(2);
                var bsatelitesInTrack = basicPayLoad.Skip(16).Take(2);
                var bgpsAntenna = basicPayLoad.Skip(18).Take(2);

                var bSpeed = basicPayLoad.Skip(20).Take(2);
                var bDirection = basicPayLoad.Skip(22).Take(2);
                var bDistance = basicPayLoad.Skip(24).Take(4);

                var bBatteryVoltage = basicPayLoad.Skip(28).Take(2);
                var bBatteryChargerStatus = basicPayLoad.Skip(30).Take(2);

                var bFixTimeStamp = basicPayLoad.Skip(32).Take(4);

                var bStatusFlags = basicPayLoad.Skip(36).Take(2);
                var nn = BitConverter.ToString(bStateFlags.ToArray());
                var bGsmMcc = basicPayLoad.Skip(38).Take(2);
                var bGsmMnc = basicPayLoad.Skip(40).Take(2);
                var bGsmLac = basicPayLoad.Skip(42).Take(2);
                var bGsmCid = basicPayLoad.Skip(44).Take(2);
                var bGsmNetworkStatus = basicPayLoad.Skip(46).Take(2);
                var bGsmModuleTemperature = basicPayLoad.Skip(48).Take(2);
                var bMaxSpeed = basicPayLoad.Skip(50).Take(2);
                var bMinSpeed = basicPayLoad.Skip(52).Take(2);
                var bGpsSignalQuality = basicPayLoad.Skip(54).Take(1);
                var bGsmSignalQuality = basicPayLoad.Skip(55).Take(1);

                var btldReferenceLocvationId = basicPayLoad.Skip(56).Take(2);
                var bBearingToReference = basicPayLoad.Skip(58).Take(2);
                var bDistanceToReference = basicPayLoad.Skip(60).Take(14);

                //additional

                if (additionalPayload.Count() != 0)
                {
                    var name1 = additionalPayload.Take(1);
                    var name2 = additionalPayload.Skip(1).Take(1);
                    var name3 = additionalPayload.Skip(2).Take(1);
                    var classByte = additionalPayload.Skip(3).Take(1);
                    var uCS2 = additionalPayload.Skip(4).Take(1);
                    var reserved = additionalPayload.Skip(5).Take(1);
                    var country = additionalPayload.Skip(6).Take(2);
                    var namev1 = additionalPayload.Skip(8).Take(40);
                    var namev2 = additionalPayload.Skip(48).Take(30);
                    var namev3 = additionalPayload.Skip(78).Take(20);

                }
                //intergration..

                var latitude = BitConverter.ToInt32(bLatitude.ToArray(), 0) * 0.0000001;
                var longitude = BitConverter.ToInt32(bLongitude.ToArray(), 0) * 0.0000001;
                var speed = BitConverter.ToInt16(bSpeed.ToArray(), 0);
                var direction = BitConverter.ToInt16(bDirection.ToArray(), 0);
                var battery = BitConverter.ToInt16(bBatteryVoltage.ToArray(), 0);

                var timestamp = UnixTimeStampToDateTime(BitConverter.ToInt32(bFixTimeStamp.ToArray(), 0));



                var gsmSignal = int.Parse(BitConverter.ToString(bGsmSignalQuality.ToArray()), NumberStyles.HexNumber);
                var gpsSignal = int.Parse(BitConverter.ToString(bGpsSignalQuality.ToArray()), NumberStyles.HexNumber);



                var headergpsTime = headertimeStamp.ToString("yyyy-MM-dd HH:mm:ss");


                var reportTiggers = BitConverter.ToInt16(breportTrigger.ToArray(), 0);

                var bitCommon = BitConverter.ToString(breportTrigger.ToArray());

                var statusFlags = BitConverter.ToString(bStatusFlags.ToArray());

                var flagsBitPositions = GetBinary(statusFlags.Replace("-", ""));

                var pTripInProgress = flagsBitPositions.Substring(0, 1);
                var pAccellerometerMotionActive = flagsBitPositions.Substring(1, 1);
                var pshockSensorMotionActive = flagsBitPositions.Substring(2, 1);
                var pAccellerometerCommunicationError = flagsBitPositions.Substring(13, 1);
                var pTempWarning = flagsBitPositions.Substring(14, 1);
                var pBatteryLow = flagsBitPositions.Substring(15, 1);

                var vbattery = 0.025 * battery;
                if (pTripInProgress == "1")
                {
                    s_eventId = "124";
                }
                if (pAccellerometerMotionActive == "1")
                {
                    s_ignition = true;
                }
                else
                {
                    s_ignition = false;
                }
                if (pshockSensorMotionActive == "1")
                {
                    s_ignition = true;
                }

                if (pTempWarning == "1")
                {

                }

                if (pBatteryLow == "1")
                {

                }

                var code = reportTiggers;

                switch (code)
                {
                    case 1:

                        s_eventId = "124";
                        break;
                    case 3:
                        //travelled distance
                        s_eventId = "124";
                        break;
                    case 4:
                        //Timer
                        s_eventId = "124";
                        break;
                    case 5:
                        //shock Sensor
                        s_ignition = true;
                        s_eventId = "2";
                        break;
                    case 6:
                        //temp alert
                        s_eventId = "124";
                        break;
                    case 7:
                        //gsm operator changed                      
                        s_eventId = "124";
                        break;
                    case 8:
                        //gsm cell Id changed
                        s_ignition = true;
                        s_eventId = "124";
                        break;
                    case 9:
                        //battery low
                        s_ignition = true;
                        s_eventId = "22";
                        break;
                    case 10:
                        //charger status changed
                        s_ignition = true;
                        s_eventId = "124";
                        break;
                    case 11:
                        //gps antenna ststus changed
                        s_ignition = true;
                        s_eventId = "124";
                        break;

                    case 15:
                        //battery power
                        //  s_ignition = true;
                        s_eventId = "124";
                        break;
                    case 16:
                        //external power
                        s_ignition = true;
                        s_eventId = "124";
                        break;
                    case 17:
                        //Battery Ok
                        s_ignition = true;
                        s_eventId = "124";
                        break;
                    case 18:
                        //gsm cell Id changed
                        s_ignition = true;
                        s_eventId = "7";
                        break;
                    case 19:
                        //accellerometer motion start
                        s_ignition = true;
                        s_eventId = "7";
                        break;
                    case 20:
                        //accellerometer motion end
                        s_ignition = true;
                        s_eventId = "8";
                        break;
                    case 21:
                        //gsm cell Id changed
                        s_ignition = true;
                        s_eventId = "7";
                        break;
                    case 22:
                        s_eventId = "8";
                        break;
                    case 23:
                        //shock sensor motion start
                        s_ignition = true;
                        s_eventId = "3";
                        break;
                    case 24:
                        //shock sensor motion end
                        s_ignition = false;
                        s_eventId = "39";
                        break;
                    case 25:
                        //pinning active
                        //  s_ignition = true;
                        // s_eventId = "7";
                        break;
                    case 26:
                        //pinning inactive
                        //s_ignition = true;
                        // s_eventId = "7";
                        break;
                    case 27:
                        //gsm registration status changed
                        //s_ignition = true;
                        s_eventId = "124";
                        break;
                    case 28:
                        //user interface action
                        //     s_ignition = true;
                        s_eventId = "124";
                        break;

                    case 29:
                        s_ignition = false;
                        s_eventId = "8";
                        break;
                    case 30:
                        s_ignition = true;
                        s_eventId = "7";
                        break;
                    case 31:
                        //crash detected
                        s_ignition = true;
                        s_eventId = "20";
                        break;

                    case 33:
                        //gps fix lost
                        //   s_ignition = true;
                        s_eventId = "124";
                        break;
                    case 34:
                        //gps  fix acquired
                        // s_ignition = true;
                        s_eventId = "124";
                        break;
                    case 35:

                        //gps first fix
                        //  s_ignition = true;
                        s_eventId = "124";
                        break;
                    default:
                        s_eventId = "124";
                        break;
                }

                s_Imei = imei.ToString();
                s_lon = longitude.ToString();
                s_lat = latitude.ToString();
                s_speed = speed.ToString();
                s_heading = direction.ToString();
                s_gps = gpsSignal.ToString();
                s_gsm = gsmSignal.ToString();
                s_message_timestamp = headergpsTime;
                s_battery = vbattery.ToString();
                // s_gps_fixtimestamp = gpsTime;



                //create xml here and send


                try
                {
                    XmlDocument doc = new XmlDocument();
                    //        // Create the XML Declaration, and append it to XML document
                    XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
                    doc.AppendChild(dec);

                    // Create the root element
                    XmlElement root = doc.CreateElement("Tramigo_Message");
                    doc.AppendChild(root);

                    XmlElement xImei = doc.CreateElement("device_Id");
                    xImei.InnerText = s_Imei;
                    root.AppendChild(xImei);


                    XmlElement xGpsUtc = doc.CreateElement("GPSDateTime");
                    xGpsUtc.InnerText = timestamp.ToString();
                    root.AppendChild(xGpsUtc);


                    XmlElement xLat = doc.CreateElement("Lat");
                    xLat.InnerText = s_lat.ToString(CultureInfo.InvariantCulture);
                    root.AppendChild(xLat);

                    XmlElement xlon = doc.CreateElement("Lon");
                    xlon.InnerText = s_lon.ToString(CultureInfo.InvariantCulture);
                    root.AppendChild(xlon);

                    var xSpd = doc.CreateElement("Speed");
                    xSpd.InnerText = s_speed;
                    root.AppendChild(xSpd);

                    XmlElement xHead = doc.CreateElement("Heading");
                    xHead.InnerText = s_heading;
                    root.AppendChild(xHead);

                    XmlElement xEvent = doc.CreateElement("event_id");
                    xEvent.InnerText = s_eventId;
                    root.AppendChild(xEvent);


                    XmlElement xgpStatus = doc.CreateElement("GpStatus");
                    xgpStatus.InnerText = s_gps;
                    root.AppendChild(xgpStatus);


                    XmlElement xgpgsm = doc.CreateElement("Gsm");
                    xgpgsm.InnerText = s_gsm;
                    root.AppendChild(xgpgsm);

                    XmlElement xOdometer = doc.CreateElement("Odometer");
                    xOdometer.InnerText = s_odometer;
                    root.AppendChild(xOdometer);

                    XmlElement xIgn = doc.CreateElement("Ign_Status");
                    xIgn.InnerText = s_ignition.ToString();
                    root.AppendChild(xIgn);

                    XmlElement xiVoltage = doc.CreateElement("battery");
                    xiVoltage.InnerText = s_battery;
                    root.AppendChild(xiVoltage);

                    //text position
                    string txtpos = string.Empty;

                    XmlElement xcountryCode = doc.CreateElement("countryCode");
                    xcountryCode.InnerText = s_countryCode;
                    root.AppendChild(xcountryCode);

                    XmlElement xGeonamesenabled = doc.CreateElement("IgnoreGeoCode");
                    xGeonamesenabled.InnerText = Convert.ToBoolean(s_IgnoreGeoCode).ToString();
                    root.AppendChild(xGeonamesenabled);

                    XmlElement xTextPos = doc.CreateElement("textMessage");
                    xTextPos.InnerText = txtpos;
                    root.AppendChild(xTextPos);

                    XmlElement xsoftwareOdo = doc.CreateElement("useSoftwareOdo");
                    bool useSoftOdo = true;
                    xsoftwareOdo.InnerText = useSoftOdo.ToString();
                    root.AppendChild(xsoftwareOdo);


                    XmlElement xSequenceNumber = doc.CreateElement("sequenceNumber");
                    xSequenceNumber.InnerText = "";
                    root.AppendChild(xSequenceNumber);



                    var rawData = BitConverter.ToString(vm).Replace("-", "");
                    XmlElement xraw = doc.CreateElement("raw_data");
                    xraw.InnerText = rawData;
                    root.AppendChild(xraw);
                    string xmlOutput = doc.OuterXml;
                    // return;
                    var viewXml = xmlOutput;
                    
                }
                catch (Exception ex)
                {
                    
                }




            }

            return s_Imei;
        }

        public static string HexToBinString(string value)
        {
            String binaryString = Convert.ToString(Convert.ToInt32(value, 16), 2);
            Int32 zeroCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(binaryString.Length) / 8)) * 8;

            return binaryString.PadLeft(zeroCount, '0');
        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static bool ConvertToBoolean(string value)
        {
            var b = value.Equals("1") ? true : false;
            return b;
        }

        public static Byte[] HexStringToByteArray(string hexinput)
        {

            if (hexinput == String.Empty)
                return null;
            if (hexinput.Length % 2 == 1)
                hexinput = "0" + hexinput;
            int arrSize = hexinput.Length / 2;
            Byte[] myBytes = new Byte[arrSize];
            for (int i = 0; i < arrSize; i++)
                myBytes[i] = Convert.ToByte(hexinput.Substring(i * 2, 2), 16);
            return myBytes;
        }

        public static int GetTriggerEventStatus(byte[] hex)
        {
            var status = 0;
            int value = BitConverter.ToInt16(hex, 0);
            var statusCode = 0;
            status = value;
            switch (status)
            {

                //time based,status
                case 0:
                    statusCode = 124;
                    break;
                //distance Trigger
                case 1:
                    statusCode = 124;
                    break;
                //heading changed
                case 2:
                    statusCode = 124;
                    break;
                //landmark changed but rare
                case 3:
                    statusCode = 124;
                    break;

                //iO Event
                case 4:
                    statusCode = 125;
                    break;
                //ignition On
                case 10:
                    statusCode = 7;
                    break;
                //ignition Off
                case 11:
                    statusCode = 8;
                    break;
                //trip Start
                case 12:
                    statusCode = 124;
                    break;
                //trip End
                case 13:
                    statusCode = 124;
                    break;
                //panic button
                case 20:
                    statusCode = 153;
                    break;
                //over speeding
                case 21:
                    statusCode = 1;
                    break;
                //idling
                case 22:
                    statusCode = 4;
                    break;
                //tilt alarm
                case 23:
                    statusCode = 14;
                    break;

                //temp alarm
                case 24:
                    statusCode = 3;
                    break;

                //motion sensor
                case 26:
                    statusCode = 12;
                    break;
                //ignition disabled
                case 27:
                    statusCode = 125;
                    break;
                //ignition enabled
                case 28:
                    statusCode = 125;
                    break;
                //listen activated
                case 30:
                    statusCode = 125;
                    break;
                //temp shutdown
                case 32:
                    statusCode = 125;
                    break;
                // boot reset
                case 33:
                    statusCode = 125;
                    break;
                //x button Press
                case 40:
                    statusCode = 125;
                    break;
                //power button press
                case 41:
                    statusCode = 125;
                    break;
                //call button
                case 42:
                    statusCode = 125;
                    break;
                //low power
                case 50:
                    statusCode = 125;
                    break;
                //power cut(external)
                case 51:
                    statusCode = 125;
                    break;

                //external power connected
                case 52:
                    statusCode = 125;
                    break;
                //batery level ok
                case 53:
                    statusCode = 125;
                    break;
                //batery removed
                case 54:
                    statusCode = 125;
                    break;

            }

            return statusCode;
        }

        public static int GetTriggerEventsUnMapped(byte[] hex)
        {
            int value = BitConverter.ToInt16(hex, 0);
            var statusCode = value;
            return statusCode;
        }

        public static int GetStateFlags(byte[] hex)
        {
            var stateFlags = 0;
            int hexResults = BitConverter.ToInt16(hex, 0);
            stateFlags = hexResults;
            return stateFlags;
        }
        public static DateTime UnixTimestampToDateTimeNew(double unixTime)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            long unixTimeStampInTicks = (long)(unixTime * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks, System.DateTimeKind.Utc);
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }



        public static string GetBinary(string hexstring)
        {
            string binarystring = String.Join(String.Empty,
                hexstring.Select(
                    c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')
                )
            );
            return binarystring;
        }

        private static int GetRangeStates(IEnumerable<byte> bStateFlags)
        {
            var comparer = BitConverter.ToInt16(bStateFlags.ToArray(), 0);
            int value = 0;

            if (comparer == 1)
            {
                value = 101;
            }
            else if (comparer == 2)
            {
                value = 102;
            }
            else if (comparer == 4)
            {
                value = 103;
            }
            else if (comparer == 8)
            {
                value = 104;
            }
            else if (comparer == 16)
            {
                value = 105;
            }
            else if (comparer == 32)
            {
                //Ignition On/Off
                value = 112;
            }
            else if (comparer >= 64 && comparer <= 127)
            {
                //SHOCL Sensor on (Moving)
                value = 106;
            }
            else if (comparer >= 64 && comparer <= 127)
            {
                value = 107;
            }
            else if (comparer >= 128 && comparer <= 255)
            {
                //Parked
                value = 108;
            }
            else if (comparer >= 256 && comparer <= 511)
            {
                //Moving
                value = 109;
            }
            else if (comparer >= 512 && comparer <= 1023)
            {
                //Stopped
                value = 110;
            }
            else if (comparer >= 1024)
            {
                //GPS ON/ OFF
                value = 111;
            }

            return value;
        }
    }
}
