﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class Attributes
    {
        public int? fuel { get; set; }
        public int? output { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public double? batteryLevel { get; set; }
        public int? sat { get; set; }
        public int? rpm { get; set; }
        public string gps { get; set; }
        public string alarm { get; set; }
        public double hdop { get; set; }
        public string pdop { get; set; }
        public string vdop { get; set; }
        public double? power { get; set; }
        public bool? out1 { get; set; }
        public bool? out2 { set; get; }
        public int? io24 { set; get; }
        public int? io205 { set; get; }
        public int? io206 { set; get; }
        public int? io69 { set; get; }
        public int? Operator { set; get; }
        public int? io27 { get; set; }
        public int? io49 { get; set; }
        public int? io32 { get; set; }
        public int? io35 { get; set; }
        public int? io37 { get; set; }
        public int? io39 { get; set; }
        public long io199 { get; set; }
        public int? workMode { get; set; }
        public double adc1 { get; set; }
        public int? di1 { get; set; }
        public int? di2 { get; set; }
        public int? di3 { set; get; }
        public int? di4 { set; get; }
        public int? io28 { get; set; }
        public int? io173 { get; set; }
        public int? io169 { get; set; }
        public int? io50 { get; set; }
        public int? io51 { get; set; }
        public int? io136 { get; set; }
        public int? io135 { get; set; }
        public int? io143 { get; set; }
        public int? io134 { get; set; }
        public int? io176 { get; set; }
        public int? io88 { get; set; }
        public int? io29 { get; set; }
        public int? io30 { get; set; }
        public int? io31 { get; set; }
        public int? io33 { get; set; }
        public int? io34 { get; set; }
        public int? io38 { get; set; }
        public int? io40 { get; set; }
        public int? io36 { get; set; }
        public int? io41 { get; set; }
        public int? io42 { get; set; }
        public int? io44 { get; set; }
        public int? io46 { get; set; }
        public int? io47 { get; set; }
        public int? io48 { get; set; }
        public int? io52 { get; set; }
        public int? io53 { get; set; }
        public int? io54 { get; set; }
        public int? io55 { get; set; }
        public int? io56 { get; set; }
        public int? io57 { get; set; }
        public int? io58 { get; set; }
        public int? io59 { get; set; }
        public int? io60 { get; set; }
        public int? io43 { get; set; }
        public int? io15 { get; set; }
        public int? io45 { get; set; }
        public int? io16 { get; set; }
        public int? io22 { get; set; }
        public int? io23 { get; set; }
        public int? io211 { get; set; }
        public int? io67 { get; set; }
        public long? io100 { get; set; }
        public int? io131 { get; set; }
        public int? io137 { get; set; }
        public int? io139 { get; set; }
        public int? io65 { get; set; }
        public int? io150 { get; set; }
        public int? io141 { get; set; }
        public int? io175 { get; set; }
        public int? io151 { get; set; }
        public int? io242 { get; set; }
        public int? io243 { get; set; }
        public int? io244 { get; set; }
        public int? io245 { get; set; }
        public int? io246 { get; set; }
        public int? io247 { get; set; }
        public int? io248 { get; set; }
        public int? io249 { get; set; }
        public int? io250 { get; set; }
        public int? io251 { get; set; }
        public int? io252 { get; set; }
        public int? io253 { get; set; }
        public int? io254 { get; set; }
        public int? io255 { get; set; }
        public decimal odometer { get; set; }
        public long? input { get; set; }
        public double? adc2 { get; set; }
        public string raw { get; set; }
        public bool ignition { get; set; }
        public bool charge { get; set; }
        public bool blocked { get; set; }
        public double battery { get; set; }
        public int? rssi { get; set; }
        public double? distance { get; set; }
        public decimal totalDistance { get; set; }
        public bool motion { get; set; }
        public int? priority { get; set; }
        public string Event { get; set; }
        public double? temp1 { get; set; }
        public string driverUniqueId { get; set; }
        public int? io171 { get; set; }
        public int? io172 { get; set; }
        public int? obdSpeed { get; set; }
        public int? coolant { get; set; }
        public int? coolantTemp { get; set; }
        public double? fuelConsumption { get; set; }
        public int? chargerPressure { get; set; }
        public int? tpl { get; set; }
        public int? axle { get; set; }
        public string obdOdometer { get; set; }
        public string hours { get; set; }
        public string throttle { get; set; }
        public string engineLoad { get; set; }
        public string drivingHours { get; set; }
        public string idleHours { get; set; }
        public string indicators { get; set; }
        public string doors { get; set; }
        public long? io1 { get; set; }
        public int? io2 { get; set; }
        public int? io3 { get; set; }
        public int? io4 { get; set; }
        public int? io5 { get; set; }
        public int? io6 { get; set; }
        public int? io7 { get; set; }
        public int? io8 { get; set; }
        public int? io9 { get; set; }
        public int? io10 { get; set; }
        public int? io11 { get; set; }
        public int? io12 { get; set; }
        public int? io13 { get; set; }
        public int? io14 { get; set; }
        public int? io17 { get; set; }
        public int? io18 { get; set; }
        public int? io19 { get; set; }
        public int? io20 { get; set; }
        public int? io21 { get; set; }
        public int? io25 { get; set; }
        public int? io26 { get; set; }
        public int? io61 { get; set; }
        public int? io62 { get; set; }
        public int? io63 { get; set; }
        public int? io64 { get; set; }
        public int? io66 { get; set; }
        public int? io68 { get; set; }
        public int? io70 { get; set; }
        public int? io71 { get; set; }
        public int? io72 { get; set; }
        public int? io73 { get; set; }
        public int? io74 { get; set; }
        public int? io75 { get; set; }
        public int? io76 { get; set; }
        public int? io77 { get; set; }
        public int? io78 { get; set; }
        public int? io79 { get; set; }
        public int? io80 { get; set; }
        public int? io81 { get; set; }
        public int? io82 { get; set; }
        public int? io83 { get; set; }
        public int? io84 { get; set; }
        public int? io85 { get; set; }
        public int? io86 { get; set; }
        public int? io87 { get; set; }
        public int? io89 { get; set; }
        public int? io90 { get; set; }
        public int? io91 { get; set; }
        public int? io92 { get; set; }
        public int? io93 { get; set; }
        public int? io94 { get; set; }
        public int? io95 { get; set; }
        public int? io96 { get; set; }
        public int? io97 { get; set; }
        public int? io98 { get; set; }
        public int? io99 { get; set; }
        public long? io101 { get; set; }
        public int? io102 { get; set; }
        public int? io103 { get; set; }
        public int? io104 { get; set; }
        public int? io105 { get; set; }
        public int? io106 { get; set; }
        public int? io107 { get; set; }
        public int? io108 { get; set; }
        public int? io109 { get; set; }
        public int? io110 { get; set; }
        public int? io111 { get; set; }
        public int? io112 { get; set; }
        public int? io113 { get; set; }
        public int? io114 { get; set; }
        public int? io115 { get; set; }
        public int? io116 { get; set; }
        public int? io117 { get; set; }
        public int? io118 { get; set; }
        public int? io119 { get; set; }
        public int? io120 { get; set; }
        public int? io121 { get; set; }
        public int? io122 { get; set; }
        public int? io123 { get; set; }
        public int? io124 { get; set; }
        public int? io125 { get; set; }
        public int? io126 { get; set; }
        public int? io127 { get; set; }
        public int? io128 { get; set; }
        public int? io129 { get; set; }
        public int? io130 { get; set; }
        public int? io132 { get; set; }
        public int? io133 { get; set; }
        public int? io138 { get; set; }
        public int? io140 { get; set; }
        public int? io142 { get; set; }
        public int? io144 { get; set; }
        public int? io145 { get; set; }
        public int? io146 { get; set; }
        public int? io147 { get; set; }
        public int? io148 { get; set; }
        public int? io149 { get; set; }
        public int? io152 { get; set; }
        public int? io153 { get; set; }
        public int? io154 { get; set; }
        public int? io155 { get; set; }
        public int? io156 { get; set; }
        public int? io157 { get; set; }
        public int? io158 { get; set; }
        public int? io159 { get; set; }
        public int? io160 { get; set; }
        public int? io161 { get; set; }
        public int? io162 { get; set; }
        public int? io163 { get; set; }
        public int? io164 { get; set; }
        public int? io165 { get; set; }
        public int? io166 { get; set; }
        public int? io167 { get; set; }
        public int? io168 { get; set; }
        public int? io170 { get; set; }
        public int? io174 { get; set; }
        public int? io177 { get; set; }
        public int? io178 { get; set; }
        public int? io179 { get; set; }
        public int? io180 { get; set; }
        public int? io181 { get; set; }
        public int? io182 { get; set; }
        public int? io183 { get; set; }
        public int? io184 { get; set; }
        public int? io185 { get; set; }
        public int? io186 { get; set; }
        public int? io187 { get; set; }
        public int? io188 { get; set; }
        public int? io189 { get; set; }
        public int? io190 { get; set; }
        public int? io191 { get; set; }
        public int? io192 { get; set; }
        public int? io193 { get; set; }
        public int? io194 { get; set; }
        public int? io195 { get; set; }
        public int? io196 { get; set; }
        public int? io197 { get; set; }
        public int? io198 { get; set; }
        public int? io200 { get; set; }
        public int? io201 { get; set; }
        public int? io202 { get; set; }
        public int? io203 { get; set; }
        public int? io204 { get; set; }
        public int? io207 { get; set; }
        public int? io208 { get; set; }
        public int? io209 { get; set; }
        public int? io210 { get; set; }
        public int? io212 { get; set; }
        public int? io213 { get; set; }
        public int? io214 { get; set; }
        public int? io215 { get; set; }
        public int? io216 { get; set; }
        public int? io217 { get; set; }
        public int? io218 { get; set; }
        public int? io219 { get; set; }
        public int? io220 { get; set; }
        public int? io221 { get; set; }
        public int? io222 { get; set; }
        public int? io223 { get; set; }
        public int? io224 { get; set; }
        public int? io225 { get; set; }
        public int? io226 { get; set; }
        public int? io227 { get; set; }
        public int? io228 { get; set; }
        public int? io229 { get; set; }
        public int? io230 { get; set; }
        public int? io231 { get; set; }
        public int? io232 { get; set; }
        public int? io233 { get; set; }
        public int? io234 { get; set; }
        public int? io235 { get; set; }
        public int? io236 { get; set; }
        public int? io237 { get; set; }
        public int? io238 { get; set; }
        public int? io239 { get; set; }
        public int? io240 { get; set; }
        public int? io241 { get; set; }
        public int? io256 { get; set; }
        public int? io257 { get; set; }
        public int? io258 { get; set; }
        public int? io259 { get; set; }
        public int? io260 { get; set; }
        public int? io261 { get; set; }
        public int? io262 { get; set; }
        public int? io263 { get; set; }
        public int? io264 { get; set; }
        public int? io265 { get; set; }
        public int? io266 { get; set; }
        public int? io267 { get; set; }
        public int? io268 { get; set; }
        public int? io269 { get; set; }
        public int? io270 { get; set; }
        public int? io271 { get; set; }
        public int? io272 { get; set; }
        public int? io273 { get; set; }
        public int? io274 { get; set; }
        public int? io275 { get; set; }
        public int? io276 { get; set; }
        public int? io277 { get; set; }
        public int? io278 { get; set; }
        public int? io279 { get; set; }
        public int? io280 { get; set; }
        public int? io281 { get; set; }
        public int? io282 { get; set; }
        public int? io283 { get; set; }
        public int? io284 { get; set; }
        public int? io285 { get; set; }
        public int? io286 { get; set; }
        public int? io287 { get; set; }
        public int? io288 { get; set; }
        public int? io289 { get; set; }
        public int? io290 { get; set; }
        public int? io291 { get; set; }
        public int? io292 { get; set; }
        public int? io293 { get; set; }
        public int? io294 { get; set; }
        public int? io295 { get; set; }
        public int? io296 { get; set; }
        public int? io297 { get; set; }
        public int? io298 { get; set; }
        public int? io299 { get; set; }
        public int? io300 { get; set; }
        public int? io301 { get; set; }
        public int? io302 { get; set; }
        public int? io303 { get; set; }
        public int? io304 { get; set; }
        public int? io305 { get; set; }
        public int? io306 { get; set; }
        public int? io307 { get; set; }
        public int? io308 { get; set; }
        public int? io309 { get; set; }
        public int? io310 { get; set; }
        public int? io311 { get; set; }
        public int? io312 { get; set; }
        public int? io313 { get; set; }
        public int? io314 { get; set; }
        public int? io315 { get; set; }
        public int? io316 { get; set; }
        public int? io317 { get; set; }
        public int? io318 { get; set; }
        public int? io319 { get; set; }
        public int? io320 { get; set; }
        public int? io321 { get; set; }
        public int? io322 { get; set; }
        public int? io323 { get; set; }
        public int? io324 { get; set; }
        public int? io325 { get; set; }
        public int? io326 { get; set; }
        public int? io327 { get; set; }
        public int? io328 { get; set; }
        public int? io329 { get; set; }
        public int? io330 { get; set; }
        public int? io331 { get; set; }
        public int? io332 { get; set; }
        public int? io333 { get; set; }
        public int? io334 { get; set; }
        public int? io335 { get; set; }
        public int? io336 { get; set; }
        public int? io337 { get; set; }
        public int? io338 { get; set; }
        public int? io339 { get; set; }
        public int? io340 { get; set; }
        public int? io341 { get; set; }
        public int? io342 { get; set; }
        public int? io343 { get; set; }
        public int? io344 { get; set; }
        public int? io345 { get; set; }
        public int? io346 { get; set; }
        public int? io347 { get; set; }
        public int? io348 { get; set; }
        public int? io349 { get; set; }
        public int? io350 { get; set; }
        public int? io351 { get; set; }
        public int? io352 { get; set; }
        public int? io353 { get; set; }
        public int? io354 { get; set; }
        public int? io355 { get; set; }
        public int? io356 { get; set; }
        public int? io357 { get; set; }
        public int? io358 { get; set; }
        public int? io359 { get; set; }
        public int? io360 { get; set; }
        public int? io361 { get; set; }
        public int? io362 { get; set; }
        public int? io363 { get; set; }
        public int? io364 { get; set; }
        public int? io365 { get; set; }
        public int? io366 { get; set; }
        public int? io367 { get; set; }
        public int? io368 { get; set; }
        public int? io369 { get; set; }
        public int? io370 { get; set; }
        public int? io371 { get; set; }
        public int? io372 { get; set; }
        public int? io373 { get; set; }
        public int? io374 { get; set; }
        public int? io375 { get; set; }
        public int? io376 { get; set; }
        public int? io377 { get; set; }
        public int? io378 { get; set; }
        public int? io379 { get; set; }
        public int? io380 { get; set; }
        public int? io381 { get; set; }
        public int? io382 { get; set; }
        public int? io383 { get; set; }
        public int? io384 { get; set; }
        public int? io385 { get; set; }
        public int? io386 { get; set; }
        public int? io387 { get; set; }
        public int? io388 { get; set; }
        public int? io389 { get; set; }
        public int? io390 { get; set; }
        public int? io391 { get; set; }
        public int? io392 { get; set; }
        public int? io393 { get; set; }
        public int? io394 { get; set; }
        public int? io395 { get; set; }
        public int? io396 { get; set; }
        public int? io397 { get; set; }
        public int? io398 { get; set; }
        public int? io399 { get; set; }
        public int? io400 { get; set; }
        public int? io401 { get; set; }
        public int? io402 { get; set; }
        public int? io403 { get; set; }
        public int? io404 { get; set; }
        public int? io405 { get; set; }
        public int? io406 { get; set; }
        public int? io407 { get; set; }
        public int? io408 { get; set; }
        public int? io409 { get; set; }
        public int? io410 { get; set; }
        public int? io411 { get; set; }
        public int? io412 { get; set; }
        public int? io413 { get; set; }
        public int? io414 { get; set; }
        public int? io415 { get; set; }
        public int? io416 { get; set; }
        public int? io417 { get; set; }
        public int? io418 { get; set; }
        public int? io419 { get; set; }
        public int? io420 { get; set; }
        public int? io421 { get; set; }
        public int? io422 { get; set; }
        public int? io423 { get; set; }
        public int? io424 { get; set; }
        public int? io425 { get; set; }
        public int? io426 { get; set; }
        public int? io427 { get; set; }
        public int? io428 { get; set; }
        public int? io429 { get; set; }
        public int? io430 { get; set; }
        public int? io431 { get; set; }
        public int? io432 { get; set; }
        public int? io433 { get; set; }
        public int? io434 { get; set; }
        public int? io435 { get; set; }
        public int? io436 { get; set; }
        public int? io437 { get; set; }
        public int? io438 { get; set; }
        public int? io439 { get; set; }
        public int? io440 { get; set; }
        public int? io441 { get; set; }
        public int? io442 { get; set; }
        public int? io443 { get; set; }
        public int? io444 { get; set; }
        public int? io445 { get; set; }
        public int? io446 { get; set; }
        public int? io447 { get; set; }
        public int? io448 { get; set; }
        public int? io449 { get; set; }
        public int? io450 { get; set; }
        public int? io451 { get; set; }
        public int? io452 { get; set; }
        public int? io453 { get; set; }
        public int? io454 { get; set; }
        public int? io455 { get; set; }
        public int? io456 { get; set; }
        public int? io457 { get; set; }
        public int? io458 { get; set; }
        public int? io459 { get; set; }
        public int? io460 { get; set; }
        public int? io461 { get; set; }
        public int? io462 { get; set; }
        public int? io463 { get; set; }
        public int? io464 { get; set; }
        public int? io465 { get; set; }
        public int? io466 { get; set; }
        public int? io467 { get; set; }
        public int? io468 { get; set; }
        public int? io469 { get; set; }
        public int? io470 { get; set; }
        public int? io471 { get; set; }
        public int? io472 { get; set; }
        public int? io473 { get; set; }
        public int? io474 { get; set; }
        public int? io475 { get; set; }
        public int? io476 { get; set; }
        public int? io477 { get; set; }
        public int? io478 { get; set; }
        public int? io479 { get; set; }
        public int? io480 { get; set; }
        public int? io481 { get; set; }
        public int? io482 { get; set; }
        public int? io483 { get; set; }
        public int? io484 { get; set; }
        public int? io485 { get; set; }
        public int? io486 { get; set; }
        public int? io487 { get; set; }
        public int? io488 { get; set; }
        public int? io489 { get; set; }
        public int? io490 { get; set; }
        public int? io491 { get; set; }
        public int? io492 { get; set; }
        public int? io493 { get; set; }
        public int? io494 { get; set; }
        public int? io495 { get; set; }
        public int? io496 { get; set; }
        public int? io497 { get; set; }
        public int? io498 { get; set; }
        public int? io499 { get; set; }

        public int? io500 { get; set; }
        public int? io501 { get; set; }
        public int? io502 { get; set; }
        public int? io503 { get; set; }
        public int? io504 { get; set; }
        public int? io505 { get; set; }
        public int? io506 { get; set; }
        public int? io507 { get; set; }
        public int? io508 { get; set; }
        public int? io509 { get; set; }
        public int? io510 { get; set; }
        public int? io511 { get; set; }
        public int? io512 { get; set; }
        public int? io513 { get; set; }
        public int? io514 { get; set; }
        public int? io515 { get; set; }
        public int? io516 { get; set; }
        public int? io517 { get; set; }
        public int? io518 { get; set; }
        public int? io519 { get; set; }
        public int? io520 { get; set; }
        public int? io521 { get; set; }
        public int? io522 { get; set; }
        public int? io523 { get; set; }
        public int? io524 { get; set; }
        public int? io525 { get; set; }
        public int? io526 { get; set; }
        public int? io527 { get; set; }
        public int? io528 { get; set; }
        public int? io529 { get; set; }
        public int? io530 { get; set; }
        public int? io531 { get; set; }
        public int? io532 { get; set; }
        public int? io533 { get; set; }
        public int? io534 { get; set; }
        public int? io535 { get; set; }
        public int? io536 { get; set; }
        public int? io537 { get; set; }
        public int? io538 { get; set; }
        public int? io539 { get; set; }
        public int? io540 { get; set; }
        public int? io541 { get; set; }
        public int? io542 { get; set; }
        public int? io543 { get; set; }
        public int? io544 { get; set; }
        public int? io545 { get; set; }
        public int? io546 { get; set; }
        public int? io547 { get; set; }
        public int? io548 { get; set; }
        public int? io549 { get; set; }
        public int? io550 { get; set; }
        public int? io551 { get; set; }
        public int? io552 { get; set; }
        public int? io553 { get; set; }
        public int? io554 { get; set; }
        public int? io555 { get; set; }
        public int? io556 { get; set; }
        public int? io557 { get; set; }
        public int? io558 { get; set; }
        public int? io559 { get; set; }
        public int? io560 { get; set; }
        public int? io561 { get; set; }
        public int? io562 { get; set; }
        public int? io563 { get; set; }
        public int? io564 { get; set; }
        public int? io565 { get; set; }
        public int? io566 { get; set; }
        public int? io567 { get; set; }
        public int? io568 { get; set; }
        public int? io569 { get; set; }
        public int? io570 { get; set; }
        public int? io571 { get; set; }
        public int? io572 { get; set; }
        public int? io573 { get; set; }
        public int? io574 { get; set; }
        public int? io575 { get; set; }
        public int? io576 { get; set; }
        public int? io577 { get; set; }
        public int? io578 { get; set; }
        public int? io579 { get; set; }
        public int? io580 { get; set; }
        public int? io581 { get; set; }
        public int? io582 { get; set; }
        public int? io583 { get; set; }
        public int? io584 { get; set; }
        public int? io585 { get; set; }
        public int? io586 { get; set; }
        public int? io587 { get; set; }
        public int? io588 { get; set; }
        public int? io589 { get; set; }
        public int? io590 { get; set; }
        public int? io591 { get; set; }
        public int? io592 { get; set; }
        public int? io593 { get; set; }
        public int? io594 { get; set; }
        public int? io595 { get; set; }
        public int? io596 { get; set; }
        public int? io597 { get; set; }
        public int? io598 { get; set; }
        public int? io599 { get; set; }
        public int? io600 { get; set; }
        public int? io601 { get; set; }
        public int? io602 { get; set; }
        public int? io603 { get; set; }
        public int? io604 { get; set; }
        public int? io605 { get; set; }
        public int? io606 { get; set; }
        public int? io607 { get; set; }
        public int? io608 { get; set; }
        public int? io609 { get; set; }
        public int? io610 { get; set; }
        public int? io611 { get; set; }
        public int? io612 { get; set; }
        public int? io613 { get; set; }
        public int? io614 { get; set; }
        public int? io615 { get; set; }
        public int? io616 { get; set; }
        public int? io617 { get; set; }
        public int? io618 { get; set; }
        public int? io619 { get; set; }
        public int? io620 { get; set; }
        public int? io621 { get; set; }
        public int? io622 { get; set; }
        public int? io623 { get; set; }
        public int? io624 { get; set; }
        public int? io625 { get; set; }
        public int? io626 { get; set; }
        public int? io627 { get; set; }
        public int? io628 { get; set; }
        public int? io629 { get; set; }
        public int? io630 { get; set; }

    }
}
