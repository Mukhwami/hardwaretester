﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace HardwareTester
{
    public class cProgram
    {      
        public static ExportedObject GetExportedObject(string data)
        {
            ExportedObject exportedObject = null;
            Location loc = null;
            try
            {
                var rawdata = data;
               

                    try
                    {
                        var pdata = data;//.Remove(0, 2);
                        var decodedUrl = HttpUtility.UrlDecode(data);
                        var xdata = "{\"position\":{\"" + decodedUrl.Replace("&", "\",\"").Replace("=", "\":\"").Replace(",\"attributes\":\"", "},\"attributes\":") + "}";
                        var jsondata = JsonConvert.DeserializeObject<clsCommonObectDecoded>(xdata);
                        jsondata.Position.Id = Regex.Replace(jsondata.Position.Id, "[A-Za-z ]", "");
                        jsondata.Position.RawData = decodedUrl;
                        loc = new Location()
                        {
                            Id = Convert.ToUInt64(jsondata.Position.Id),
                            DeviceId = Convert.ToInt32(jsondata.Position.DeviceId),
                            Valid = Convert.ToBoolean(jsondata.Position.Valid),
                            FixTime = Helper.FromUnixTime(jsondata.Position.FixTime),
                            DeviceTime = Helper.FromUnixTime(jsondata.Position.DeviceTime),
                            Protocol = jsondata.Position.Protocol,
                            Latitude = Decimal.Parse(jsondata.Position.Latitude, System.Globalization.NumberStyles.Float),
                            Longitude = Decimal.Parse(jsondata.Position.Longitude, System.Globalization.NumberStyles.Float),
                            Altitude = Convert.ToDouble(jsondata.Position.Altitude),
                            Speed = Convert.ToDouble(jsondata.Position.Speed),
                            Course = Convert.ToDouble(jsondata.Position.Course),
                            StatusCode = jsondata.Position.StatusCode,
                            Ignition = jsondata.Attributes.ignition,                            

                        };
                        exportedObject = new ExportedObject(loc, jsondata.Attributes, data);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                
                

            }
            catch (Exception en)
            {

                exportedObject = null;// GetExportedObjectException(data);
            }
            return exportedObject;
        }
    }
}
