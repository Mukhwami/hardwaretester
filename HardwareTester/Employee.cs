﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class Employee
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public decimal Amount { get; set; }
    }
    public class Test
    {
        public int Id { get; set; }
        public string firstName { get; set; }
        public string LastName { get; set; }
    }
    public class GenericRepository<T>  where T : class
    {
        
        public T SaveChanges(Dictionary<string, object> paramDic, T tEntity)
        {
            int total = paramDic.Count;
            var cols = new StringBuilder();
            var val = new StringBuilder();
            for (int i = 0; i < total; i++)
            {
                if (i == total - 1)
                {
                    val.Append($"'{paramDic.ElementAt(i).Value}'");
                    cols.Append($"{paramDic.ElementAt(i).Key}");
                }
                else
                {
                    val.Append($"'{paramDic.ElementAt(i).Value}',");
                    cols.Append($"{paramDic.ElementAt(i).Key},");
                }
            }
            //Getting Type of Generic Class Model
            Type tModelType = tEntity.GetType();

            //We will be defining a PropertyInfo Object which contains details about the class property 
            PropertyInfo[] arrayPropertyInfos = tModelType.GetProperties();

            //Now we will loop in all properties one by one to get value
            foreach (PropertyInfo property in arrayPropertyInfos)
            {
                Console.WriteLine("Name of Property is:\t" + property.Name);
                Console.WriteLine("Name of Type is\t:\t" + property.PropertyType.Name);
                Console.WriteLine("Value of Property is:\t" + property.GetValue(tEntity).ToString());
                Console.WriteLine(Environment.NewLine);
            }
            string sQL = $"INSERT INTO {typeof(T).Name}({cols.ToString()}) VALUES({val});";
            return tEntity;
        }
    }
}
