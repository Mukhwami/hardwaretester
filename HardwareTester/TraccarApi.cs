﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Configuration;
using RestSharp;

namespace HardwareTester
{
    public class TraccarApi
    {
        public static string TraccarUsername = "Admin";
        public static string TraccarPassword = "WltC0ntr0l";
        public static string TraccarBaseUrl = "http://data.gpsiot.net:8082";//"http://192.168.1.167:8082";//
        public static bool UpdateTraccaOdometer(long IMEI, long Mileage)
        {
            var Updated = false;
            var _Device = Program._cacheTraccarDevices().FirstOrDefault(x => x.UniqueId == IMEI.ToString());
            try
            {
                if (_Device == null)
                    return Updated;
                var totalDistance = Mileage * 1000;
                var dev = new DeviceName() { deviceId = _Device.Id, totalDistance = totalDistance, hours = 1000 };
                var UrlStr = $"{TraccarBaseUrl}/api/devices/{dev.deviceId}/accumulators/";
                var byteArray = Encoding.ASCII.GetBytes(TraccarUsername + ":" + TraccarPassword);
                var client = new RestClient(UrlStr);//http://dev.whitelabeltracking.com:8082/api/devices/2/accumulators/");
                var serilized = JsonConvert.SerializeObject(dev, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                var request = new RestRequest(Method.PUT);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", $"Basic {Convert.ToBase64String(byteArray)}");
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("ContentType", "application/json");
                request.AddParameter(_Device.Name, serilized, ParameterType.RequestBody);
                var response = client.Execute(request);
                switch (response.StatusCode)
                {
                    case HttpStatusCode.NoContent:
                        Console.ResetColor();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Device distance updated successfully");
                        Updated = true;
                        break;
                    default:
                        Console.ResetColor();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Device distance Failed");
                        Updated = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                Updated = false;
            }
            return Updated;
        }
        public static bool UpdateOdometerOnTraccar(long IMEI, long Mileage)
        {
            var Updated = false;
            try
            {
                var _Device = Program._cacheTraccarDevices().FirstOrDefault(x => x.UniqueId == IMEI.ToString());
                if (_Device == null)
                    return Updated;
                var totalDistance = Mileage * 1000;
                var dev = new DeviceName() { deviceId = _Device.Id, totalDistance = totalDistance, hours = 1000 };
                //string UrlStr = $"/api/devices/{dev.deviceId}/accumulators";//, );4.2
                var UrlStr = $"/api/devices/{dev.deviceId}/distance";//, );old 3.17
                var byteArray = Encoding.ASCII.GetBytes(TraccarUsername + ":" + TraccarPassword);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(TraccarBaseUrl);
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var serilized = JsonConvert.SerializeObject(dev, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    var inputMessage = new HttpRequestMessage
                    {
                        Content = new StringContent(serilized, Encoding.UTF8, "application/json"),
                        Method = HttpMethod.Put,
                        //RequestUri=UrlStr
                    };
                    inputMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var message = client.PutAsync(UrlStr, inputMessage.Content).Result;
                    switch (message.StatusCode)
                    {
                        case HttpStatusCode.NoContent:
                            Console.ResetColor();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Device distance updated successfully");
                            Updated = true;
                            break;
                        case HttpStatusCode.Accepted:
                            Console.ResetColor();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Device distance updated successfully");
                            Updated = true;
                            break;
                        default:
                            Console.ResetColor();
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Device distance Failed");
                            Updated = false;
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                Updated = false;
            }
            return Updated;
        }
        public static bool SendGprsCommand(long IMEI, string custom, string type)
        {
            var Updated = false;
            try
            {
                var _Device = Program._cacheTraccarDevices().FirstOrDefault(x => x.UniqueId == IMEI.ToString());
                if (_Device == null)
                    return Updated;

                var cmd = new Command { data = custom };
                var dev = new GprsCommand() { attributes = cmd, id = 0, deviceId = _Device.Id, description = "New", textChannel = false, type = type };
                var UrlStr = "api/commands/send";
                var byteArray = Encoding.ASCII.GetBytes(TraccarUsername + ":" + TraccarPassword);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri($"{TraccarBaseUrl}/");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var serilized = JsonConvert.SerializeObject(dev, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    var inputMessage = new HttpRequestMessage
                    {
                        Content = new StringContent(serilized, Encoding.UTF8, "application/json"),
                        Method = HttpMethod.Put,
                        //RequestUri=new Uri($"{client.BaseAddress.AbsoluteUri}")
                    };
                    inputMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var message = client.PostAsync(UrlStr, inputMessage.Content).Result;
                    switch (message.StatusCode)
                    {
                        case HttpStatusCode.OK:
                        case HttpStatusCode.Accepted:
                            Console.ResetColor();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Device distance updated successfully");
                            Updated = true;
                            break;
                        default:
                            Console.ResetColor();
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Device distance Failed");
                            Updated = false;
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                Updated = false;
            }
            return Updated;
        }
    }
}
