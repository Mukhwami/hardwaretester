﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class General
    {

    }
    public interface IMachine
    {
        void Start();
        void Stop();
    }
    public class Car : IMachine
    {
        public void Start()
        {
            Console.WriteLine("Car Engine Started");
        }

        public void Stop()
        {
            Console.WriteLine("Car Engine Stopped");
        }
    }
    public class Aeroplane : IMachine
    {
        public void Start()
        {
            Console.WriteLine("Aeroplane Engine Started");
        }

        public void Stop()
        {
            Console.WriteLine("Aeroplane Engine Stopped");
        }
    }
    public class Truck : IMachine
    {
        public void Start()
        {
            Console.WriteLine("Truck Engine Started");
        }

        public void Stop()
        {
            Console.WriteLine("Truck Engine Stopped");
        }
    }

}
