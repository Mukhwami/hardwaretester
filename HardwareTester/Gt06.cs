﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HardwareTester
{
    class Gt06
    {

        private string protocol_id;
        private string date_time;
        private int num_sat;
        private double lat;
        private double lon;
        private int speed;
        private string course_stat;
        private int is_real_time_gps;
        private int gps_positioned;
        private int east_west_lon;
        private int south_north_lat;
        private int heading;
        private string MCC;
        private string MNC;
        private string LAC;
        private string cellID;
        private string serial_num;
        private string tail_char;

        private string terminal_info;

        private string immob_stat;
        private string gps_stat;
        private string charge_stat;
        private string ign_stat;
        private string alarm_stat;

        private string voltage_level;
        private string gsm_sig;

        private string odometer;

        public void status_message0(string sMsg, string simei)
        {
            try
            {
                //process location/alarm data
                string[] arr_data = sMsg.Split('-');

                if (arr_data[3] == "13")
                {

                    protocol_id = arr_data[3];
                    terminal_info = Convert.ToString(Convert.ToInt32(arr_data[4], 16), 2).PadLeft(8, '0');

                    immob_stat = terminal_info.Substring(0, 1);
                    gps_stat = terminal_info.Substring(1, 1);
                    alarm_stat = terminal_info.Substring(2, 3);
                    charge_stat = terminal_info.Substring(5, 1);
                    ign_stat = terminal_info.Substring(6, 1);

                    voltage_level = Convert.ToInt32(arr_data[5], 16).ToString();
                    gsm_sig = arr_data[6];
                    serial_num = arr_data[9] + arr_data[10];
                    tail_char = arr_data[13] + arr_data[14];



                    //xml stuff
                    XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
                    XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);

                    doc.AppendChild(dec);// Create the root element
                    XmlElement root = doc.CreateElement("GT06_Message");
                    doc.AppendChild(root);

                    //add the message
                    XmlElement xProtocolID = doc.CreateElement("protocol_id");
                    xProtocolID.InnerText = protocol_id;
                    root.AppendChild(xProtocolID);

                    XmlElement xDeviceID = doc.CreateElement("device_id");
                    xDeviceID.InnerText = simei;
                    root.AppendChild(xDeviceID);

                    XmlElement xDateTime = doc.CreateElement("datetime");
                    xDateTime.InnerText = DateTime.Now.ToString();
                    root.AppendChild(xDateTime);

                    XmlElement xImmobStatus = doc.CreateElement("immobilizer_status");
                    xImmobStatus.InnerText = immob_stat;
                    root.AppendChild(xImmobStatus);

                    XmlElement xGpsStat = doc.CreateElement("gpstracking_status");
                    xGpsStat.InnerText = gps_stat;
                    root.AppendChild(xGpsStat);

                    XmlElement xAlarmStat = doc.CreateElement("alarm_status");
                    xAlarmStat.InnerText = alarm_stat;
                    root.AppendChild(xAlarmStat);

                    XmlElement xChargeStat = doc.CreateElement("charge_status");
                    xChargeStat.InnerText = charge_stat;
                    root.AppendChild(xChargeStat);

                    XmlElement xIgnStat = doc.CreateElement("ign_status");
                    xIgnStat.InnerText = ign_stat;
                    root.AppendChild(xIgnStat);

                    XmlElement xVoltage = doc.CreateElement("voltage_level");
                    xVoltage.InnerText = voltage_level;
                    root.AppendChild(xVoltage);

                    XmlElement xGsm = doc.CreateElement("gsm_signal");
                    xGsm.InnerText = gsm_sig;
                    root.AppendChild(xGsm);


                    XmlElement xSerialNum = doc.CreateElement("serial_num");
                    xSerialNum.InnerText = serial_num;
                    root.AppendChild(xSerialNum);


                    XmlElement xTailChar = doc.CreateElement("tailchar");
                    xTailChar.InnerText = tail_char;
                    root.AppendChild(xTailChar);

                    //text position
                    string txtpos = string.Empty;



                    string xmlOutput = doc.OuterXml;


                }
            }
            catch (Exception ex)
            {

            }

        }


        public void processMsg(string oMsg, string simei)
        {

            try
            {
                ////process location/alarm data
                string[] s_init = oMsg.Split(new string[] { "0D-0A" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string sMsg in s_init)
                {
                    string curr_Data = sMsg + "0D-0A";
                    string[] arr_data = curr_Data.Split('-');

                    if (arr_data.Length == 42)
                        AlarmMessage(curr_Data, simei);

                    if (arr_data.Length == 40) //odometer data included
                        OdoMessage(curr_Data, simei);

                    if (arr_data.Length == 15 || arr_data.Length == 16)
                        status_message(curr_Data, simei);
                    if (arr_data.Length == 36) //location data packet
                    {
                        if (arr_data[3] == "12")
                        {

                            int year, month, day, hour, minute, second;
                            protocol_id = arr_data[3];
                            year = Convert.ToInt32(arr_data[4], 16);
                            month = Convert.ToInt32(arr_data[5], 16);
                            day = Convert.ToInt32(arr_data[6], 16);
                            hour = Convert.ToInt32(arr_data[7], 16);
                            minute = Convert.ToInt32(arr_data[8], 16);
                            second = Convert.ToInt32(arr_data[9], 16);

                            date_time = day.ToString().PadLeft(2, '0') + "/" + month.ToString().PadLeft(2, '0') + "/20" + year.ToString() + " " + hour.ToString().PadLeft(2, '0')
                                + ":" + minute.ToString().PadLeft(2, '0') + ":" + second.ToString().PadLeft(2, '0');

                            num_sat = Convert.ToInt32(arr_data[10].Substring(1, 1), 16);
                            lat = (Convert.ToInt32(arr_data[11] + arr_data[12] + arr_data[13] + arr_data[14], 16) / 30000.0) / 60.0;
                            lon = (Convert.ToInt32(arr_data[15] + arr_data[16] + arr_data[17] + arr_data[18], 16) / 30000.0) / 60.0;
                            speed = Convert.ToInt32(arr_data[19], 16);
                            course_stat = $"{Convert.ToString(Convert.ToInt32(arr_data[20], 16), 2).PadLeft(8, '0')}{Convert.ToString(Convert.ToInt32(arr_data[21], 16), 2).PadLeft(8, '0')}";
                            is_real_time_gps = int.Parse(course_stat.Substring(2, 1));
                            gps_positioned = int.Parse(course_stat.Substring(3, 1));
                            east_west_lon = int.Parse(course_stat.Substring(4, 1));
                            south_north_lat = int.Parse(course_stat.Substring(5, 1));
                            heading = Convert.ToInt32(course_stat.Substring(6, 10), 2);
                            MCC = Convert.ToInt32(arr_data[22] + arr_data[23], 16).ToString();
                            MNC = arr_data[24];
                            LAC = arr_data[25] + arr_data[26];
                            cellID = arr_data[27] + arr_data[28] + arr_data[29];
                            serial_num = arr_data[30] + arr_data[31];
                            tail_char = arr_data[34] + arr_data[35];



                            //xml stuff
                            XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
                            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);

                            doc.AppendChild(dec);// Create the root element
                            XmlElement root = doc.CreateElement("GT06_Message");
                            doc.AppendChild(root);

                            //add the message
                            XmlElement xProtocolID = doc.CreateElement("protocol_id");
                            xProtocolID.InnerText = protocol_id;
                            root.AppendChild(xProtocolID);

                            XmlElement xRawMessage = doc.CreateElement("raw_message");
                            xRawMessage.InnerText = curr_Data;
                            root.AppendChild(xRawMessage);


                            XmlElement xDeviceID = doc.CreateElement("device_id");
                            xDeviceID.InnerText = simei;
                            root.AppendChild(xDeviceID);


                            XmlElement xDateTime = doc.CreateElement("datetime");
                            xDateTime.InnerText = date_time;
                            root.AppendChild(xDateTime);

                            XmlElement xNumSat = doc.CreateElement("num_sat");
                            xNumSat.InnerText = num_sat.ToString();
                            root.AppendChild(xNumSat);

                            XmlElement xLat = doc.CreateElement("lat");
                            if (south_north_lat == 0)
                                xLat.InnerText = "-" + lat.ToString();
                            else
                                xLat.InnerText = lat.ToString();
                            root.AppendChild(xLat);

                            XmlElement xLon = doc.CreateElement("lon");
                            if (east_west_lon == 1)
                                xLon.InnerText = "-" + lon.ToString();
                            else
                                xLon.InnerText = lon.ToString();
                            root.AppendChild(xLon);

                            XmlElement xspeed = doc.CreateElement("speed");
                            xspeed.InnerText = speed.ToString();
                            root.AppendChild(xspeed);


                            XmlElement xHeading = doc.CreateElement("heading");
                            xHeading.InnerText = heading.ToString();
                            root.AppendChild(xHeading);

                            XmlElement xMCC = doc.CreateElement("MCC");
                            xMCC.InnerText = MCC;
                            root.AppendChild(xMCC);

                            XmlElement xMNC = doc.CreateElement("MNC");
                            xMNC.InnerText = MNC;
                            root.AppendChild(xMNC);

                            XmlElement xLAC = doc.CreateElement("LAC");
                            xLAC.InnerText = LAC;
                            root.AppendChild(xLAC);

                            XmlElement xCellID = doc.CreateElement("cell_id");
                            xCellID.InnerText = cellID;
                            root.AppendChild(xCellID);

                            XmlElement xSerialNum = doc.CreateElement("serial_num");
                            xSerialNum.InnerText = serial_num;
                            root.AppendChild(xSerialNum);


                            XmlElement xTailChar = doc.CreateElement("tailchar");
                            xTailChar.InnerText = tail_char;
                            root.AppendChild(xTailChar);

                            //text position
                            int iMax_speed = 0;
                            string txtpos = string.Empty;


                            XmlElement xTextPos = doc.CreateElement("TextPos");
                            xTextPos.InnerText = txtpos;
                            root.AppendChild(xTextPos);

                            string road_info_alertid = "";
                            bool bRoadInfoID = false;


                            string xmlOutput = doc.OuterXml;



                        }

                    }
                }
            }
            catch (Exception ex)
            {



            }

        }

        private void AlarmMessage(string sMsg, string simei)
        {
            string[] arr_data = sMsg.Split('-');
            int year, month, day, hour, minute, second;
            protocol_id = arr_data[3];
            year = Convert.ToInt32(arr_data[4], 16);
            month = Convert.ToInt32(arr_data[5], 16);
            day = Convert.ToInt32(arr_data[6], 16);
            hour = Convert.ToInt32(arr_data[7], 16);
            minute = Convert.ToInt32(arr_data[8], 16);
            second = Convert.ToInt32(arr_data[9], 16);

            date_time = day.ToString().PadLeft(2, '0') + "/" + month.ToString().PadLeft(2, '0') + "/20" + year.ToString() + " " + hour.ToString().PadLeft(2, '0')
                + ":" + minute.ToString().PadLeft(2, '0') + ":" + second.ToString().PadLeft(2, '0');

            num_sat = Convert.ToInt32(arr_data[10].Substring(1, 1), 16);
            lat = (Convert.ToInt32(arr_data[11] + arr_data[12] + arr_data[13] + arr_data[14], 16) / 30000.0) / 60.0;
            lon = (Convert.ToInt32(arr_data[15] + arr_data[16] + arr_data[17] + arr_data[18], 16) / 30000.0) / 60.0;
            speed = Convert.ToInt32(arr_data[19], 16);
            course_stat = Convert.ToString(Convert.ToInt32(arr_data[20] + arr_data[21], 16), 2).PadLeft(16, '0');
            is_real_time_gps = int.Parse(course_stat.Substring(2, 1));
            gps_positioned = int.Parse(course_stat.Substring(3, 1));
            east_west_lon = int.Parse(course_stat.Substring(4, 1));
            south_north_lat = int.Parse(course_stat.Substring(5, 1));
            heading = Convert.ToInt32(course_stat.Substring(6, 10), 2);
            MCC = Convert.ToInt32(arr_data[23] + arr_data[24], 16).ToString();
            MNC = arr_data[25];
            LAC = arr_data[26] + arr_data[27];
            cellID = arr_data[28] + arr_data[29] + arr_data[30];

            byte[] bData = Helper.hexStringToByteArray(sMsg.Replace("-",""));
            string Status_information = Convert.ToString(bData[31], 2).PadLeft(8, '0');
            //Status information
            int alarm = Convert.ToInt32(Status_information.Substring(2, 3), 2);

            terminal_info = Convert.ToString(Convert.ToInt32(arr_data[31], 16), 2).PadLeft(8, '0');

            immob_stat = terminal_info.Substring(0, 1);
            gps_stat = terminal_info.Substring(1, 1);
            alarm_stat = terminal_info.Substring(2, 3);
            charge_stat = terminal_info.Substring(5, 1);
            ign_stat = terminal_info.Substring(6, 1);

            voltage_level = Convert.ToInt32(arr_data[32], 16).ToString();
            gsm_sig = arr_data[33];
            serial_num = arr_data[36] + arr_data[37];
            tail_char = arr_data[40] + arr_data[41];


            //xml stuff
            XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);

            doc.AppendChild(dec);// Create the root element
            XmlElement root = doc.CreateElement("GT06_Message");
            doc.AppendChild(root);

            //add the message
            XmlElement xProtocolID = doc.CreateElement("protocol_id");
            xProtocolID.InnerText = protocol_id;
            root.AppendChild(xProtocolID);

            XmlElement xRawMessage = doc.CreateElement("raw_message");
            xRawMessage.InnerText = sMsg;
            root.AppendChild(xRawMessage);

            XmlElement xDeviceID = doc.CreateElement("device_id");
            xDeviceID.InnerText = simei;
            root.AppendChild(xDeviceID);

            XmlElement xDateTime = doc.CreateElement("datetime");
            xDateTime.InnerText = date_time;
            root.AppendChild(xDateTime);

            XmlElement xNumSat = doc.CreateElement("num_sat");
            xNumSat.InnerText = num_sat.ToString();
            root.AppendChild(xNumSat);

            XmlElement xLat = doc.CreateElement("lat");
            if (south_north_lat == 0)
                xLat.InnerText = "-" + lat.ToString();
            else
                xLat.InnerText = lat.ToString();
            root.AppendChild(xLat);

            XmlElement xLon = doc.CreateElement("lon");
            if (east_west_lon == 1)
                xLon.InnerText = "-" + lon.ToString();
            else
                xLon.InnerText = lon.ToString();
            root.AppendChild(xLon);

            XmlElement xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);


            XmlElement xHeading = doc.CreateElement("heading");
            xHeading.InnerText = heading.ToString();
            root.AppendChild(xHeading);

            XmlElement xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            XmlElement xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            XmlElement xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            XmlElement xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            XmlElement xImmobStatus = doc.CreateElement("immobilizer_status");
            xImmobStatus.InnerText = immob_stat;
            root.AppendChild(xImmobStatus);

            XmlElement xGpsStat = doc.CreateElement("gpstracking_status");
            xGpsStat.InnerText = gps_stat;
            root.AppendChild(xGpsStat);

            XmlElement xAlarmStat = doc.CreateElement("alarm_status");
            xAlarmStat.InnerText = alarm_stat;
            root.AppendChild(xAlarmStat);

            XmlElement xChargeStat = doc.CreateElement("charge_status");
            xChargeStat.InnerText = charge_stat;
            root.AppendChild(xChargeStat);

            XmlElement xIgnStat = doc.CreateElement("ign_status");
            xIgnStat.InnerText = ign_stat;
            root.AppendChild(xIgnStat);

            XmlElement xVoltage = doc.CreateElement("voltage_level");
            xVoltage.InnerText = voltage_level;
            root.AppendChild(xVoltage);

            XmlElement xGsm = doc.CreateElement("gsm_signal");
            xGsm.InnerText = gsm_sig;
            root.AppendChild(xGsm);


            XmlElement xSerialNum = doc.CreateElement("serial_num");
            xSerialNum.InnerText = serial_num;
            root.AppendChild(xSerialNum);


            XmlElement xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            string txtpos = string.Empty;

            XmlElement xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos;
            root.AppendChild(xTextPos);


            string xmlOutput = doc.OuterXml;


        }

        public void status_message(string sMsg, string simei)
        {
            try
            {
                //process location/alarm data
                string[] arr_data = sMsg.Split('-');

                if (arr_data[3] == "13")
                {

                    protocol_id = arr_data[3];
                    terminal_info = Convert.ToString(Convert.ToInt32(arr_data[4], 16), 2).PadLeft(8, '0');
                    string tt = sMsg.Replace("-", "").Substring(8, 2);
                    immob_stat = terminal_info.Substring(0, 1);
                    gps_stat = terminal_info.Substring(1, 1);
                    alarm_stat = terminal_info.Substring(2, 3);
                    charge_stat = terminal_info.Substring(5, 1);
                    ign_stat = terminal_info.Substring(6, 1);

                    voltage_level = Convert.ToInt32(arr_data[5], 16).ToString();
                    gsm_sig = arr_data[6];
                    serial_num = arr_data[9] + arr_data[10];
                    tail_char = arr_data[13] + arr_data[14];



                    //xml stuff
                    XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
                    XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);

                    doc.AppendChild(dec);// Create the root element
                    XmlElement root = doc.CreateElement("GT06_Message");
                    doc.AppendChild(root);

                    //add the message
                    XmlElement xProtocolID = doc.CreateElement("protocol_id");
                    xProtocolID.InnerText = protocol_id;
                    root.AppendChild(xProtocolID);

                    XmlElement xDeviceID = doc.CreateElement("device_id");
                    xDeviceID.InnerText = simei;
                    root.AppendChild(xDeviceID);

                    XmlElement xDateTime = doc.CreateElement("datetime");
                    xDateTime.InnerText = DateTime.Now.ToString();
                    root.AppendChild(xDateTime);

                    XmlElement xImmobStatus = doc.CreateElement("immobilizer_status");
                    xImmobStatus.InnerText = immob_stat;
                    root.AppendChild(xImmobStatus);

                    XmlElement xGpsStat = doc.CreateElement("gpstracking_status");
                    xGpsStat.InnerText = gps_stat;
                    root.AppendChild(xGpsStat);

                    XmlElement xAlarmStat = doc.CreateElement("alarm_status");
                    xAlarmStat.InnerText = alarm_stat;
                    root.AppendChild(xAlarmStat);

                    XmlElement xChargeStat = doc.CreateElement("charge_status");
                    xChargeStat.InnerText = charge_stat;
                    root.AppendChild(xChargeStat);

                    XmlElement xIgnStat = doc.CreateElement("ign_status");
                    xIgnStat.InnerText = ign_stat;
                    root.AppendChild(xIgnStat);

                    XmlElement xVoltage = doc.CreateElement("voltage_level");
                    xVoltage.InnerText = voltage_level;
                    root.AppendChild(xVoltage);

                    XmlElement xGsm = doc.CreateElement("gsm_signal");
                    xGsm.InnerText = gsm_sig;
                    root.AppendChild(xGsm);


                    XmlElement xSerialNum = doc.CreateElement("serial_num");
                    xSerialNum.InnerText = serial_num;
                    root.AppendChild(xSerialNum);


                    XmlElement xTailChar = doc.CreateElement("tailchar");
                    xTailChar.InnerText = tail_char;
                    root.AppendChild(xTailChar);

                    //text position
                    string txtpos = string.Empty;



                    string xmlOutput = doc.OuterXml;


                }
            }
            catch (Exception ex)
            {




            }

        }

        private void OdoMessage(string sMsg, string simei)
        {

            string[] arr_data = sMsg.Split('-');
            if (arr_data[3] == "12")
            {
                int year, month, day, hour, minute, second;
                protocol_id = arr_data[3];
                year = Convert.ToInt32(arr_data[4], 16);
                month = Convert.ToInt32(arr_data[5], 16);
                day = Convert.ToInt32(arr_data[6], 16);
                hour = Convert.ToInt32(arr_data[7], 16);
                minute = Convert.ToInt32(arr_data[8], 16);
                second = Convert.ToInt32(arr_data[9], 16);

                date_time = day.ToString().PadLeft(2, '0') + "/" + month.ToString().PadLeft(2, '0') + "/20" + year.ToString() + " " + hour.ToString().PadLeft(2, '0')
                        + ":" + minute.ToString().PadLeft(2, '0') + ":" + second.ToString().PadLeft(2, '0');

                num_sat = Convert.ToInt32(arr_data[10].Substring(1, 1), 16);
                lat = (Convert.ToInt32(arr_data[11] + arr_data[12] + arr_data[13] + arr_data[14], 16) / 30000.0) / 60.0;
                lon = (Convert.ToInt32(arr_data[15] + arr_data[16] + arr_data[17] + arr_data[18], 16) / 30000.0) / 60.0;
                speed = Convert.ToInt32(arr_data[19], 16);
                course_stat = Convert.ToString(Convert.ToInt32(arr_data[20] + arr_data[21], 16), 2).PadLeft(16, '0');
                is_real_time_gps = int.Parse(course_stat.Substring(2, 1));
                gps_positioned = int.Parse(course_stat.Substring(3, 1));
                east_west_lon = int.Parse(course_stat.Substring(4, 1));
                south_north_lat = int.Parse(course_stat.Substring(5, 1));
                heading = Convert.ToInt32(course_stat.Substring(6, 10), 2);
                MCC = Convert.ToInt32(arr_data[22] + arr_data[23], 16).ToString();
                MNC = arr_data[24];
                LAC = arr_data[25] + arr_data[26];
                cellID = arr_data[27] + arr_data[28] + arr_data[29];
                odometer = (double.Parse(Convert.ToInt32(arr_data[30] + arr_data[31] + arr_data[32] + arr_data[33], 16).ToString()) / 1000).ToString();
                serial_num = arr_data[34] + arr_data[35];
                tail_char = arr_data[38] + arr_data[39];

                // 

                //xml stuff
                XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
                XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);

                doc.AppendChild(dec);// Create the root element
                XmlElement root = doc.CreateElement("GT06_Message");
                doc.AppendChild(root);

                //add the message
                XmlElement xProtocolID = doc.CreateElement("protocol_id");
                xProtocolID.InnerText = protocol_id;
                root.AppendChild(xProtocolID);

                XmlElement xRawMessage = doc.CreateElement("raw_message");
                xRawMessage.InnerText = sMsg;
                root.AppendChild(xRawMessage);


                XmlElement xDeviceID = doc.CreateElement("device_id");
                xDeviceID.InnerText = simei;
                root.AppendChild(xDeviceID);

                XmlElement xDateTime = doc.CreateElement("datetime");
                xDateTime.InnerText = date_time;
                root.AppendChild(xDateTime);

                XmlElement xNumSat = doc.CreateElement("num_sat");
                xNumSat.InnerText = num_sat.ToString();
                root.AppendChild(xNumSat);

                XmlElement xLat = doc.CreateElement("lat");
                if (south_north_lat == 0)
                {
                    lat = double.Parse("-" + lat.ToString());
                    xLat.InnerText = lat.ToString();
                }
                else
                    xLat.InnerText = lat.ToString();
                root.AppendChild(xLat);

                XmlElement xLon = doc.CreateElement("lon");
                if (east_west_lon == 1)
                {
                    lon = double.Parse("-" + lon.ToString());
                    xLon.InnerText = lon.ToString();
                }
                else
                    xLon.InnerText = lon.ToString();
                root.AppendChild(xLon);

                XmlElement xspeed = doc.CreateElement("speed");
                xspeed.InnerText = speed.ToString();
                root.AppendChild(xspeed);


                XmlElement xHeading = doc.CreateElement("heading");
                xHeading.InnerText = heading.ToString();
                root.AppendChild(xHeading);



                XmlElement xIgnStat = doc.CreateElement("ign_status");
                xIgnStat.InnerText = ign_stat;
                root.AppendChild(xIgnStat);


                XmlElement xMCC = doc.CreateElement("MCC");
                xMCC.InnerText = MCC;
                root.AppendChild(xMCC);

                XmlElement xMNC = doc.CreateElement("MNC");
                xMNC.InnerText = MNC;
                root.AppendChild(xMNC);

                XmlElement xLAC = doc.CreateElement("LAC");
                xLAC.InnerText = LAC;
                root.AppendChild(xLAC);

                XmlElement xCellID = doc.CreateElement("cell_id");
                xCellID.InnerText = cellID;
                root.AppendChild(xCellID);

                XmlElement xOdo = doc.CreateElement("odometer");
                xOdo.InnerText = odometer;
                root.AppendChild(xOdo);

                XmlElement xSerialNum = doc.CreateElement("serial_num");
                xSerialNum.InnerText = serial_num;
                root.AppendChild(xSerialNum);


                XmlElement xTailChar = doc.CreateElement("tailchar");
                xTailChar.InnerText = tail_char;
                root.AppendChild(xTailChar);


                //text position
                int iMax_speed = 0;

                string txtpos = string.Empty;


                XmlElement xTextPos = doc.CreateElement("TextPos");
                xTextPos.InnerText = txtpos;
                root.AppendChild(xTextPos);

                XmlElement xRoadSpeed = doc.CreateElement("max_road_speed");
                xRoadSpeed.InnerText = iMax_speed.ToString();
                root.AppendChild(xRoadSpeed);

                string xmlOutput = doc.OuterXml;

            }

        }
    }
}
