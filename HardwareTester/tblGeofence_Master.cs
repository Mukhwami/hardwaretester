﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class tblGeofence_Master
    {
        public decimal ipkGeoMID { get; set; }
        public decimal ifkDeviceID { get; set; }
        public decimal ifkUserID { get; set; }
        public Nullable<decimal> ifkCompanyID { get; set; }
        public Nullable<decimal> ifkGroupMID { get; set; }
        public string vGeoName { get; set; }
        public string vDescription { get; set; }
        public Nullable<System.DateTime> dCreateDate { get; set; }
        public Nullable<System.DateTime> dLastAccessed { get; set; }
        public Nullable<bool> bStatus { get; set; }
        public string cIN_OUT { get; set; }
        public string vGeofenceType { get; set; }
        public Nullable<int> vZoomleval { get; set; }
        public Nullable<int> iMaxSpeed { get; set; }
        public Nullable<double> iProximity { get; set; }
        public Nullable<double> iAreaSqMeters { get; set; }
        public Nullable<int> updatedBy { get; set; }
        public Nullable<bool> isPolygon { get; set; }
        public Nullable<int> ZoneTypeId { get; set; }
        public ICollection<tblGeofence_Detail> Polygons { get; set; }
    }

    public class tblGeofence_Detail
    {
        public decimal ipkGeoDID { get; set; }
        public Nullable<double> vLatitude { get; set; }
        public Nullable<double> vLongitude { get; set; }
        public Nullable<int> iGeoZoneTypeId { get; set; }
        public Nullable<decimal> ifkGeoMID { get; set; }
        public virtual tblGeofence_Detail GeofenceMaster { get; set; }

    }

    public class GeoMID
    {
        public int geoMID { get; set; }
        public string GeoName { get; set; }
        public string GeoStreetName { get; set; }
        public string GeofenceType { get; set; }
        public int ZoneTypeId { get; set; }
        public int iMaxSpeed { get; set; }
        public List<Loc> Loc { get; set; }
        public override string ToString()
        {
            return $"{GetHashCode()} {geoMID},{GeoName}"; 
        }

    }
    public class Loc
    {
        public double Lt { get; set; }
        public double Lg { get; set; }
        public override string ToString()
        {
            return $"{GetHashCode()} {Lt},{Lg}";
        }
    }
}
