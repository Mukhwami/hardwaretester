﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Xml;
using System.Text.RegularExpressions;
namespace HardwareTester
{
    public class cNoran
    {
        private const string queue_path = @".\private$\UTS_MQ_IN";
        public string Locate;
        public string AlarmType;
        public string Speed;
        public int Angle;
        public int Alarm;
        public double Lng;
        public double Lat;
        public string GPSDateTime;
        public string DevId;
        public string nIOStatus;
        public string nOilState;
        public string Enable;
        public string sUserID;
        public Int32[] sDateTime;
        public string txtpos;
        public double Odometer;
        public string RFID;
        public int Temperature;

       
        public void CreateMessage061(string pData)
        {
            Odometer = 0.00; Temperature = 0; RFID = "00000";
            var origaldata = pData.Split('-');
            //string YY,DD,M,HH,MM,SS;
            var origaldata2 = pData.Split('-')
                                 .Select(x => byte.Parse(x, NumberStyles.HexNumber))
                                 .ToArray();
            var bEnable = origaldata2.Skip(4).Take(1).ToArray();
            var ig_statuss = hex2binary(BitConverter.ToString(bEnable).Replace("-", ""));
            var ig_status = ig_statuss[6].ToString();


            Locate = origaldata[4];
            Alarm = origaldata2[5];
            Speed = origaldata[6];

            switch (Alarm)
            {
                case 11:
                    ig_status = "1";
                    break;
                case 12:
                    ig_status = "0";
                    break;
            }
            Angle = (int)BitConverter.ToSingle(origaldata2.Skip(7).Take(2).ToArray(), 0);
            Lng = BitConverter.ToSingle(origaldata2.Skip(16).Take(4).ToArray(), 0);
            Lat = BitConverter.ToSingle(origaldata2.Skip(20).Take(4).ToArray(), 0);
            DevId = Encoding.Default.GetString(origaldata2.Skip(4).Take(11).ToArray()).Replace("\0", "");
            GPSDateTime = GetTimeString(BitConverter.ToUInt32(origaldata2.Skip(16).Take(4).ToArray(), 0));
            GPSDateTime = GetTimeString(BitConverter.ToUInt32(origaldata2.Skip(20).Take(4).ToArray(), 0));
            var IMEI = Convert.ToInt64(Regex.Replace(DevId, "[A-Za-z ]", ""));
            var xSpeed = Convert.ToInt32(origaldata2[6]);
            //Odometer = getOdometer.GetOdommeter(IMEI.ToString(), Lat, Lng, xSpeed);
            Odometer = 0;
            var Device_Id = Regex.Replace(DevId, "[A-Za-z ]", "").TrimStart(new Char[] { '0' });
            

        }
        public void CreateMessage06(string pData)
        {

            Odometer = 0.00; Temperature = 0; RFID = "00000";
            var origaldata = pData.Split('-');
            //string YY,DD,M,HH,MM,SS;
            var origaldata2 = pData.Split('-')
                                 .Select(x => byte.Parse(x, NumberStyles.HexNumber))
                                 .ToArray();
            var bEnable = origaldata2.Skip(4).Take(1).ToArray();
            var ig_statuss = hex2binary(BitConverter.ToString(bEnable).Replace("-", ""));
            var ig_status = ig_statuss[6].ToString();


            Locate = origaldata[4];
            Alarm = origaldata2[5];
            Speed = origaldata[6];

            switch (Alarm)
            {
                case 11:
                    ig_status = "1";
                    break;
                case 12:
                    ig_status = "0";
                    break;
            }
            var dat = origaldata2.Skip(7).Take(2).ToArray().ToString();
            try
            {
                Angle = (int)BitConverter.ToSingle(origaldata2.Skip(7).Take(2).ToArray(), 0);
            }
            catch (Exception)
            {
                Angle = 0;
            }
            Lng = BitConverter.ToSingle(origaldata2.Skip(9).Take(4).ToArray(), 0);
            Lat = BitConverter.ToSingle(origaldata2.Skip(13).Take(4).ToArray(), 0);
            DevId = Encoding.Default.GetString(origaldata2.Skip(21).Take(11).ToArray()).Replace("\0", "");
            GPSDateTime = GetTimeString(BitConverter.ToUInt32(origaldata2.Skip(17).Take(4).ToArray(), 0));
            nIOStatus = origaldata[32];
            nOilState = origaldata[33];

            if (origaldata.Length == 41)
            {
                Temperature = BitConverter.ToUInt16(origaldata2.Skip(34).Take(2).ToArray(), 0);
                Odometer = BitConverter.ToSingle(origaldata2.Skip(36).Take(4).ToArray(), 0);
               
            }
            if (origaldata.Length == 49)
            {
                Odometer = BitConverter.ToSingle(origaldata2.Skip(45).Take(4).ToArray(), 0);
                RFID = Encoding.Default.GetString(origaldata2.Skip(34).Take(11).ToArray()).Replace("\0", "");
                
            }
            var IMEI = Convert.ToInt64(Regex.Replace(DevId, "[A-Za-z ]", ""));
            var xSpeed = Convert.ToInt32(origaldata2[6]);
            //Odometer = getOdometer.GetOdommeter(IMEI.ToString(), Lat, Lng, xSpeed);
            Odometer = 0;
            var Device_Id = Regex.Replace(DevId, "[A-Za-z ]", "").TrimStart(new Char[] { '0' });
            

            //road speed
            var iMax_speed = 0;

            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);

            doc.AppendChild(dec);// Create the root element
            var root = doc.CreateElement("NORAN006_Message");
            doc.AppendChild(root);


            var xPort = doc.CreateElement("port");
            xPort.InnerText = "6344";
            root.AppendChild(xPort);

            //add the message
            var xRawMessage = doc.CreateElement("raw_message");
            xRawMessage.InnerText = pData.Replace("-", "");
            root.AppendChild(xRawMessage);

            var xDeviceID = doc.CreateElement("device_id");
            xDeviceID.InnerText = DevId;
            root.AppendChild(xDeviceID);

            var xAlarmStat = doc.CreateElement("alarm_status");
            xAlarmStat.InnerText = Alarm.ToString();
            root.AppendChild(xAlarmStat);

            var xig_status = doc.CreateElement("ig_status");
            xig_status.InnerText = ig_status;
            root.AppendChild(xig_status);

            var xDateTime = doc.CreateElement("datetime");
            xDateTime.InnerText = "20" + GPSDateTime;
            root.AppendChild(xDateTime);

            var sspeed = Convert.ToInt32(Speed, 16);
            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = sspeed.ToString();
            root.AppendChild(xspeed);

            var xlon = doc.CreateElement("lon");
            xlon.InnerText = Lng.ToString();
            root.AppendChild(xlon);

            var xlat = doc.CreateElement("lat");
            xlat.InnerText = Lat.ToString();
            root.AppendChild(xlat);

            var xHeading = doc.CreateElement("heading");
            xHeading.InnerText = Angle.ToString();
            root.AppendChild(xHeading);

            var xMileage = doc.CreateElement("Odometer");
            xMileage.InnerText = Odometer.ToString();
            root.AppendChild(xMileage);

            var xRFID = doc.CreateElement("RFID");
            xRFID.InnerText = RFID;
            root.AppendChild(xRFID);

            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos;
            root.AppendChild(xTextPos);

            var xTemperature = doc.CreateElement("Temperature");
            xTemperature.InnerText = Temperature.ToString();
            root.AppendChild(xTemperature);

            var xmax_road_speed = doc.CreateElement("max_road_speed");
            xmax_road_speed.InnerText = iMax_speed.ToString();
            root.AppendChild(xmax_road_speed);

            var xmlOutput = doc.OuterXml;

            //Debug.WriteLine(xmlOutput);


        }
        public void CreateMessage_06(string pData)
        {

            Odometer = 0.00; Temperature = 0; RFID = "00000";
            var origaldata = pData.Split('-');
            //string YY,DD,M,HH,MM,SS;
            var origaldata2 = pData.Split('-')
                                 .Select(x => byte.Parse(x, NumberStyles.HexNumber))
                                 .ToArray();
            var bEnable = origaldata2.Skip(14).Take(1).ToArray();
            var ig_statuss = hex2binary(BitConverter.ToString(bEnable).Replace("-", ""));
            var ig_status = ig_statuss[6].ToString();


            Locate = origaldata[10];
            Alarm = origaldata2[11];
            Speed = origaldata[12];
            switch (Alarm)
            {
                case 11:
                    ig_status = "1";
                    break;
                case 12:
                    ig_status = "0";
                    break;
            }
            Angle = (int)BitConverter.ToSingle(origaldata2.Skip(13).Take(2).ToArray(), 0);
            Lng = BitConverter.ToSingle(origaldata2.Skip(15).Take(4).ToArray(), 0);
            Lat = BitConverter.ToSingle(origaldata2.Skip(19).Take(4).ToArray(), 0);
            DevId = Encoding.Default.GetString(origaldata2.Skip(27).Take(11).ToArray()).Replace("\0", "");
            GPSDateTime = GetTimeString(BitConverter.ToUInt32(origaldata2.Skip(23).Take(4).ToArray(), 0));
            nIOStatus = origaldata[38];
            nOilState = origaldata[39];
            var Device_Id = Regex.Replace(DevId, "[A-Za-z ]", "").TrimStart(new Char[] { '0' });
            

            //road speed
            var iMax_speed = 0;

            var IMEI = Convert.ToInt64(Regex.Replace(DevId, "[A-Za-z ]", ""));
            try
            {
                //29-00-09-80-00-00-00-00-00-00-01-0B-0E-B9-00-F5-19-13-42-8B-8E-A2-BF-4B-67-86-46-4E-52-30-39-46-30-31-32-39-37-00-00-00-14
                //0   1  2  3  4  5  6  7  8  9 10 11 12
                var xSpeed = Convert.ToInt32(origaldata2[12]);
                // Odometer = getOdometer.GetOdommeter(IMEI.ToString(), Lat, Lng, xSpeed);
                Odometer = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);

            doc.AppendChild(dec);// Create the root element
            var root = doc.CreateElement("NORAN006_Message");
            doc.AppendChild(root);

            var xPort = doc.CreateElement("port");
            xPort.InnerText = "6344";
            root.AppendChild(xPort);

            //add the message
            var xRawMessage = doc.CreateElement("raw_message");
            xRawMessage.InnerText = pData.Replace("-", "");
            root.AppendChild(xRawMessage);

            var xDeviceID = doc.CreateElement("device_id");
            xDeviceID.InnerText = DevId;
            root.AppendChild(xDeviceID);

            var xAlarmStat = doc.CreateElement("alarm_status");
            xAlarmStat.InnerText = Alarm.ToString();
            root.AppendChild(xAlarmStat);

            var xig_status = doc.CreateElement("ig_status");
            xig_status.InnerText = ig_status;
            root.AppendChild(xig_status);

            var xDateTime = doc.CreateElement("datetime");
            xDateTime.InnerText = "20" + GPSDateTime;
            root.AppendChild(xDateTime);

            var sspeed = Convert.ToInt32(Speed, 16);
            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = sspeed.ToString();
            root.AppendChild(xspeed);

            var xlon = doc.CreateElement("lon");
            xlon.InnerText = Lng.ToString();
            root.AppendChild(xlon);

            var xlat = doc.CreateElement("lat");
            xlat.InnerText = Lat.ToString();
            root.AppendChild(xlat);

            var xHeading = doc.CreateElement("heading");
            xHeading.InnerText = Angle.ToString();
            root.AppendChild(xHeading);

            var xMileage = doc.CreateElement("Odometer");
            xMileage.InnerText = Odometer.ToString();
            root.AppendChild(xMileage);

            var xRFID = doc.CreateElement("RFID");
            xRFID.InnerText = RFID;
            root.AppendChild(xRFID);

            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos;
            root.AppendChild(xTextPos);

            var xTemperature = doc.CreateElement("Temperature");
            xTemperature.InnerText = Temperature.ToString();
            root.AppendChild(xTemperature);

            var xmax_road_speed = doc.CreateElement("max_road_speed");
            xmax_road_speed.InnerText = iMax_speed.ToString();
            root.AppendChild(xmax_road_speed);

            var xmlOutput = doc.OuterXml;

            //Debug.WriteLine(xmlOutput);

          

        }

        public void CreateMessage08(string pData)
        {
            // 34-00-08-00-01-0B-00-00-00-00-00-00-26-43-5E-D4-12-42-15-D6-AA-BF-4E-52-30-39-42-30-39-31-36-31-00-00-31-37-2D-30-39-2D-30-38-20-31-33-3A-35-38-3A-31-39-00
            //  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
            Odometer = 0.00; Temperature = 0; RFID = "00000";
            var origaldata = pData.Split('-');
            //string YY,DD,M,HH,MM,SS;
            var origaldata2 = pData.Split('-')
                                 .Select(x => byte.Parse(x, NumberStyles.HexNumber))
                                 .ToArray();
            var bEnable = origaldata2.Skip(4).Take(1).ToArray();
            var ig_statuss = hex2binary(BitConverter.ToString(bEnable).Replace("-", ""));
            var ig_status = ig_statuss[6].ToString();
            Locate = origaldata[4];
            Alarm = origaldata2[5];
            switch (Alarm)
            {
                case 11:
                    ig_status = "1";
                    break;
                case 12:
                    ig_status = "0";
                    break;
            }
            Speed = BitConverter.ToInt32(origaldata2.Skip(6).Take(4).ToArray(), 0).ToString();
            Angle = (int)BitConverter.ToSingle(origaldata2.Skip(10).Take(4).ToArray(), 0);
            Lng = BitConverter.ToSingle(origaldata2.Skip(14).Take(4).ToArray(), 0);
            Lat = BitConverter.ToSingle(origaldata2.Skip(18).Take(4).ToArray(), 0);
            DevId = Encoding.Default.GetString(origaldata2.Skip(22).Take(11).ToArray()).Replace("\0", "");


            var nDatetime = origaldata2.Skip(33).Take(18).ToArray();
            var sData = BitConverter.ToString(nDatetime).ConvertFromHexToASCII();
            GPSDateTime = Encoding.ASCII.GetString(BitConverter.ToString(nDatetime).ConvertFromHexToASCII()).Replace("\0", "");
            var Device_Id = Regex.Replace(DevId, "[A-Za-z ]", "").TrimStart(new Char[] { '0' });
          

            var iMax_speed = 0;

            var IMEI = Convert.ToInt64(Regex.Replace(DevId, "[A-Za-z ]", ""));
            //if (Odometer == 0.0)
            //{
            //    Odometer = getOdometer.GetOdommeter(IMEI.ToString(), Lat, Lng, Convert.ToInt32(Speed));
            //}
            if (Angle > 360)
                Angle = Angle / 100;
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);

            doc.AppendChild(dec);// Create the root element
            var root = doc.CreateElement("NORAN006_Message");
            doc.AppendChild(root);

            var xPort = doc.CreateElement("port");
            xPort.InnerText = "6344";
            root.AppendChild(xPort);


            //add the message
            var xRawMessage = doc.CreateElement("raw_message");
            xRawMessage.InnerText = pData.Replace("-", "");
            root.AppendChild(xRawMessage);

            var xDeviceID = doc.CreateElement("device_id");
            xDeviceID.InnerText = DevId;
            root.AppendChild(xDeviceID);

            var xAlarmStat = doc.CreateElement("alarm_status");
            xAlarmStat.InnerText = Alarm.ToString();
            root.AppendChild(xAlarmStat);

            var xig_status = doc.CreateElement("ig_status");
            xig_status.InnerText = ig_status;
            root.AppendChild(xig_status);

            var xDateTime = doc.CreateElement("datetime");
            xDateTime.InnerText = "20" + GPSDateTime;
            root.AppendChild(xDateTime);

            
            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = Speed.ToString();
            root.AppendChild(xspeed);

            var xlon = doc.CreateElement("lon");
            xlon.InnerText = Lng.ToString();
            root.AppendChild(xlon);

            var xlat = doc.CreateElement("lat");
            xlat.InnerText = Lat.ToString();
            root.AppendChild(xlat);

            var xHeading = doc.CreateElement("heading");
            xHeading.InnerText = Angle.ToString();
            root.AppendChild(xHeading);

            var xMileage = doc.CreateElement("Odometer");
            xMileage.InnerText = Odometer.ToString();
            root.AppendChild(xMileage);

            var xRFID = doc.CreateElement("RFID");
            xRFID.InnerText = RFID;
            root.AppendChild(xRFID);

            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos;
            root.AppendChild(xTextPos);

            var xTemperature = doc.CreateElement("Temperature");
            xTemperature.InnerText = Temperature.ToString();
            root.AppendChild(xTemperature);

            var xmax_road_speed = doc.CreateElement("max_road_speed");
            xmax_road_speed.InnerText = iMax_speed.ToString();
            root.AppendChild(xmax_road_speed);

            var xmlOutput = doc.OuterXml;

            //Debug.WriteLine(xmlOutput);

           

        }

        public void CreateMessage_08(string pData)
        {
            // 3B -00-09-80-00-00-00-00-00-00-01-0B-00-00-00-00-00-00-98-42-5E-D4-12-42-06-E0-AA-BF-4E-52-30-39-42-30-39-31-36-31-00-00-31-37-2D-30-39-2D-31-34-20-31-33-3A-34-31-3A-35-30-00-14
            // 1  2  3  4  5  6  7  8   9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59
            // 3B-00-09-80-00-00-00-00-00-00-01-0B-14-00-00-00-00-80-B0-43-1F-D2-12-42-BB-34-AB-BF-4E-52-30-39-42-30-39-31-36-31-00-00-31-37-2D-31-30-2D-30-32-20-31-30-3A-34-36-3A-32-35-00-14

            Odometer = 0.00; Temperature = 0; RFID = "00000";
            var origaldata = pData.Split('-');
            //string YY,DD,M,HH,MM,SS;
            var origaldata2 = pData.Split('-')
                                 .Select(x => byte.Parse(x, NumberStyles.HexNumber))
                                 .ToArray();
            var bEnable = origaldata2.Skip(4).Take(1).ToArray();
            var ig_statuss = hex2binary(BitConverter.ToString(bEnable).Replace("-", ""));
            var ig_status = ig_statuss[6].ToString();
            Locate = origaldata[10];
            Alarm = origaldata2[11];
            switch (Alarm)
            {
                case 11:
                    ig_status = "1";
                    break;
                case 12:
                    ig_status = "0";
                    break;
            }
            Speed = BitConverter.ToInt32(origaldata2.Skip(12).Take(4).ToArray(), 0).ToString();
            var speed = Convert.ToInt32(Speed);
            Angle = (int)BitConverter.ToSingle(origaldata2.Skip(16).Take(4).ToArray(), 0);
            double Course = BitConverter.ToSingle(origaldata2.Skip(16).Take(4).ToArray(), 0);
            Lng = BitConverter.ToSingle(origaldata2.Skip(20).Take(4).ToArray(), 0);
            Lat = BitConverter.ToSingle(origaldata2.Skip(24).Take(4).ToArray(), 0);
            DevId = Encoding.Default.GetString(origaldata2.Skip(28).Take(11).ToArray()).Replace("\0", "");

            var nDatetime = origaldata2.Skip(40).Take(18).ToArray();
            var sData = BitConverter.ToString(nDatetime).ConvertFromHexToASCII();
            GPSDateTime = Encoding.ASCII.GetString(BitConverter.ToString(nDatetime).ConvertFromHexToASCII()).Replace("\0", "");
            var Device_Id = Regex.Replace(DevId, "[A-Za-z ]", "").TrimStart(new Char[] { '0' });
            

            if (Lng == 0.0 && Lat == 0)
                return;
            var iMax_speed = 0;

            var IMEI = Convert.ToInt64(Regex.Replace(DevId, "[A-Za-z ]", ""));
            //Odometer = getOdometer.GetOdommeter(IMEI.ToString(), Lat, Lng, Convert.ToInt32(Speed));
            Odometer = 0;
            if (Angle > 360)
                Angle = Angle / 100;
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);

            doc.AppendChild(dec);// Create the root element
            var root = doc.CreateElement("NORAN006_Message");
            doc.AppendChild(root);

            //add the message
            var xRawMessage = doc.CreateElement("raw_message");
            xRawMessage.InnerText = pData.Replace("-", "");
            root.AppendChild(xRawMessage);

            var xPort = doc.CreateElement("port");
            xPort.InnerText = "6344";
            root.AppendChild(xPort);

            var xDeviceID = doc.CreateElement("device_id");
            xDeviceID.InnerText = DevId;
            root.AppendChild(xDeviceID);

            var xAlarmStat = doc.CreateElement("alarm_status");
            xAlarmStat.InnerText = Alarm.ToString();
            root.AppendChild(xAlarmStat);

            var xig_status = doc.CreateElement("ig_status");
            xig_status.InnerText = ig_status;
            root.AppendChild(xig_status);

            var xDateTime = doc.CreateElement("datetime");
            xDateTime.InnerText = "20" + GPSDateTime;
            root.AppendChild(xDateTime);

        
            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = Speed.ToString();
            root.AppendChild(xspeed);

            var xlon = doc.CreateElement("lon");
            xlon.InnerText = Lng.ToString();
            root.AppendChild(xlon);

            var xlat = doc.CreateElement("lat");
            xlat.InnerText = Lat.ToString();
            root.AppendChild(xlat);

            var xHeading = doc.CreateElement("heading");
            xHeading.InnerText = Angle.ToString();
            root.AppendChild(xHeading);

            var xMileage = doc.CreateElement("Odometer");
            xMileage.InnerText = Odometer.ToString();
            root.AppendChild(xMileage);

            var xRFID = doc.CreateElement("RFID");
            xRFID.InnerText = RFID;
            root.AppendChild(xRFID);

            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos;
            root.AppendChild(xTextPos);

            var xTemperature = doc.CreateElement("Temperature");
            xTemperature.InnerText = Temperature.ToString();
            root.AppendChild(xTemperature);

            var xmax_road_speed = doc.CreateElement("max_road_speed");
            xmax_road_speed.InnerText = iMax_speed.ToString();
            root.AppendChild(xmax_road_speed);

            var xmlOutput = doc.OuterXml;

           

        }




















        public string GetTimeString(UInt32 nDateTime)
        {
            UInt32 ndate = 0;
            var sb = new StringBuilder();
            ndate = nDateTime >> 26;//year
            sb.Append((ndate / 10).ToString());
            sb.Append((ndate % 10).ToString() + "-");
            ndate = nDateTime >> 22 & 0x0f;//month f=15
            sb.Append((ndate / 10).ToString());
            sb.Append((ndate % 10).ToString() + "-");
            ndate = nDateTime >> 17 & 0x1f;//day 2f=31
            sb.Append((ndate / 10).ToString());
            sb.Append((ndate % 10).ToString() + " ");
            ndate = nDateTime >> 12 & 0x1f;//hour 3f=63
            sb.Append((ndate / 10).ToString());
            sb.Append((ndate % 10).ToString() + ":");
            ndate = nDateTime >> 6 & 0x3f;//minute
            sb.Append((ndate / 10).ToString());
            sb.Append((ndate % 10).ToString() + ":");
            ndate = nDateTime & 0x3f;//second
            sb.Append((ndate / 10).ToString());
            sb.Append((ndate % 10).ToString());

            return sb.ToString();
        }
        public static string hex2binary(string hexvalue)
        {

            return String.Join(String.Empty, hexvalue.Select(c => Convert.ToString(Convert.ToUInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
        }
    }
}
