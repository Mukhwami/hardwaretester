﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class Helper
    {
        public static string ChangeFromBigToLittleEndian(string hex)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hex.Length; i++)
            {
                sb.Append(hex.Substring(i * 2, 2));
            }
            return sb.ToString();
        }
        public static DateTime FromUnixTime(string unixTimes)
        { //1970, 1, 1, 0, 0, 0, 0
            var date = DateTime.UtcNow;

            try
            {

                var totalvalue = unixTimes.Substring(0, 10) + "." + unixTimes.Substring(10, unixTimes.Length - 10);
                var unixTime = Convert.ToDouble(totalvalue);
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                date = epoch.AddSeconds(unixTime);
            }
            catch (Exception ex)
            {
                date = DateTime.UtcNow;
            }

            //hack remove

            return date;
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            try
            {
                unixTimeStamp = unixTimeStamp / 1000;
                // Unix timestamp is seconds past epoch
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
                return dtDateTime;
            }
            catch
            {
                return DateTime.Now;
            }
        }
        public static DateTime UnixTimeStampToDateTimeRuptela(double unixTimeStamp)
        {
            try
            {
                // Unix timestamp is seconds past epoch
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
                return dtDateTime;
            }
            catch
            {
                return DateTime.Now;
            }
        }

        public static  Byte[] hexStringToByteArray(string hexinput)
        {
            if (hexinput == String.Empty)
                return null;
            if (hexinput.Length % 2 == 1)
                hexinput = "0" + hexinput;
            var arr_size = hexinput.Length / 2;
            var myBytes = new Byte[arr_size];
            for (var i = 0; i < arr_size; i++)
                myBytes[i] = Convert.ToByte(hexinput.Substring(i * 2, 2), 16);
            return myBytes;
        }

        public static DateTime TimeStampToDateTime(double TimeStamp)
        {
            var dtDateTime = new DateTime(2007, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(TimeStamp);
            return dtDateTime;
        }

        public static float GetFloatIEE754(byte[] array)
        {
            Array.Reverse(array);
            return BitConverter.ToSingle(array, 0);
        }

        public static string Reverse(string s)
        {
            var charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
    public static class Convert_from_Hex_Ascii
    {
        public static string Reverse(this string s)
        {
            var charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
        public static Byte[] HexStringToByteArray(string hexinput)
        {

            if (hexinput == String.Empty)
                return null;
            if (hexinput.Length % 2 == 1)
                hexinput = "0" + hexinput;
            var arr_size = hexinput.Length / 2;
            var myBytes = new Byte[arr_size];
            for (var i = 0; i < arr_size; i++)
                myBytes[i] = Convert.ToByte(hexinput.Substring(i * 2, 2), 16);
            return myBytes;
        }
        public static byte[] ConvertFromHexToASCII(this string hex)
        {
            hex = hex.Replace("-", "");
            var raw = new byte[hex.Length / 2];
            for (var i = 0; i < raw.Length; i++)
            {
                raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return raw;
        }
        public static byte[] ToByteArray(this String HexString)
        {
            var NumberChars = HexString.Length;
            var bytes = new byte[NumberChars / 2];
            for (var i = 0; i < NumberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(HexString.Substring(i, 2), 16);
            }
            return bytes;
        }
        public static string GetAsciText(string hexString)
        {
            var sb = new StringBuilder();
            try
            {
                for (var i = 0; i < hexString.Length; i += 2)
                {
                    var hs = hexString.Substring(i, 2);
                    sb.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
                }
            }
            catch (Exception)
            {
                return hexString;

            }
            return sb.ToString();
        }
        public static string ToHex(this string input)
        {
            var sb = new StringBuilder();
            foreach (var c in input)
                sb.AppendFormat("0x{0:X2} ", (int)c);
            return sb.ToString().Trim();
        }
        public static List<clsCommon> GetListOfGosafePostions(string pdata)
        {
            var str_data = GetAsciText(pdata);
            var lstOfPositions = new List<clsCommon>();
            var CommonObj = new clsCommon();
            try
            {
                var split_options = new string[] { "GS16", "GS46", "GS06", "SYS:", "GPS:", "GSM:", "COT:", "ADC:", "DTT:", "OBD:", "FUL:" };
                var sMain = str_data.Split('$');
                if (sMain.Length > 1)
                {
                    var simei_datetime = ",,";
                    for (var i = 0; i < sMain.Length; i++)
                    {

                        if (i == 0)
                        {
                            var local_data = sMain[i].Split(split_options, StringSplitOptions.None);
                            simei_datetime = local_data[1];
                            CommonObj = GetGosafeCommonObj(local_data[1], sMain[i]);
                        }
                        else
                        {
                            var local_data = sMain[i].Split(split_options, StringSplitOptions.None);
                            CommonObj = GetGosafeCommonObj(simei_datetime, sMain[i], i);
                        }
                        if (CommonObj != null)
                            lstOfPositions.Add(CommonObj);
                        CommonObj = null;
                    }
                }
                else
                {
                    var comb_data = str_data.Split('#');
                    if (comb_data.Length > 2)
                    {
                        var all_data = comb_data[1].Split(split_options, StringSplitOptions.None);
                        CommonObj = GetGosafeCommonObj(all_data[1], comb_data[1]);
                    }
                    else
                    {
                        var all_data = str_data.Split(split_options, StringSplitOptions.None);
                        CommonObj = GetGosafeCommonObj(all_data[1], str_data);
                    }
                    if (CommonObj != null)
                        lstOfPositions.Add(CommonObj);
                    CommonObj = null;
                }
            }
            catch (Exception e)
            {
              
            }
            return lstOfPositions;

        }

        private static clsCommon GetGosafeCommonObj(string IMEI_datetime, string sData, int x_time = 0)
        {
            var EventId = "";
            var datetime = "";
            var odometer = "0";
            var external_ps_voltage = "0";
            var batt_voltage = "0";
            var event_status = "0";
            var CommonObj = new clsCommon();
            CommonObj.raw_data = sData;
            var split_options = Regex.Split(sData, @"(GS06)|(GS16)|(GS46)|(SYS:)|(GPS:)|(GSM:)|(COT:)|(ADC:)|(DTT:)|(OBD:)|(FUL:)").Where((s, i) => s != "").ToArray();
            var simei = IMEI_datetime.Split(',');
            try
            {
                datetime = IMEI_datetime.Split(',')[2].Substring(6, 2) + "/" + IMEI_datetime.Split(',')[2].Substring(8, 2) + "/20" + IMEI_datetime.Split(',')[2].Substring(10, 2) + " " +
                  IMEI_datetime.Split(',')[2].Substring(0, 2) + ":" + IMEI_datetime.Split(',')[2].Substring(2, 2) + ":" + IMEI_datetime.Split(',')[2].Substring(4, 2);
                var DevId = Regex.Replace(IMEI_datetime.Split(',')[2], @"\D+", "");
                var dateTest = Convert.ToInt64(DevId);
                if (dateTest == 0)
                    datetime = DateTime.Now.ToString();
                CommonObj.dGPSDateTime = Convert.ToDateTime(datetime).ToString();
                EventId = IMEI_datetime.Split(',')[3];
                if (x_time > 0)
                {
                    var split_opts = new string[] { "GS16", "GS46", "GS06", "SYS:", "GPS:", "GSM:", "COT:", "ADC:", "DTT:", "OBD:", "FUL:" };
                    var new_date_evt = sData.Split(split_opts, StringSplitOptions.None);
                    datetime = new_date_evt[0].Substring(6, 2) + "/" + new_date_evt[0].Substring(8, 2) + "/20" + new_date_evt[0].Substring(10, 2) + " " +
                    new_date_evt[0].Substring(0, 2) + ":" + new_date_evt[0].Substring(2, 2) + ":" + new_date_evt[0].Substring(4, 2);
                    DevId = Regex.Replace(new_date_evt[0], @"\D+", "");
                    dateTest = Convert.ToInt64(DevId);
                    if (dateTest == 0)
                        datetime = DateTime.Now.ToString();
                    CommonObj.dGPSDateTime = Convert.ToDateTime(datetime).ToString();
                    EventId = new_date_evt[0].Split(',')[1];
                }

                for (var i = split_options.Length - 1; i >= 0; i--)
                {
                    switch (split_options[i])
                    {
                        case "GPS:"://gps params
                            var gps_params = split_options[i + 1].Split(';');
                            if (gps_params[2].Substring(0, 1) == "S")
                                CommonObj.Latitude = gps_params[2].Replace('S', '-');
                            if (gps_params[2].Substring(0, 1) == "N")
                                CommonObj.Latitude = gps_params[2].Replace('N', ' ').TrimStart();
                            if (gps_params[3].Substring(0, 1) == "W")
                                CommonObj.Longitude = gps_params[3].Replace('W', '-');
                            if (gps_params[3].Substring(0, 1) == "E")
                                CommonObj.Longitude = gps_params[3].Replace('E', ' ').TrimStart();
                            CommonObj.VehicleSpeed = Convert.ToDecimal(gps_params[4]);
                            CommonObj.Heading = Convert.ToInt32(gps_params[5]);
                            CommonObj.nAltitude = gps_params[6];
                            break;
                        case "COT:":
                            var cot_params = split_options[i + 1].Split(';');
                            odometer = "0";
                            try
                            {
                                if (cot_params.Length == 1)
                                    odometer = (double.Parse(split_options[i + 1]) / 1000).ToString();
                                if (cot_params.Length > 1)
                                {
                                    if (cot_params[0].Split(';')[0] != "")
                                        odometer = (double.Parse(cot_params[0].Split(';')[0]) / 1000).ToString();
                                }
                            }
                            catch (Exception e)
                            {
                                odometer = "0";
                            }
                            CommonObj.Odometer = Convert.ToDecimal(odometer);
                            break;
                        case "ADC:":
                            var adc_params = split_options[i + 1].Split(';');
                            external_ps_voltage = adc_params[0];
                            batt_voltage = adc_params[1].TrimEnd(',');
                            CommonObj.iBatteryBackup = (int)ConvertBatteryVoltageToPercentage(4.5, 0.0, Convert.ToDouble(batt_voltage));
                            break;
                        case "DTT:":
                            var dev_status = "0";
                            var device_params = split_options[i + 1].Split(';');
                            dev_status = device_params[0];
                            if(split_options[1].Equals("GS46") || split_options[1].Equals("GS16"))
                                dev_status = device_params[0];
                            else
                                dev_status = device_params[1];
                            if (dev_status == "")
                                dev_status = device_params[1];
                            var statusnumber = Convert.ToInt64(dev_status, 16).ToString();
                            dev_status = ToBinary(Convert.ToInt64(dev_status, 16)).PadRight(25, '0');
                            var ign_status = 0;
                            ign_status = Convert.ToInt16(dev_status.Substring(13, 1));
                            CommonObj.bIsIgnitionOn = Convert.ToBoolean(ign_status);
                            break;
                        case "FUL:":
                            CommonObj.Fuel1_Litres = Convert.ToSingle(split_options[i + 1].TrimEnd('#'));
                            break;
                        case "OBD:":
                            CommonObj.ObdData = GetGosafeObdData(split_options[i + 1].TrimEnd(','));
                            break;
                    }

                }
                //handle events
                if (EventId != "")
                {
                    var vEvenId = Convert.ToInt32(EventId, 16);
                    var s_bin = Convert.ToString(vEvenId, 2).PadLeft(8, '0');
                    event_status = s_bin.Substring(0, 1);
                    EventId = Convert.ToInt32(s_bin.Substring(1, 7), 2).ToString();
                }
                CommonObj.ReportID = GetGosafeEventId(EventId, event_status);
                Console.WriteLine(JsonConvert.SerializeObject(CommonObj,Newtonsoft.Json.Formatting.None, new JsonSerializerSettings { NullValueHandling=NullValueHandling.Ignore}));
                return CommonObj;

            }
            catch (Exception e)
            {
                 return null;
            }
        }
        public static int GetGosafeEventId(string eventId, string _event_status)
        {
            var ReportID = 124;
            switch (eventId)
            {
                case "": //position report
                    ReportID = 124;
                    break;

                case "0": //tow
                    if (_event_status == "1") { ReportID = 5; }
                    else if (_event_status == "0") { ReportID = 16; }
                    break;

                case "1": //idle
                    if (_event_status == "1") { ReportID = 4; }
                    else if (_event_status == "0") { ReportID = 11; }
                    break;

                case "2": //parking
                    if (_event_status == "1") { ReportID = 7; }
                    else if (_event_status == "0") { ReportID = 8; }
                    //ReportID = ;
                    break;

                case "3": //overspeed
                    if (_event_status == "1") { ReportID = 1; }
                    else if (_event_status == "0") { ReportID = 6; }
                    break;

                case "4": //gsm jamming
                    if (_event_status == "1") { ReportID = 19; }
                    else if (_event_status == "0") { ReportID = 32; } //no gsm jamming off at the momemnt
                    break;

                case "8": //harsh braking
                    if (_event_status == "1") { ReportID = 5; }
                    else if (_event_status == "0") { ReportID = 16; }
                    ReportID = 2;
                    break;

                case "9": //harsh acceleration
                    if (_event_status == "1") { ReportID = 3; }
                    //else if (_event_status == "0")  { ReportID =16; }
                    break;

                case "10": //Harsh Cornering - Left
                    if (_event_status == "1") { ReportID = 14; }
                    else if (_event_status == "0") { ReportID = 15; }
                    break;

                case "11": //Harsh Cornering - Right
                    if (_event_status == "1") { ReportID = 14; }
                    else if (_event_status == "0") { ReportID = 15; }
                    break;

                case "12": //impact
                    if (_event_status == "1") { ReportID = 20; }
                    // else if (_event_status == "0")  { ReportID =; }
                    ReportID = 20;
                    break;

                case "14": //External Power Supply
                    if (_event_status == "1") { ReportID = 24; }
                    else if (_event_status == "0") { ReportID = 25; }
                    break;

                case "15": //Backup Battery  0 - normal, 1 - Low battery: no charging event
                    if (_event_status == "1") { ReportID = 28; }
                    else if (_event_status == "0") { ReportID = 31; } //TODO ALERT
                    break;

                case "16": //Immobiler Status
                    if (_event_status == "1") { ReportID = 35; } //TODO ALERT
                    else if (_event_status == "0") { ReportID = 36; } //TODO ALERT
                    break;

                case "21": //Overrevving
                    if (_event_status == "1") { ReportID = 21; }
                    else if (_event_status == "0") { ReportID = 37; } //TODO ALERT
                    break;

                case "22": //Engine Overheating TODO ALERT
                    if (_event_status == "1") { ReportID = 44; }
                    else if (_event_status == "0") { ReportID = 45; }
                    break;


                case "26": //ignition status
                    if (_event_status == "1") { ReportID = 7; }
                    else if (_event_status == "0") { ReportID = 8; }
                    break;

                case "28": //ign
                    if (_event_status == "1") { ReportID = 7; }
                    else if (_event_status == "0") { ReportID = 8; }
                    break;

                default: //809 etc
                    if (eventId.Length > 0)
                    {
                        ReportID = Convert.ToInt32(eventId);
                    }
                    break;
            }
            return ReportID;
        }
        public static string ToBinary(Int64 Decimal)
        {
            // Declare a few variables we're going to need
            Int64 BinaryHolder;
            char[] BinaryArray;
            var BinaryResult = "";

            while (Decimal > 0)
            {
                BinaryHolder = Decimal % 2;
                BinaryResult += BinaryHolder;
                Decimal = Decimal / 2;
            }

            // The algoritm gives us the binary number in reverse order (mirrored)
            // We store it in an array so that we can reverse it back to normal
            BinaryArray = BinaryResult.ToCharArray();
            //Array.Reverse(BinaryArray);
            BinaryResult = new string(BinaryArray);

            return BinaryResult.PadRight(8, '0');
        }
        public static string GetGosafeObdData(string hex)
        {
    
            var jsonObdParams = "";
            var Arr_data = hex.Split(':');
            var data_length = 0;
            var OBD = Arr_data[Arr_data.Length - 1];
            string obd_pid_mode;
            var obd_pid = "";
            var obd_data = "";
            var str_desc = "";
            var sb = new StringBuilder();
            try
            {

                sb.Append("{");
                //int pos = 0;
                OBD = OBD.TrimEnd(',').Replace("OBD:", "");
                //read the first data length
                while (OBD.Length > 0)
                {
                    //data_length = (int.Parse(OBD.Substring(0, 2)) * 2) + 2;
                    data_length = (Convert.ToInt32(OBD.Substring(0, 2), 16) * 2) + 2;
                    if (data_length > 14) break;
                    var j = 0;
                    j = j + 2;
                    obd_pid_mode = OBD.Substring(j, 2);
                    j = j + 2;
                    obd_pid = OBD.Substring(j, 2);
                    var pid_desc = Program.OBD_PIDS.Where(key => key.Key.Contains(obd_pid))
                        .Select(key => key.Value).FirstOrDefault();
                    str_desc = pid_desc.ToString();
                    j = j + 2;
                    obd_data = OBD.Substring(j, (data_length - j));
                    if (obd_pid == "0C")
                        obd_data = Convert.ToString(Convert.ToInt32(obd_data, 16) / 4);
                    else
                        obd_data = Convert.ToInt32(obd_data, 16).ToString();
                    sb.Append("\"" + str_desc + "\"" + ":\"" + obd_data + "\",");
                    OBD = OBD.Substring(data_length, OBD.Length - data_length);
                    // sb.Append("}");
                }

            }
            catch (Exception ex)
            {
                

            }

            try
            {
                sb.Append("}");
                if (sb.ToString() != "")
                {
                    var test = JsonConvert.DeserializeObject<ObdParams>(sb.ToString().Replace(",}", "}"));
                    jsonObdParams = JsonConvert.SerializeObject(test);
                    var obdParams = JsonConvert.DeserializeObject<ObdParams>(jsonObdParams);
                    var oBD = new OBD()
                    {
                        BPC = obdParams.Boost_Pressure_Control,
                        LTFBank1 = obdParams.Long_Term_Fuel_Bank1,
                        TbCTemperature = obdParams.Turbocharge_Temperature,
                        PIDS_Supported_41_60 = obdParams.PIDS_Supported_41_60,
                        TimeAdvance = obdParams.TimeAdvance,
                        EngFuelRate = obdParams.EngFuelRate,
                        RPM = obdParams.RPM,
                        CTemp = $"{obdParams.CTemp}& deg",
                        FuelL = obdParams.FuelL,
                        FuelT = obdParams.FuelT,
                        AirFlw = obdParams.AirFlw,
                        IaT = obdParams.IaT,
                        TPos = obdParams.TPos,
                        //EngHrs = obdParams.EngHrs,
                        MStatus = obdParams.MStatus
                    };
                    jsonObdParams = JsonConvert.SerializeObject(oBD, Newtonsoft.Json.Formatting.None,
                                                                                              new JsonSerializerSettings
                                                                                              {
                                                                                                  NullValueHandling = NullValueHandling.Ignore
                                                                                              });
                    if (jsonObdParams == "{}")
                        jsonObdParams = "";
                }
            }
            catch (Exception en)
            {
               jsonObdParams = OBD;
            }
            return jsonObdParams;
        }
        public static double ConvertBatteryVoltageToPercentage(double MaxVolt, double MinVolt, double CurrentVoltBatteryReading)
        {

            var percentage = CurrentVoltBatteryReading;
            if (CurrentVoltBatteryReading < MinVolt)
            {
                var min = CurrentVoltBatteryReading;
                var range = (MaxVolt - min);
                var valueAboveMin = (CurrentVoltBatteryReading - min);
                percentage = (valueAboveMin / range) * 100;
            }
            else if (CurrentVoltBatteryReading > MaxVolt)
            {
                //percentage = CurrentVoltBatteryReading;
                var range = (CurrentVoltBatteryReading - MinVolt);
                var valueAboveMin = (CurrentVoltBatteryReading - MinVolt);
                percentage = (valueAboveMin / range) * 100;
            }
            else
            {
                var range = (MaxVolt - MinVolt);
                var valueAboveMin = (CurrentVoltBatteryReading - MinVolt);
                percentage = (valueAboveMin / range) * 100;
            }
            if (percentage > 100)
                percentage = 100;
            if (percentage < 0)
                percentage = 0;
            return percentage;

        }
    }
}
