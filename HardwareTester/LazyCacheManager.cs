﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LazyCache;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;

namespace HardwareTester
{
    public  class LazyCacheManager
    {
         public static List<TraccarDevices> CacheGetTraccarDevices()
        {
            var lstOfTraccarDevices = new List<TraccarDevices>();
            Console.Write("Loading Traccar Devices.");
            try
            {
                var url = TraccarApi.TraccarBaseUrl + "/api/devices/";
                var byteArray = Encoding.ASCII.GetBytes(TraccarApi.TraccarUsername + ":" + TraccarApi.TraccarPassword);
                using(var client=new HttpClient())
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync(url).Result;
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            {

                                response.EnsureSuccessStatusCode();
                                var res = response.Content.ReadAsStringAsync().Result;
                                if (response.IsSuccessStatusCode)
                                {

                                    var _devices = JsonConvert.DeserializeObject<object>(res);
                                    var lst = new List<Object>((IEnumerable<Object>)_devices);
                                    for (var j = 0; j < lst.Count; j++)
                                    {
                                        try
                                        {
                                            var devices = JsonConvert.DeserializeObject<TraccarDevices>(lst[j].ToString());
                                            devices.UniqueId = Regex.Replace(devices.UniqueId, "[A-Za-z ]", "");
                                            devices.DeviceId = Convert.ToInt64(devices.UniqueId);
                                            lstOfTraccarDevices.Add(devices);
                                            Console.Write(".");
                                        }
                                        catch (Exception e)
                                        {
                                            continue;
                                        }
                                    }

                                }

                            }
                            break;
                        default:
                            lstOfTraccarDevices = null;
                            break;

                    }
                }
            }
            catch (Exception e)
            {
                lstOfTraccarDevices = null;
            }
            return lstOfTraccarDevices;
        }

        public static List<tblGeofence_Master> GetGeofenceMaster()
        {
            List<tblGeofence_Master> lst = new List<tblGeofence_Master>();
            lst.Add(new tblGeofence_Master
            {
                ipkGeoMID= 80067,
                ifkCompanyID=6,
                ifkDeviceID=0,
                ifkUserID=1292,
                ifkGroupMID=5024,
                vGeoName= "NTQOffice",
                vDescription= "GeoZone",
                bStatus= true,
                cIN_OUT="OUT",
                vGeofenceType="Location",
                vZoomleval=18,
                iMaxSpeed=12,
                isPolygon=true,
                ZoneTypeId=0
            });
            lst.Add(new tblGeofence_Master
            {
                ipkGeoMID = 80068,
                ifkCompanyID = 6,
                ifkDeviceID = 0,
                ifkUserID = 1292,
                ifkGroupMID = 5024,
                vGeoName = "WLT OFFICE",
                vDescription = "GeoZone",
                bStatus = true,
                cIN_OUT = "IN",
                vGeofenceType = "Location",
                vZoomleval = 18,
                iMaxSpeed = 12,
                isPolygon = true,
                ZoneTypeId = 0
            });
            return lst;
        }

        public static List<tblGeofence_Detail> GetGeofenceDetail()
        {
            List<tblGeofence_Detail> lst = new List<tblGeofence_Detail>();
            lst.Add(new tblGeofence_Detail { ipkGeoDID= 102312 ,ifkGeoMID= 80067 ,vLatitude= -27.4332036168203, vLongitude= 153.088839032785 ,iGeoZoneTypeId=0});
            lst.Add(new tblGeofence_Detail { ipkGeoDID = 102313, ifkGeoMID = 80067, vLatitude = -27.4341701324155, vLongitude = 153.089053609503, iGeoZoneTypeId = 0 });
            lst.Add(new tblGeofence_Detail { ipkGeoDID = 102314, ifkGeoMID = 80067, vLatitude = -27.4341701324155, vLongitude = 153.089053609503, iGeoZoneTypeId = 0 });
            lst.Add(new tblGeofence_Detail { ipkGeoDID = 102315, ifkGeoMID = 80067, vLatitude = -27.4332893180525, vLongitude = 153.091886022225, iGeoZoneTypeId = 0 });

            lst.Add(new tblGeofence_Detail { ipkGeoDID = 102316, ifkGeoMID = 80068, vLatitude = 0.630801424519967, vLongitude = 38.8606302504502, iGeoZoneTypeId = 0 });
            lst.Add(new tblGeofence_Detail { ipkGeoDID = 102317, ifkGeoMID = 80068, vLatitude = 0.603337197346792, vLongitude = 38.914188600052, iGeoZoneTypeId = 0 });
            lst.Add(new tblGeofence_Detail { ipkGeoDID = 102318, ifkGeoMID = 80068, vLatitude = 0.566260271923467, vLongitude = 38.8359110121722, iGeoZoneTypeId = 0 });
            lst.Add(new tblGeofence_Detail { ipkGeoDID = 102319, ifkGeoMID = 80068, vLatitude = 0.566260271923467, vLongitude = 38.7411539321072, iGeoZoneTypeId = 0 });
            lst.Add(new tblGeofence_Detail { ipkGeoDID = 102320, ifkGeoMID = 80068, vLatitude = 0.566260271923467, vLongitude = 38.7411539321072, iGeoZoneTypeId = 0 });


            return lst;
        }
    }
}
