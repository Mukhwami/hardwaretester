﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HardwareTester
{
    class cQL_GV300
    {
        private Hashtable hT;
        private const string queue_path = @".\private$\UTS_MQ_IN";
        //device data fields
        private string protocol_ver;
        private string unique_id;
        private string device_name;
        private int gps_accuracy;
        private double speed;
        private int azimuth;
        private double alt;
        private double lng;
        private double lat;
        private string gps_utc_time;
        private string MCC;
        private string MNC;
        private string LAC;
        private string cellID;
        private double odometer;
        private string send_time;
        private string count_num;
        private string tail_char;

        //analogs
        string ign_time_cnt;
        string analog_inpt_1;
        string analog_input_2;
        string bkp_batt;
        string devc_status;


        //reserverd fields
        private string reserved_0;
        private string reserved_1;

        public cQL_GV300()
        {
            //initialize device event mapping: 

            hT = new Hashtable();
            //position related report
            hT.Add("GTTOW", "1"); //towing
            hT.Add("GTDIS", "1"); //digital input status
            hT.Add("GTIOB", "1"); //IO combination changes
            hT.Add("GTGEO", "1"); //geofence, if enabled
            hT.Add("GTSPD", "1"); //overspeed 
            hT.Add("GTSOS", "1"); //set SOS mode to 1
            hT.Add("GTHBM", "1"); // harsh behaviour
            hT.Add("GTIGL", "1");

            //fixed report
            hT.Add("GTFRI", "2"); // fixed report

            hT.Add("GTEPS", "12"); // External power supply
            hT.Add("GTAIS", "12");

            hT.Add("GTPNA", "3"); //power on report
            hT.Add("GTPFA", "3"); //power off report
            hT.Add("GTPDP", "3"); //main power supply connected

            hT.Add("GTMPN", "4"); //MAIN POWER CONNECTED
            hT.Add("GTMPF", "4"); //main power supply reconnected
            hT.Add("GTBTC", "4"); //back up battery started charging
            hT.Add("GTBPN", "4"); //backup battery connected
            hT.Add("GTBPF", "4"); //back batt removed
            hT.Add("GTSOA", "4"); //shell opened
            hT.Add("GTJDR", "4"); //gsm jamming

            hT.Add("GTSTC", "5"); //backup batt stopped charging

            hT.Add("GTBPL", "6"); //backup battery low
            hT.Add("GTSTT", "7"); //device motion state

            //gps state
            hT.Add("GTANT", "8");

            //ignition
            hT.Add("GTIGN", "9"); //on
            hT.Add("GTIGF", "10"); //off

            hT.Add("GTIDN", "11");
            hT.Add("GTIDF", "11");

            //driver id options
            hT.Add("GTERI", "13");
            hT.Add("GTIDA", "14");

            //device information
            hT.Add("GTINF", "15");

            //stuff I will handle later
            hT.Add("GTRTL", "16");
            hT.Add("GTDOG", "17");
            hT.Add("GTCAN", "18");
            hT.Add("GTJDS", "19");

        }
     

        public void getMsgType(string str_report)
        {
            var strs = str_report.Split(',');
            try
            {
                if (hT[strs[0].Substring(6, 5)].ToString() == "1")
                {
                    //position related report
                    make_pos_report(str_report); //1
                }
                if (hT[strs[0].Substring(6, 5)].ToString() == "2")
                {
                    //fixed report
                    make_fixed_report(str_report);
                }
                if (hT[strs[0].Substring(6, 5)].ToString() == "3")
                {
                    make_evt_rept_a(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "4")
                {
                    make_evt_rept_b(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "5")
                {
                    make_evt_rept_c(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "6")
                {
                    make_battlow_rpt(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "7")
                {
                    make_devmotion_rpt(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "8")
                {
                    make_gps_rpt_f(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "9")
                {
                    make_ignon_rpt(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "10")
                {
                    make_ignoff_rpt(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "11")
                {
                    make_idle_rpt(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "12")
                {
                    make_eps_rpt(str_report);
                }
                if (hT[strs[0].Substring(6, 5)].ToString() == "13")
                {
                    if (strs[1].Substring(0, 2) == "4B")
                        make_GTERI_CAN(str_report);
                    else
                        make_GTERI(str_report);
                }
                if (hT[strs[0].Substring(6, 5)].ToString() == "14")
                {
                    makeGTIDA(str_report);
                }
                if (hT[strs[0].Substring(6, 5)].ToString() == "15")
                {

                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "16")
                {

                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "17")
                {

                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "18")
                {
                    //CAN messages
                    makeCANMsg(str_report);
                }

                if (hT[strs[0].Substring(6, 5)].ToString() == "19")
                {

                }
                else
                {
                    //write the new report to error file

                }
            }
            catch (Exception e)
            {
                //FileIO fio = new FileIO();
                //fio.WriteDayLog(e.Message + " : cQL_GV300.getMsgType(): " + str_report);
            }
        }

        private void make_pos_report(string _strRpt) //GTDIS, GTTOW, GTIOB, GTGEO, GTSPD, GTSOS, GTHBM
        {
            //1
            var digital_inpt_id = "";
            var digital_inpt_stat = "";
            var speed_level = "";
            var speed_stat = "0";
            var hb_ = "";
            string rept_type;
            var engine_stat = "0";

            var s_info = _strRpt.Split(',');
            rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            reserved_0 = s_info[4];
            if (s_info[0] == "+RESP:GTDIS" || s_info[0] == "+BUFF:GTDIS")
            {
                digital_inpt_id = s_info[5].Substring(0, 1);
                digital_inpt_stat = s_info[5].Substring(1, 1);
            }
            if (s_info[0] == "+RESP:GTSOS" || s_info[0] == "+BUFF:GTSOS")
            {
                digital_inpt_id = s_info[5].Substring(0, 1);
                digital_inpt_stat = s_info[5].Substring(1, 1);
            }
            if (s_info[0] == "+RESP:GTHBM" || s_info[0] == "+BUFF:GTHBM")
            {
                speed_level = s_info[5].Substring(0, 1);
                hb_ = s_info[5].Substring(1, 1);
            }

            if (s_info[0] == "+RESP:GTSPD" || s_info[0] == "+BUFF:GTSPD")
            {
                speed_stat = s_info[5].Substring(1, 1);
            }

            if (s_info[0] == "+RESP:GTIGL" || s_info[0] == "+BUFF:GTIGL")
            {
                engine_stat = s_info[5].Substring(1, 1);
            }


            var num_gps_pos = s_info[6];
            gps_accuracy = (s_info[7] == "") ? 0 : int.Parse(s_info[7]);
            speed = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            azimuth = (s_info[9] == "") ? 0 : int.Parse(s_info[9]);
            alt = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            lng = (s_info[11] == "") ? 0.0 : double.Parse(s_info[11]);
            lat = (s_info[12] == "") ? 0.0 : double.Parse(s_info[12]);
            gps_utc_time = s_info[13];
            MCC = s_info[14];
            MNC = s_info[15];
            LAC = s_info[16];
            cellID = s_info[17];
            reserved_1 = s_info[18];
            odometer = (s_info[19] == "") ? 0.0 : double.Parse(s_info[19]);
            send_time = s_info[20];
            count_num = s_info[21].Substring(0, 4);
            tail_char = s_info[21].Substring(4, 1);

            //send to streaminsight
            //cAlert alert_c = new cAlert();
            //alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3, "", hb_);

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            //handle reports
            if (s_info[0] == "+RESP:GTDIS" || s_info[0] == "+BUFF:GTDIS")
            {
                var xdig_id = doc.CreateElement("digital_input_id");
                xdig_id.InnerText = digital_inpt_id;
                root.AppendChild(xdig_id);

                var xdig_stat = doc.CreateElement("digital_input_stat");
                xdig_stat.InnerText = digital_inpt_stat;
                root.AppendChild(xdig_stat);
            }

            if (s_info[0] == "+RESP:GTSOS" || s_info[0] == "+BUFF:GTSOS")
            {
                var xdig_id = doc.CreateElement("digital_input_id");
                xdig_id.InnerText = digital_inpt_id;
                root.AppendChild(xdig_id);

                var xdig_stat = doc.CreateElement("digital_input_stat");
                xdig_stat.InnerText = digital_inpt_stat;
                root.AppendChild(xdig_stat);
            }

            if (s_info[0] == "+RESP:GTSPD" || s_info[0] == "+BUFF:GTSPD")
            {
                var xspeed_stat = doc.CreateElement("speed_status");
                xspeed_stat.InnerText = speed_stat;
                root.AppendChild(xspeed_stat);
            }

            if (s_info[0] == "+RESP:GTIGL" || s_info[0] == "+BUFF:GTIGL")
            {
                var xengine_stat = doc.CreateElement("engine_status");
                xengine_stat.InnerText = engine_stat;
                root.AppendChild(xengine_stat);
            }

            if (s_info[0] == "+RESP:GTHBM" || s_info[0] == "+BUFF:GTHBM")
            {
                var xspeed_level = doc.CreateElement("speed_level");
                xspeed_level.InnerText = speed_level;
                root.AppendChild(xspeed_level);

                var xhb_message = doc.CreateElement("hb_type");
                xhb_message.InnerText = hb_;
                root.AppendChild(xhb_message);
            }

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            var xMileage = doc.CreateElement("odometer");
            xMileage.InnerText = odometer.ToString();
            root.AppendChild(xMileage);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //road speed
            var iMaxSpeed = 0;

            //text position
            //cCommon cc = new cCommon();
            //string txtpos = cc.makePostext(street_search_svc, poi_svc, nearby_svc, geo_uname, lat.ToString(), lng.ToString(), out iMaxSpeed);
            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = string.Empty;
            root.AppendChild(xTextPos);

            var xRoadSpeed = doc.CreateElement("max_road_speed");
            xRoadSpeed.InnerText = iMaxSpeed.ToString();
            root.AppendChild(xRoadSpeed);

            //add alert options
            //cAlert c = new cAlert();
            //string sOptions = c.getAlertStatPriority(unique_id, rept_type, "QL");
            //string[] arrOpts = sOptions.Split('-');

            //XmlElement xIsAlert = doc.CreateElement("IsAlert");
            //xIsAlert.InnerText = arrOpts[0];
            //xIsAlert.InnerText = xIsAlert.InnerText.TrimEnd(',');
            //if (xIsAlert.InnerText == "") xIsAlert.InnerText = "0";
            //root.AppendChild(xIsAlert);

            //XmlElement xPriority = doc.CreateElement("Priority");
            //xPriority.InnerText = arrOpts[1];
            //root.AppendChild(xPriority);

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

           
        }

        private void make_fixed_report(string _strRpt) //GTFRI
        {
            try
            {
                var report_id = "";
                var s_info = _strRpt.Split(',');
                var rept_type = s_info[0];
                protocol_ver = s_info[1];
                unique_id = s_info[2];
                device_name = s_info[3];
                var main_voltage = s_info[4];

                switch (int.Parse(s_info[5].Substring(0, 1)))
                {
                    case 1:
                        report_id = "fixed timing report";
                        break;
                    case 2:
                        report_id = "fixed distance report";
                        break;
                    case 3:
                        report_id = "fixed mileage report";
                        break;
                    case 4:
                        report_id = "fixed mileage and timing report";
                        break;
                    default:
                        report_id = "no report defined";
                        break;
                }

                var num_gps_pos = int.Parse(s_info[6]);
                var x = 7;
                var n = (num_gps_pos * 12) + x;

                for (var i = 0; i < num_gps_pos; i++)
                {
                    gps_accuracy = (s_info[x] == "") ? 0 : int.Parse(s_info[x]);
                    x++;
                    speed = (s_info[x] == "") ? 0.0 : double.Parse(s_info[x]);
                    x++;
                    azimuth = (s_info[x] == "") ? 0 : int.Parse(s_info[x]);
                    x++;
                    alt = (s_info[x] == "") ? 0.0 : double.Parse(s_info[x]);
                    x++;
                    lng = (s_info[x] == "") ? 0.0 : double.Parse(s_info[x]);
                    x++;
                    lat = (s_info[x] == "") ? 0.0 : double.Parse(s_info[x]);
                    x++;
                    gps_utc_time = s_info[x];
                    MCC = s_info[x];
                    x++;
                    MNC = s_info[x];
                    x++;
                    LAC = s_info[x];
                    x++;
                    cellID = s_info[x];
                    x++;
                    reserved_1 = s_info[x];
                    x++;

                    odometer = (s_info[n] == "") ? 0.0 : double.Parse(s_info[n]);
                    n++;
                    ign_time_cnt = s_info[n];
                    n++;
                    analog_inpt_1 = s_info[n];
                    n++;
                    analog_input_2 = s_info[n];
                    n++;
                    bkp_batt = s_info[n];
                    n++;
                    devc_status = s_info[n];
                    n += 4;
                    send_time = s_info[n];
                    n++;
                    count_num = s_info[n].Substring(0, 4);
                    tail_char = s_info[n].Substring(4, 1);
                    n = n - 2;

                    //send to streaminsight
                    //cAlert alert_c = new cAlert();
                    //Thread thrd = new Thread(() => alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3));
                    //thrd.Start();

                    //make the xml and send
                    var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
                    var dec = doc.CreateXmlDeclaration("1.0", null, null);
                    doc.AppendChild(dec);

                    // Create the root element
                    var root = doc.CreateElement("QL_Message_GV300");
                    doc.AppendChild(root);

                    var xrept_type = doc.CreateElement("rept_type");
                    xrept_type.InnerText = rept_type;
                    root.AppendChild(xrept_type);

                    var xprotocol_ver = doc.CreateElement("protocol_ver");
                    xprotocol_ver.InnerText = protocol_ver;
                    root.AppendChild(xprotocol_ver);

                    var xdevice_id = doc.CreateElement("device_id");
                    xdevice_id.InnerText = unique_id;
                    root.AppendChild(xdevice_id);

                    var xdevice_name = doc.CreateElement("device_name");
                    xdevice_name.InnerText = device_name;
                    root.AppendChild(xdevice_name);

                    //external power supply
                    var xmain_supply_vcc = doc.CreateElement("main_power_vcc");
                    xmain_supply_vcc.InnerText = main_voltage;
                    root.AppendChild(xmain_supply_vcc);

                    //handle reports
                    var x_report_id = doc.CreateElement("report_id");
                    x_report_id.InnerText = report_id;
                    root.AppendChild(x_report_id);

                    var x_report_exp = doc.CreateElement("report_exp");
                    x_report_exp.InnerText = s_info[5].Substring(1, 1);
                    root.AppendChild(x_report_exp);

                    var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
                    xgps_accuracy.InnerText = gps_accuracy.ToString();
                    root.AppendChild(xgps_accuracy);

                    var xspeed = doc.CreateElement("speed");
                    xspeed.InnerText = speed.ToString();
                    root.AppendChild(xspeed);

                    var xazimuth = doc.CreateElement("azimuth");
                    xazimuth.InnerText = azimuth.ToString();
                    root.AppendChild(xazimuth);

                    var xlatitude = doc.CreateElement("latitude");
                    xlatitude.InnerText = lat.ToString();
                    root.AppendChild(xlatitude);

                    var xlongitude = doc.CreateElement("longitude");
                    xlongitude.InnerText = lng.ToString();
                    root.AppendChild(xlongitude);

                    var xaltitude = doc.CreateElement("altitude");
                    xaltitude.InnerText = alt.ToString();
                    root.AppendChild(xaltitude);

                    var xgps_time = doc.CreateElement("gps_utc_time");
                    xgps_time.InnerText = gps_utc_time;
                    root.AppendChild(xgps_time);

                    var xMCC = doc.CreateElement("MCC");
                    xMCC.InnerText = MCC;
                    root.AppendChild(xMCC);

                    var xMNC = doc.CreateElement("MNC");
                    xMNC.InnerText = MNC;
                    root.AppendChild(xMNC);

                    var xLAC = doc.CreateElement("LAC");
                    xLAC.InnerText = LAC;
                    root.AppendChild(xLAC);

                    var xCellID = doc.CreateElement("cell_id");
                    xCellID.InnerText = cellID;
                    root.AppendChild(xCellID);

                    var xMileage = doc.CreateElement("odometer");
                    xMileage.InnerText = odometer.ToString();
                    root.AppendChild(xMileage);

                    //ignition time count
                    var xIgn_count = doc.CreateElement("ign_time_count");
                    xIgn_count.InnerText = ign_time_cnt;
                    root.AppendChild(xIgn_count);

                    //analog input 1
                    var xanalog_inpt = doc.CreateElement("analog_input_1");
                    xanalog_inpt.InnerText = analog_inpt_1;
                    root.AppendChild(xanalog_inpt);

                    //analog input 2
                    var xanalog_inpt2 = doc.CreateElement("analog_input_2");
                    xanalog_inpt2.InnerText = analog_input_2;
                    root.AppendChild(xanalog_inpt2);

                    //back up battery %
                    var xbkp_batt = doc.CreateElement("backup_batt");
                    xbkp_batt.InnerText = bkp_batt;
                    root.AppendChild(xbkp_batt);

                    //device status
                    var xdev_status = doc.CreateElement("device_status");
                    xdev_status.InnerText = devc_status;
                    root.AppendChild(xdev_status);

                    var xSendTime = doc.CreateElement("send_time");
                    xSendTime.InnerText = send_time;
                    root.AppendChild(xSendTime);

                    var xCount = doc.CreateElement("count");
                    xCount.InnerText = count_num;
                    root.AppendChild(xCount);

                    var xTailChar = doc.CreateElement("tailchar");
                    xTailChar.InnerText = tail_char;
                    root.AppendChild(xTailChar);

                    var iMax_speed = 0;


                    //text position
                    var txtpos = string.Empty;


                    var xTextPos = doc.CreateElement("TextPos");
                    xTextPos.InnerText = txtpos.TrimStart(',');
                    root.AppendChild(xTextPos);

                   

                    var xRawData = doc.CreateElement("raw_data");
                    xRawData.InnerText = _strRpt;
                    root.AppendChild(xRawData);



                    var xmlOutput = doc.OuterXml;

                    

                }


                //alert_c.process_geoAlerts(unique_id, convertstrDate(gps_utc_time), lat, lng, (int)speed, txtpos, 0, "rept_type", doc, "DEVICE_MSG_QL", _lastcoord);
            }
            catch (Exception e)
            {
                //FileIO fio = new FileIO();
                //fio.WriteDayLog(e.Message + " : cQL_GV300.make_fixed_report(): " + _strRpt);
            }

        }

        private void make_evt_rept_a(string _strRpt)  //GTPNA, GTPFA, GTPDP
        {
            //GTPNA, GTPFA, GTPDP
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            send_time = s_info[4];
            count_num = s_info[5].Substring(0, 4);
            tail_char = s_info[5].Substring(4, 1);


            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //write to log
            var xmlOutput = doc.OuterXml;

          
        }

        private void make_evt_rept_b(string _strRpt)
        {
            //GTMPN, GTMPF, GTBTC, GTBPN, GTBPF, GTSOA, GTJDR
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            gps_accuracy = (s_info[4] == "") ? 0 : int.Parse(s_info[4]);
            speed = (s_info[5] == "") ? 0.0 : double.Parse(s_info[5]);
            azimuth = (s_info[6] == "") ? 0 : int.Parse(s_info[6]);
            alt = (s_info[7] == "") ? 0.0 : double.Parse(s_info[7]);
            lng = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            lat = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            gps_utc_time = s_info[10];
            MCC = s_info[11];
            MNC = s_info[12];
            LAC = s_info[13];
            cellID = s_info[14];
            reserved_1 = s_info[15];
            send_time = s_info[16];
            count_num = s_info[17].Substring(0, 4);
            tail_char = s_info[17].Substring(4, 1);

            //send to streaminsight
            //cAlert alert_c = new cAlert();
            //alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3);

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;



            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

            //add alert options
            //cAlert c = new cAlert();
            //string sOptions = c.getAlertStatPriority(unique_id, rept_type, "QL");
            //string[] arrOpts = sOptions.Split('-');

            //XmlElement xIsAlert = doc.CreateElement("IsAlert");
            //xIsAlert.InnerText = arrOpts[0];
            //xIsAlert.InnerText = xIsAlert.InnerText.TrimEnd(',');
            //if (xIsAlert.InnerText == "") xIsAlert.InnerText = "0";
            //root.AppendChild(xIsAlert);

            //XmlElement xPriority = doc.CreateElement("Priority");
            //xPriority.InnerText = arrOpts[1];
            //root.AppendChild(xPriority);

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

           


        } //GTMPN, GTMPF, GTBTC, GTBPN, GTBPF, GTSOA, GTJDR

        private void make_evt_rept_c(string _strRpt) //GTSTC
        {
            //GTSTC

            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            gps_accuracy = (s_info[5] == "") ? 0 : int.Parse(s_info[5]);
            speed = (s_info[6] == "") ? 0.0 : double.Parse(s_info[6]);
            azimuth = (s_info[7] == "") ? 0 : int.Parse(s_info[7]);
            alt = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            lng = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            lat = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            gps_utc_time = s_info[11];
            MCC = s_info[12];
            MNC = s_info[13];
            LAC = s_info[14];
            cellID = s_info[15];
            reserved_1 = s_info[16];
            send_time = s_info[17];
            count_num = s_info[18].Substring(0, 4);
            tail_char = s_info[18].Substring(4, 1);

            //send to streaminsight
            //cAlert alert_c = new cAlert();
            //alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3);

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;


            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

            //add alert options
            //cAlert c = new cAlert();
            //string sOptions = c.getAlertStatPriority(unique_id, rept_type, "QL");
            //string[] arrOpts = sOptions.Split('-');

            //XmlElement xIsAlert = doc.CreateElement("IsAlert");
            //xIsAlert.InnerText = arrOpts[0];
            //xIsAlert.InnerText = xIsAlert.InnerText.TrimEnd(',');
            //if (xIsAlert.InnerText == "") xIsAlert.InnerText = "0";
            //root.AppendChild(xIsAlert);

            //XmlElement xPriority = doc.CreateElement("Priority");
            //xPriority.InnerText = arrOpts[1];
            //root.AppendChild(xPriority);

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

            

        }

        private void make_battlow_rpt(string _strRpt) //GTBPL -- main battery low
        {
            //GTBPL -- main battery low

            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            var battery_vcc = s_info[4];
            gps_accuracy = (s_info[5] == "") ? 0 : int.Parse(s_info[5]);
            speed = (s_info[6] == "") ? 0.0 : double.Parse(s_info[6]);
            azimuth = (s_info[7] == "") ? 0 : int.Parse(s_info[7]);
            alt = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            lng = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            lat = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            gps_utc_time = s_info[11];
            MCC = s_info[12];
            MNC = s_info[13];
            LAC = s_info[14];
            cellID = s_info[15];
            reserved_1 = s_info[16];
            send_time = s_info[17];
            count_num = s_info[18].Substring(0, 4);
            tail_char = s_info[18].Substring(4, 1);

            //send to streaminsight
            //cAlert alert_c = new cAlert();
            //alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3);

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            var xbatt_vcc = doc.CreateElement("batt_vcc");
            xbatt_vcc.InnerText = battery_vcc;
            root.AppendChild(xbatt_vcc);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;


            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

            //add alert options
            //cAlert c = new cAlert();
            //string sOptions = c.getAlertStatPriority(unique_id, rept_type, "QL");
            //string[] arrOpts = sOptions.Split('-');

            //XmlElement xIsAlert = doc.CreateElement("IsAlert");
            //xIsAlert.InnerText = arrOpts[0];
            //xIsAlert.InnerText = xIsAlert.InnerText.TrimEnd(',');
            //if (xIsAlert.InnerText == "") xIsAlert.InnerText = "0";
            //root.AppendChild(xIsAlert);

            //XmlElement xPriority = doc.CreateElement("Priority");
            //xPriority.InnerText = arrOpts[1];
            //root.AppendChild(xPriority);

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

            
        }

        private void make_devmotion_rpt(string _strRpt) // //GTSTT -- device motion state indication
        {
            //GTSTT -- device motion state indication
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            var motion_state = s_info[4];
            gps_accuracy = (s_info[5] == "") ? 0 : int.Parse(s_info[5]);
            speed = (s_info[6] == "") ? 0.0 : double.Parse(s_info[6]);
            azimuth = (s_info[7] == "") ? 0 : int.Parse(s_info[7]);
            alt = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            lng = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            lat = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            gps_utc_time = s_info[11];
            MCC = s_info[12];
            MNC = s_info[13];
            LAC = s_info[14];
            cellID = s_info[15];
            reserved_1 = s_info[16];
            send_time = s_info[17];
            count_num = s_info[18].Substring(0, 4);
            tail_char = s_info[18].Substring(4, 1);

            //send to streaminsight
            //cAlert alert_c = new cAlert();
            //alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3);

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            var xmotion_state = doc.CreateElement("motion_state");
            xmotion_state.InnerText = motion_state;
            root.AppendChild(xmotion_state);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;


            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

           

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

        }

        private void make_gps_rpt_f(string _strRpt) //GTANT - current state of GPS Antenna
        {
            //GTANT - current state of GPS Antenna
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            var gps_state = s_info[4];
            gps_accuracy = (s_info[5] == "") ? 0 : int.Parse(s_info[5]);
            speed = (s_info[6] == "") ? 0.0 : double.Parse(s_info[6]);
            azimuth = (s_info[7] == "") ? 0 : int.Parse(s_info[7]);
            alt = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            lng = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            lat = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            gps_utc_time = s_info[11];
            MCC = s_info[12];
            MNC = s_info[13];
            LAC = s_info[14];
            cellID = s_info[15];
            reserved_1 = s_info[16];
            send_time = s_info[17];
            count_num = s_info[18].Substring(0, 4);
            tail_char = s_info[18].Substring(4, 1);

            //send to streaminsight
            //cAlert alert_c = new cAlert();
            //alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3);

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            var xgps_state = doc.CreateElement("gps_state");
            xgps_state.InnerText = gps_state;
            root.AppendChild(xgps_state);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;



            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

           

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);



            var xmlOutput = doc.OuterXml;

           
        }

        private void make_ignon_rpt(string _strRpt) //GTIGN - Ignition on report
        {
            //GTIGN - Ignition on report
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            var ign_dur = s_info[4];
            gps_accuracy = (s_info[5] == "") ? 0 : int.Parse(s_info[5]);
            speed = (s_info[6] == "") ? 0.0 : double.Parse(s_info[6]);
            azimuth = (s_info[7] == "") ? 0 : int.Parse(s_info[7]);
            alt = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            lng = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            lat = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            gps_utc_time = s_info[11];
            MCC = s_info[12];
            MNC = s_info[13];
            LAC = s_info[14];
            cellID = s_info[15];
            reserved_1 = s_info[16];
            var ign_time_count = s_info[17];
            odometer = (s_info[18] == "") ? 0.0 : double.Parse(s_info[18]);

            send_time = s_info[19];
            count_num = s_info[20].Substring(0, 4);
            tail_char = s_info[20].Substring(4, 1);

            

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            var xign_dur = doc.CreateElement("ign_off_duration");
            xign_dur.InnerText = ign_dur;
            root.AppendChild(xign_dur);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            //odometer
            var xMileage = doc.CreateElement("odometer");
            xMileage.InnerText = odometer.ToString();
            root.AppendChild(xMileage);

            //ignition time count
            var xign_count = doc.CreateElement("ign_count");
            xign_count.InnerText = ign_time_count;
            root.AppendChild(xign_count);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;


            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

            

           



            var xmlOutput = doc.OuterXml;

            
        }

        private void make_ignoff_rpt(string _strRpt) //GTIGF - Ignition off report
        {
            //GTIGF - Ignition off report
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            var ign_dur = s_info[4];
            gps_accuracy = (s_info[5] == "") ? 0 : int.Parse(s_info[5]);
            speed = (s_info[6] == "") ? 0.0 : double.Parse(s_info[6]);
            azimuth = (s_info[7] == "") ? 0 : int.Parse(s_info[7]);
            alt = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            lng = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            lat = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            gps_utc_time = s_info[11];
            MCC = s_info[12];
            MNC = s_info[13];
            LAC = s_info[14];
            cellID = s_info[15];
            reserved_1 = s_info[16];
            var ign_time_count = s_info[17];
            odometer = (s_info[18] == "") ? 0.0 : double.Parse(s_info[18]);
            send_time = s_info[19];
            count_num = s_info[20].Substring(0, 4);
            tail_char = s_info[20].Substring(4, 1);

           


            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            var xign_dur = doc.CreateElement("ign_on_duration");
            xign_dur.InnerText = ign_dur;
            root.AppendChild(xign_dur);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            //odometer
            var xMileage = doc.CreateElement("odometer");
            xMileage.InnerText = odometer.ToString();
            root.AppendChild(xMileage);

            //ignition time count
            var xign_count = doc.CreateElement("ign_count");
            xign_count.InnerText = ign_time_count;
            root.AppendChild(xign_count);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;



            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

            //add alert options
            //cAlert c = new cAlert();
            //string sOptions = c.getAlertStatPriority(unique_id, rept_type, "QL");
            //string[] arrOpts = sOptions.Split('-');

            //XmlElement xIsAlert = doc.CreateElement("IsAlert");
            //xIsAlert.InnerText = arrOpts[0];
            //xIsAlert.InnerText = xIsAlert.InnerText.TrimEnd(',');
            //if (xIsAlert.InnerText == "") xIsAlert.InnerText = "0";
            //root.AppendChild(xIsAlert);

            //XmlElement xPriority = doc.CreateElement("Priority");
            //xPriority.InnerText = arrOpts[1];
            //root.AppendChild(xPriority);

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


        }

        private void make_idle_rpt(string _strRpt) //GTIDN, GTIDF
        {
            //GTIDN, GTIDF
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            var motion_state = s_info[4];
            var dur_idle = s_info[5];
            gps_accuracy = (s_info[6] == "") ? 0 : int.Parse(s_info[6]);
            speed = (s_info[7] == "") ? 0.0 : double.Parse(s_info[7]);
            azimuth = (s_info[8] == "") ? 0 : int.Parse(s_info[8]);
            alt = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            lng = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            lat = (s_info[11] == "") ? 0.0 : double.Parse(s_info[11]);
            gps_utc_time = s_info[12];
            MCC = s_info[13];
            MNC = s_info[14];
            LAC = s_info[15];
            cellID = s_info[16];
            reserved_1 = s_info[16];
            odometer = (s_info[18] == "") ? 0.0 : double.Parse(s_info[18]);
            send_time = s_info[19];
            count_num = s_info[20].Substring(0, 4);
            tail_char = s_info[20].Substring(4, 1);

            //send to streaminsight
            //cAlert alert_c = new cAlert();
            //alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3);

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            if (s_info[0] == "+RESP:GTIDF" || s_info[0] == "+BUFF:GTIDF")
            {
                //motion state of the device
                var xmotion_state = doc.CreateElement("motion_state");
                xmotion_state.InnerText = motion_state;
                root.AppendChild(xmotion_state);

                //duration of idling status
                var xdur_idle = doc.CreateElement("idling_duration");
                xdur_idle.InnerText = dur_idle;
                root.AppendChild(xdur_idle);

            }
            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            //odometer
            var xMileage = doc.CreateElement("odometer");
            xMileage.InnerText = odometer.ToString();
            root.AppendChild(xMileage);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);
            //text position
            var txtpos = string.Empty;



            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

            //add alert options
            //cAlert c = new cAlert();
            //string sOptions = c.getAlertStatPriority(unique_id, rept_type, "QL");
            //string[] arrOpts = sOptions.Split('-');

            //XmlElement xIsAlert = doc.CreateElement("IsAlert");
            //xIsAlert.InnerText = arrOpts[0];
            //xIsAlert.InnerText = xIsAlert.InnerText.TrimEnd(',');
            //if (xIsAlert.InnerText == "") xIsAlert.InnerText = "0";
            //root.AppendChild(xIsAlert);

            //XmlElement xPriority = doc.CreateElement("Priority");
            //xPriority.InnerText = arrOpts[1];
            //root.AppendChild(xPriority);

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

        }

        private void make_eps_rpt(string _strRpt) //GTEPS, GTAIS
        {
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            var analog_input_vcc = s_info[4];
            gps_accuracy = (s_info[7] == "") ? 0 : int.Parse(s_info[7]);
            speed = (s_info[8] == "") ? 0.0 : double.Parse(s_info[8]);
            azimuth = (s_info[9] == "") ? 0 : int.Parse(s_info[9]);
            alt = (s_info[10] == "") ? 0.0 : double.Parse(s_info[10]);
            lng = (s_info[11] == "") ? 0.0 : double.Parse(s_info[11]);
            lat = (s_info[12] == "") ? 0.0 : double.Parse(s_info[12]);
            gps_utc_time = s_info[13];
            MCC = s_info[14];
            MNC = s_info[15];
            LAC = s_info[16];
            cellID = s_info[17];
            reserved_1 = s_info[18];
            odometer = (s_info[19] == "") ? 0.0 : double.Parse(s_info[19]);
            send_time = s_info[20];
            count_num = s_info[21].Substring(0, 4);
            tail_char = s_info[21].Substring(4, 1);

            //send to streaminsight
            //cAlert alert_c = new cAlert();
            //alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3);

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            //analog input vcc
            var xanalog_inpt_vcc = doc.CreateElement("analog_input_vcc");
            xanalog_inpt_vcc.InnerText = analog_input_vcc;
            root.AppendChild(xanalog_inpt_vcc);

            //analog id that triggered report 
            var xanalog_id = doc.CreateElement("analog_input_id");
            xanalog_id.InnerText = s_info[5].Substring(0, 1);
            root.AppendChild(xanalog_id);
            //type of range
            var xvoltage_range = doc.CreateElement("voltage_range");
            xvoltage_range.InnerText = s_info[5].Substring(1, 1); ;
            root.AppendChild(xvoltage_range);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            //odometer
            var xMileage = doc.CreateElement("odometer");
            xMileage.InnerText = odometer.ToString();
            root.AppendChild(xMileage);

            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;



            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

            //add alert options
            //cAlert c = new cAlert();
            //string sOptions = c.getAlertStatPriority(unique_id, rept_type, "QL");
            //string[] arrOpts = sOptions.Split('-');

            //XmlElement xIsAlert = doc.CreateElement("IsAlert");
            //xIsAlert.InnerText = arrOpts[0];
            //xIsAlert.InnerText = xIsAlert.InnerText.TrimEnd(',');
            //if (xIsAlert.InnerText == "") xIsAlert.InnerText = "0";
            //root.AppendChild(xIsAlert);

            //XmlElement xPriority = doc.CreateElement("Priority");
            //xPriority.InnerText = arrOpts[1];
            //root.AppendChild(xPriority);

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

           
        }

        //GTERI - UART is being used
        private void make_GTERI(string _strRpt)
        {

            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);
            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);
            try
            {

                var i = 27;
                var fuel_sensor_data = "";
                var ac_devices = new StringBuilder("");
                var report_id = "";
                var s_info = _strRpt.Split(',');
                var rept_type = s_info[0];
                protocol_ver = s_info[1];
                unique_id = s_info[2];
                device_name = s_info[3];
                var eri_mask = s_info[4];
                var main_voltage = s_info[5];

                switch (int.Parse(s_info[6].Substring(0, 1)))
                {
                    case 1:
                        report_id = "fixed timing report";
                        break;
                    case 2:
                        report_id = "fixed distance report";
                        break;
                    case 3:
                        report_id = "fixed mileage report";
                        break;
                    case 4:
                        report_id = "fixed mileage and timing report";
                        break;
                    case 5:
                        report_id = "fixed mileage or timing report";
                        break;
                    default:
                        report_id = "no report defined";
                        break;
                }

                var num_gps_pos = s_info[7];
                gps_accuracy = (s_info[8] == "") ? 0 : int.Parse(s_info[8]);
                speed = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
                azimuth = (s_info[10] == "") ? 0 : int.Parse(s_info[10]);
                alt = (s_info[11] == "") ? 0.0 : double.Parse(s_info[11]);
                lng = (s_info[12] == "") ? 0.0 : double.Parse(s_info[12]);
                lat = (s_info[13] == "") ? 0.0 : double.Parse(s_info[13]);
                gps_utc_time = s_info[14];
                MCC = s_info[15];
                MNC = s_info[16];
                LAC = s_info[17];
                cellID = s_info[18];
                reserved_1 = s_info[19];
                odometer = (s_info[20] == "") ? 0.0 : double.Parse(s_info[20]);
                var ign_time_cnt = s_info[21];
                var analog_inpt_1 = s_info[22];
                var analog_input_2 = s_info[23];
                var bkp_batt = s_info[24];
                var devc_status = s_info[25];
                var UART_devtype = s_info[26];

                var xACDevices = doc.CreateElement("onewiredevice");

                //handle UART connected devices
                if (s_info[26] == "1")
                {
                    i = 27;
                    fuel_sensor_data = s_info[27]==""? "0":s_info[27];
                    var fuel_level = Convert.ToInt32(fuel_sensor_data, 16);
                    var dfuel_level = Convert.ToDouble(fuel_level) / 100;

                    var xf_sensor = doc.CreateElement("fuel_level");
                    xf_sensor.InnerText = dfuel_level.ToString();
                    xACDevices.AppendChild(xf_sensor);

                    i += 2;

                }


                if (s_info[26] == "2")
                {
                    i = 27;
                    var num_ac_devices = (s_info[27] == "") ? 0 : int.Parse(s_info[27]);
                    for (var x = 0; x < num_ac_devices; x++)
                    {
                        i++;
                        xACDevices.SetAttribute("deviceid", s_info[i]);
                        i++;
                        xACDevices.SetAttribute("device_type", s_info[i]);
                        i++;
                        if (s_info[i - 1] == "1")
                        {
                            var decValue = 0;
                            var dblValue = 0.0;
                            //check if temp is negative
                            var bin_val = Convert.ToString(Convert.ToInt32(s_info[i].Substring(0, 2), 16), 2).PadLeft(8, '0');
                            //convert temperature reading to readable
                            if (bin_val.Substring(0, 5) == "11111") // negative temp
                            {
                                var intVal = Convert.ToInt32(s_info[i], 16);

                                decValue = 65536 - intVal;
                                dblValue = decValue * 0.0625 * -1.0;
                            }
                            else
                            {
                                if (s_info[i] != "")
                                    decValue = Convert.ToInt32(s_info[i], 16);
                                dblValue = decValue * 0.0625;
                            }
                            xACDevices.SetAttribute("device_data", dblValue.ToString() + "C");
                        }
                        else
                            xACDevices.SetAttribute("device_data", s_info[i]);

                    }

                    if (num_ac_devices == 0)
                        i += 2;

                    if (num_ac_devices == 1)
                        i++;

                }


                var send_time = s_info[i];

                count_num = s_info[s_info.Length-1].Substring(0, 4);
                tail_char = s_info[s_info.Length - 1].Substring(4, 1);

               

                //send to streaminsight
                //cAlert alert_c = new cAlert();
                //Thread thrd = new Thread(() => alert_c.Process_Alerts(unique_id, rept_type, convertstrDate(gps_utc_time), (int)speed, lat, lng, "QL", _app, 3));
                //thrd.Start();


                var xrept_type = doc.CreateElement("rept_type");
                xrept_type.InnerText = rept_type;
                root.AppendChild(xrept_type);

                var xprotocol_ver = doc.CreateElement("protocol_ver");
                xprotocol_ver.InnerText = protocol_ver;
                root.AppendChild(xprotocol_ver);

                var xdevice_id = doc.CreateElement("device_id");
                xdevice_id.InnerText = unique_id;
                root.AppendChild(xdevice_id);

                var xdevice_name = doc.CreateElement("device_name");
                xdevice_name.InnerText = device_name;
                root.AppendChild(xdevice_name);

                //external power supply
                var xmain_supply_vcc = doc.CreateElement("main_power_vcc");
                xmain_supply_vcc.InnerText = main_voltage;
                root.AppendChild(xmain_supply_vcc);

                //handle reports
                var x_report_id = doc.CreateElement("report_id");
                x_report_id.InnerText = report_id;
                root.AppendChild(x_report_id);

                var x_report_exp = doc.CreateElement("report_exp");
                x_report_exp.InnerText = s_info[6].Substring(1, 1);
                root.AppendChild(x_report_exp);

                var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
                xgps_accuracy.InnerText = gps_accuracy.ToString();
                root.AppendChild(xgps_accuracy);

                var xspeed = doc.CreateElement("speed");
                xspeed.InnerText = speed.ToString();
                root.AppendChild(xspeed);

                var xazimuth = doc.CreateElement("azimuth");
                xazimuth.InnerText = azimuth.ToString();
                root.AppendChild(xazimuth);

                var xlatitude = doc.CreateElement("latitude");
                xlatitude.InnerText = lat.ToString();
                root.AppendChild(xlatitude);

                var xlongitude = doc.CreateElement("longitude");
                xlongitude.InnerText = lng.ToString();
                root.AppendChild(xlongitude);

                var xaltitude = doc.CreateElement("altitude");
                xaltitude.InnerText = alt.ToString();
                root.AppendChild(xaltitude);

                var xgps_time = doc.CreateElement("gps_utc_time");
                xgps_time.InnerText = gps_utc_time;
                root.AppendChild(xgps_time);

                var xMCC = doc.CreateElement("MCC");
                xMCC.InnerText = MCC;
                root.AppendChild(xMCC);

                var xMNC = doc.CreateElement("MNC");
                xMNC.InnerText = MNC;
                root.AppendChild(xMNC);

                var xLAC = doc.CreateElement("LAC");
                xLAC.InnerText = LAC;
                root.AppendChild(xLAC);

                var xCellID = doc.CreateElement("cell_id");
                xCellID.InnerText = cellID;
                root.AppendChild(xCellID);

                var xMileage = doc.CreateElement("odometer");
                xMileage.InnerText = odometer.ToString();
                root.AppendChild(xMileage);

                //ignition time count
                var xIgn_count = doc.CreateElement("ign_time_count");
                xIgn_count.InnerText = ign_time_cnt;
                root.AppendChild(xIgn_count);

                //analog input 1
                var xanalog_inpt = doc.CreateElement("analog_input_1");
                xanalog_inpt.InnerText = analog_inpt_1;
                root.AppendChild(xanalog_inpt);

                //analog input 2
                var xanalog_inpt2 = doc.CreateElement("analog_input_2");
                xanalog_inpt2.InnerText = analog_input_2;
                root.AppendChild(xanalog_inpt2);

                //back up battery %
                var xbkp_batt = doc.CreateElement("backup_batt");
                xbkp_batt.InnerText = bkp_batt;
                root.AppendChild(xbkp_batt);

                //device status
                var xdev_status = doc.CreateElement("device_status");
                xdev_status.InnerText = devc_status;
                root.AppendChild(xdev_status);

                //periphy connect devices
                //device_type
                var xdev_type = doc.CreateElement("UART_device_type");
                xdev_type.InnerText = UART_devtype;
                root.AppendChild(xdev_type);

                //connected devices
                root.AppendChild(xACDevices);


                var xSendTime = doc.CreateElement("send_time");
                xSendTime.InnerText = send_time;
                root.AppendChild(xSendTime);

                var xCount = doc.CreateElement("count");
                xCount.InnerText = count_num;
                root.AppendChild(xCount);

                var xTailChar = doc.CreateElement("tailchar");
                xTailChar.InnerText = tail_char;
                root.AppendChild(xTailChar);

                var iMax_speed = 0;

                //text position
                var txtpos = string.Empty;


                var xTextPos = doc.CreateElement("TextPos");
                xTextPos.InnerText = txtpos.TrimStart(',');
                root.AppendChild(xTextPos);

                var xRoadSpeed = doc.CreateElement("max_road_speed");
                xRoadSpeed.InnerText = iMax_speed.ToString();
                root.AppendChild(xRoadSpeed);

                //road information data
                //bool bRoadInfoID = false;

                //if (speed > iMax_speed && iMax_speed > 0)
                //    bRoadInfoID = alert_c.RoadAlerts(unique_id, convertstrDate(gps_utc_time), (int)speed, iMax_speed, lat, lng, "QL", txtpos);

                ////add alert options
                //cAlert c = new cAlert();

                //XmlElement xIsAlert = doc.CreateElement("IsAlert");
                //xIsAlert.InnerText = "0";
                //root.AppendChild(xIsAlert);

                //XmlElement xPriority = doc.CreateElement("Priority");
                //xPriority.InnerText = "0";
                //root.AppendChild(xPriority);

                var xRawData = doc.CreateElement("raw_data");
                xRawData.InnerText = _strRpt;
                root.AppendChild(xRawData);


                var xmlOutput = doc.OuterXml;

              
            }
            catch (Exception e)
            {
                
            }


        }

        private void make_GTERI_CAN(string _strRpt)
        {
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);
            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);
            try
            {

                var i = 27;
                var fuel_sensor_data = "";
                var ac_devices = new StringBuilder("");
                var report_id = "";
                var s_info = _strRpt.Split(',');
                var rept_type = s_info[0];
                protocol_ver = s_info[1];
                unique_id = s_info[2];
                device_name = s_info[3];
                var eri_mask = s_info[4];
                var main_voltage = s_info[5];

                switch (int.Parse(s_info[6].Substring(0, 1)))
                {
                    case 1:
                        report_id = "fixed timing report";
                        break;
                    case 2:
                        report_id = "fixed distance report";
                        break;
                    case 3:
                        report_id = "fixed mileage report";
                        break;
                    case 4:
                        report_id = "fixed mileage and timing report";
                        break;
                    case 5:
                        report_id = "fixed mileage or timing report";
                        break;
                    default:
                        report_id = "no report defined";
                        break;
                }

                var num_gps_pos = s_info[7];
                gps_accuracy = (s_info[8] == "") ? 0 : int.Parse(s_info[8]);
                speed = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
                azimuth = (s_info[10] == "") ? 0 : int.Parse(s_info[10]);
                alt = (s_info[11] == "") ? 0.0 : double.Parse(s_info[11]);
                lng = (s_info[12] == "") ? 0.0 : double.Parse(s_info[12]);
                lat = (s_info[13] == "") ? 0.0 : double.Parse(s_info[13]);
                gps_utc_time = s_info[14];
                MCC = s_info[15];
                MNC = s_info[16];
                LAC = s_info[17];
                cellID = s_info[18];
                reserved_1 = s_info[19];
                odometer = (s_info[20] == "") ? 0.0 : double.Parse(s_info[20]);
                var ign_time_cnt = s_info[21];
                var analog_inpt_1 = s_info[22];
                var bkp_batt = s_info[23];
                var devc_status = s_info[24];
                var UART_devtype = s_info[25];

                var xACDevices = doc.CreateElement("onewiredevice");

                //handle UART connected devices
                if (s_info[25] == "1")
                {
                    i = 26;
                    fuel_sensor_data = s_info[26];
                    var fuel_level = Convert.ToInt32(fuel_sensor_data, 16);
                    var dfuel_level = Convert.ToDouble(fuel_level) / 100;

                    var xf_sensor = doc.CreateElement("fuel_level");
                    xf_sensor.InnerText = dfuel_level.ToString();
                    xACDevices.AppendChild(xf_sensor);

                    i += 2;

                }


                if (s_info[25] == "2")
                {
                    i = 26;
                    var num_ac_devices = (s_info[26] == "") ? 0 : int.Parse(s_info[27]);
                    for (var x = 0; x < num_ac_devices; x++)
                    {
                        i++;
                        xACDevices.SetAttribute("deviceid", s_info[i]);
                        i++;
                        xACDevices.SetAttribute("device_type", s_info[i]);
                        i++;
                        if (s_info[i - 1] == "1")
                        {
                            var decValue = 0;
                            var dblValue = 0.0;
                            //check if temp is negative
                            var bin_val = Convert.ToString(Convert.ToInt32(s_info[i].Substring(0, 2), 16), 2).PadLeft(8, '0');
                            //convert temperature reading to readable
                            if (bin_val.Substring(0, 5) == "11111") // negative temp
                            {
                                var intVal = Convert.ToInt32(s_info[i], 16);

                                decValue = 65536 - intVal;
                                dblValue = decValue * 0.0625 * -1.0;
                            }
                            else
                            {
                                if (s_info[i] != "")
                                    decValue = Convert.ToInt32(s_info[i], 16);
                                dblValue = decValue * 0.0625;
                            }
                            xACDevices.SetAttribute("device_data", dblValue.ToString() + "C");
                        }
                        else
                            xACDevices.SetAttribute("device_data", s_info[i]);

                    }

                    if (num_ac_devices == 0)
                        i += 2;

                    if (num_ac_devices == 1)
                        i++;

                }


                var send_time = s_info[50]==""? s_info[s_info.Length - 2]: "";
                var lastInfo = s_info[51].Length >= 5 ? s_info[51] : s_info[s_info.Length - 1];
                count_num = lastInfo.Length>=4? lastInfo.Substring(0, 4): "";
                tail_char = lastInfo.Length >= 5 ? lastInfo.Substring(4, 1) : "";

                var xrept_type = doc.CreateElement("rept_type");
                xrept_type.InnerText = rept_type;
                root.AppendChild(xrept_type);

                var xprotocol_ver = doc.CreateElement("protocol_ver");
                xprotocol_ver.InnerText = protocol_ver;
                root.AppendChild(xprotocol_ver);

                var xdevice_id = doc.CreateElement("device_id");
                xdevice_id.InnerText = unique_id;
                root.AppendChild(xdevice_id);

                var xdevice_name = doc.CreateElement("device_name");
                xdevice_name.InnerText = device_name;
                root.AppendChild(xdevice_name);

                //external power supply
                var xmain_supply_vcc = doc.CreateElement("main_power_vcc");
                xmain_supply_vcc.InnerText = main_voltage;
                root.AppendChild(xmain_supply_vcc);

                //handle reports
                var x_report_id = doc.CreateElement("report_id");
                x_report_id.InnerText = report_id;
                root.AppendChild(x_report_id);

                var x_report_exp = doc.CreateElement("report_exp");
                x_report_exp.InnerText = s_info[6].Substring(1, 1);
                root.AppendChild(x_report_exp);

                var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
                xgps_accuracy.InnerText = gps_accuracy.ToString();
                root.AppendChild(xgps_accuracy);

                var xspeed = doc.CreateElement("speed");
                xspeed.InnerText = speed.ToString();
                root.AppendChild(xspeed);

                var xazimuth = doc.CreateElement("azimuth");
                xazimuth.InnerText = azimuth.ToString();
                root.AppendChild(xazimuth);

                var xlatitude = doc.CreateElement("latitude");
                xlatitude.InnerText = lat.ToString();
                root.AppendChild(xlatitude);

                var xlongitude = doc.CreateElement("longitude");
                xlongitude.InnerText = lng.ToString();
                root.AppendChild(xlongitude);

                var xaltitude = doc.CreateElement("altitude");
                xaltitude.InnerText = alt.ToString();
                root.AppendChild(xaltitude);

                var xgps_time = doc.CreateElement("gps_utc_time");
                xgps_time.InnerText = gps_utc_time;
                root.AppendChild(xgps_time);

                var xMCC = doc.CreateElement("MCC");
                xMCC.InnerText = MCC;
                root.AppendChild(xMCC);

                var xMNC = doc.CreateElement("MNC");
                xMNC.InnerText = MNC;
                root.AppendChild(xMNC);

                var xLAC = doc.CreateElement("LAC");
                xLAC.InnerText = LAC;
                root.AppendChild(xLAC);

                var xCellID = doc.CreateElement("cell_id");
                xCellID.InnerText = cellID;
                root.AppendChild(xCellID);

                var xMileage = doc.CreateElement("odometer");
                xMileage.InnerText = odometer.ToString();
                root.AppendChild(xMileage);

                //ignition time count
                var xIgn_count = doc.CreateElement("ign_time_count");
                xIgn_count.InnerText = ign_time_cnt;
                root.AppendChild(xIgn_count);

                //analog input 1
                var xanalog_inpt = doc.CreateElement("analog_input_1");
                xanalog_inpt.InnerText = analog_inpt_1;
                root.AppendChild(xanalog_inpt);

                //analog input 2
                var xanalog_inpt2 = doc.CreateElement("analog_input_2");
                xanalog_inpt2.InnerText = analog_input_2;
                root.AppendChild(xanalog_inpt2);

                //back up battery %
                var xbkp_batt = doc.CreateElement("backup_batt");
                xbkp_batt.InnerText = bkp_batt;
                root.AppendChild(xbkp_batt);

                //device status
                var xdev_status = doc.CreateElement("device_status");
                xdev_status.InnerText = devc_status;
                root.AppendChild(xdev_status);

                //periphy connect devices
                //device_type
                var xdev_type = doc.CreateElement("UART_device_type");
                xdev_type.InnerText = UART_devtype;
                root.AppendChild(xdev_type);

                //connected devices
                root.AppendChild(xACDevices);


                var xSendTime = doc.CreateElement("send_time");
                xSendTime.InnerText = send_time;
                root.AppendChild(xSendTime);

                var xCount = doc.CreateElement("count");
                xCount.InnerText = count_num;
                root.AppendChild(xCount);

                var xTailChar = doc.CreateElement("tailchar");
                xTailChar.InnerText = tail_char;
                root.AppendChild(xTailChar);

                var iMax_speed = 0;

                //text position
                var txtpos = string.Empty;


                var xTextPos = doc.CreateElement("TextPos");
                xTextPos.InnerText = txtpos.TrimStart(',');
                root.AppendChild(xTextPos);

                var xRoadSpeed = doc.CreateElement("max_road_speed");
                xRoadSpeed.InnerText = iMax_speed.ToString();
                root.AppendChild(xRoadSpeed);

                var xRawData = doc.CreateElement("raw_data");
                xRawData.InnerText = _strRpt;
                root.AppendChild(xRawData);


                var xmlOutput = doc.OuterXml;

                

            }
            catch (Exception e)
            {
                //FileIO fio = new FileIO();
                //fio.WriteDayLog(e.Message + " : cQL_GV300.make_GTERI(): " + _strRpt);
            }
        }

        private void makeGTIDA(string _strRpt) //driver ID is tagged
        {
            var s_info = _strRpt.Split(',');
            var rept_type = s_info[0];
            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            var ID = s_info[5];
            var ID_rept_type = s_info[6];
            gps_accuracy = (s_info[8] == "") ? 0 : int.Parse(s_info[8]);
            speed = (s_info[9] == "") ? 0.0 : double.Parse(s_info[9]);
            azimuth = (s_info[10] == "") ? 0 : int.Parse(s_info[10]);
            alt = (s_info[11] == "") ? 0.0 : double.Parse(s_info[11]);
            lng = (s_info[12] == "") ? 0.0 : double.Parse(s_info[12]);
            lat = (s_info[13] == "") ? 0.0 : double.Parse(s_info[13]);
            gps_utc_time = s_info[14];
            MCC = s_info[15];
            MNC = s_info[16];
            LAC = s_info[17];
            cellID = s_info[18];
            reserved_1 = s_info[19];
            odometer = (s_info[20] == "") ? 0.0 : double.Parse(s_info[20]);
            send_time = s_info[25];
            count_num = s_info[26].Substring(0, 4);
            tail_char = s_info[26].Substring(4, 1);

           


            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            //driver id part
            var xID = doc.CreateElement("DriverID");
            xID.InnerText = ID;
            root.AppendChild(xID);

            var xIDType = doc.CreateElement("ID_report_type");
            xIDType.InnerText = ID_rept_type;
            root.AppendChild(xIDType);

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            var xMileage = doc.CreateElement("odometer");
            xMileage.InnerText = odometer.ToString();
            root.AppendChild(xMileage);


            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;



            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);

            //add alert options
            //cAlert c = new cAlert();
            //string sOptions = c.getAlertStatPriority(unique_id, rept_type, "QL");
            //string[] arrOpts = sOptions.Split('-');

            //XmlElement xIsAlert = doc.CreateElement("IsAlert");
            //xIsAlert.InnerText = arrOpts[0];
            //xIsAlert.InnerText = xIsAlert.InnerText.TrimEnd(',');
            //if (xIsAlert.InnerText == "") xIsAlert.InnerText = "0";
            //root.AppendChild(xIsAlert);

            //XmlElement xPriority = doc.CreateElement("Priority");
            //xPriority.InnerText = arrOpts[1];
            //root.AppendChild(xPriority);

            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

            

        }

        private void makeCANMsg(string _strRpt)
        {
            string sVIN;
            string can_rept_type;
            string can_device_state;
            string can_report_mask;
            string ign_key;
            string total_distance;
            string total_fuel_used;
            string engine_rpm;
            string coolant_temp;
            string fuel_consumption;
            string fuel_level;
            string acc_pedal_pressure;
            string engine_hours;
            string total_drv_time;
            string total_idle_time;
            string idle_fuel_used;
            string axle_weight;
            string dtc;
            string lights_stat;
            string doors_stat;



            var s_info = _strRpt.Split(',');

            var rept_type = s_info[0];

            protocol_ver = s_info[1];
            unique_id = s_info[2];
            device_name = s_info[3];
            can_rept_type = s_info[4];
            can_device_state = s_info[5];
            can_report_mask = s_info[6];
            sVIN = s_info[7];
            ign_key = s_info[8];
            total_distance = s_info[9];
            total_fuel_used = s_info[10];
            engine_rpm = s_info[11];
            coolant_temp = s_info[13];
            fuel_consumption = s_info[14];
            fuel_level = s_info[15];
            acc_pedal_pressure = s_info[17];
            engine_hours = s_info[18];
            total_drv_time = s_info[19];
            total_idle_time = s_info[20];
            idle_fuel_used = s_info[21];
            axle_weight = s_info[22];
            dtc = s_info[24];
            lights_stat = s_info[25];
            doors_stat = s_info[26];

            //other info
            gps_accuracy = (s_info[29] == "") ? 0 : int.Parse(s_info[29]);
            speed = (s_info[30] == "") ? 0.0 : double.Parse(s_info[30]);
            azimuth = (s_info[31] == "") ? 0 : int.Parse(s_info[31]);
            alt = (s_info[32] == "") ? 0.0 : double.Parse(s_info[32]);
            lng = (s_info[33] == "") ? 0.0 : double.Parse(s_info[33]);
            lat = (s_info[34] == "") ? 0.0 : double.Parse(s_info[34]);
            
            try
            {
                if (s_info.Length == 36)
                {
                    gps_utc_time = s_info[s_info.Length - 1].Substring(0, 13); //2018111112102$
                    tail_char = s_info[s_info.Length - 1].Substring(13, 1);
                }
                else
                {
                    gps_utc_time = s_info[35];
                    MCC = s_info[36];
                    MNC = s_info[37];
                    LAC = s_info[38];
                    cellID = s_info[39];
                    reserved_1 = s_info[40];
                    send_time = s_info[41];
                    count_num = s_info[42].Substring(0, 4);
                    tail_char = s_info[42].Substring(4, 1);
                }
            }
            catch (Exception)
            {
                MCC = "00"; MNC = "00"; LAC = "00";cellID = "00";
                reserved_1 = "00"; send_time = "00"; count_num = "00";
                tail_char = tail_char?? "00";
            }

            //make the xml and send
            var doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            var dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);

            // Create the root element
            var root = doc.CreateElement("QL_Message_GV300");
            doc.AppendChild(root);

            var xrept_type = doc.CreateElement("rept_type");
            xrept_type.InnerText = rept_type;
            root.AppendChild(xrept_type);

            var xprotocol_ver = doc.CreateElement("protocol_ver");
            xprotocol_ver.InnerText = protocol_ver;
            root.AppendChild(xprotocol_ver);

            var xdevice_id = doc.CreateElement("device_id");
            xdevice_id.InnerText = unique_id;
            root.AppendChild(xdevice_id);

            var xdevice_name = doc.CreateElement("device_name");
            xdevice_name.InnerText = device_name;
            root.AppendChild(xdevice_name);

            //CAN info
            var xcan_rept_type = doc.CreateElement("can_rept_type");
            xcan_rept_type.InnerText = can_rept_type;
            root.AppendChild(xcan_rept_type);

            var xcan_device_state = doc.CreateElement("can_device_state");
            xcan_device_state.InnerText = can_device_state;
            root.AppendChild(xcan_device_state);

            var xcan_report_mask = doc.CreateElement("can_report_mask");
            xcan_report_mask.InnerText = can_report_mask;
            root.AppendChild(xcan_report_mask);

            var xVIN = doc.CreateElement("VIN");
            xVIN.InnerText = sVIN;
            root.AppendChild(xVIN);

            var xign_key = doc.CreateElement("ign_key");
            xign_key.InnerText = ign_key;
            root.AppendChild(xign_key);

            var xtotal_distance = doc.CreateElement("total_distance");
            xtotal_distance.InnerText = total_distance;
            root.AppendChild(xtotal_distance);

            var xtotal_fuel_used = doc.CreateElement("total_fuel_used");
            xtotal_fuel_used.InnerText = total_fuel_used;
            root.AppendChild(xtotal_fuel_used);

            var xengine_rpm = doc.CreateElement("engine_rpm");
            xengine_rpm.InnerText = engine_rpm;
            root.AppendChild(xengine_rpm);

            var xcoolant_temp = doc.CreateElement("coolant_temp");
            xcoolant_temp.InnerText = coolant_temp;
            root.AppendChild(xcoolant_temp);

            var xfuel_consumption = doc.CreateElement("fuel_consumption");
            xfuel_consumption.InnerText = fuel_consumption;
            root.AppendChild(xfuel_consumption);

            var xfuel_level = doc.CreateElement("fuel_level");
            xfuel_level.InnerText = fuel_level;
            root.AppendChild(xfuel_level);

            var xacc_pedal_pressure = doc.CreateElement("acc_pedal_pressure");
            xacc_pedal_pressure.InnerText = acc_pedal_pressure;
            root.AppendChild(xacc_pedal_pressure);

            var xengine_hours = doc.CreateElement("engine_hours");
            xengine_hours.InnerText = engine_hours;
            root.AppendChild(xengine_hours);

            var xtotal_drv_time = doc.CreateElement("total_drv_time");
            xtotal_drv_time.InnerText = total_drv_time;
            root.AppendChild(xtotal_drv_time);

            var xtotal_idle_time = doc.CreateElement("total_idle_time");
            xtotal_idle_time.InnerText = total_idle_time;
            root.AppendChild(xtotal_idle_time);

            var xidle_fuel_used = doc.CreateElement("idle_fuel_used");
            xidle_fuel_used.InnerText = idle_fuel_used;
            root.AppendChild(xidle_fuel_used);

            var xaxle_weight = doc.CreateElement("axle_weight");
            xaxle_weight.InnerText = axle_weight;
            root.AppendChild(xaxle_weight);

            var xDTC = doc.CreateElement("DTC");
            xDTC.InnerText = dtc;
            root.AppendChild(xDTC);

            var xlights_stat = doc.CreateElement("lights_status");
            xlights_stat.InnerText = lights_stat;
            root.AppendChild(xlights_stat);

            var xdoor_stat = doc.CreateElement("door_status");
            xdoor_stat.InnerText = doors_stat;
            root.AppendChild(xdoor_stat);

            //end CAN info

            var xgps_accuracy = doc.CreateElement("GPS_Accuracy");
            xgps_accuracy.InnerText = gps_accuracy.ToString();
            root.AppendChild(xgps_accuracy);

            var xspeed = doc.CreateElement("speed");
            xspeed.InnerText = speed.ToString();
            root.AppendChild(xspeed);

            var xazimuth = doc.CreateElement("azimuth");
            xazimuth.InnerText = azimuth.ToString();
            root.AppendChild(xazimuth);

            var xaltitude = doc.CreateElement("altitude");
            xaltitude.InnerText = alt.ToString();
            root.AppendChild(xaltitude);


            var xlatitude = doc.CreateElement("latitude");
            xlatitude.InnerText = lat.ToString();
            root.AppendChild(xlatitude);

            var xlongitude = doc.CreateElement("longitude");
            xlongitude.InnerText = lng.ToString();
            root.AppendChild(xlongitude);

            var xgps_time = doc.CreateElement("gps_utc_time");
            xgps_time.InnerText = gps_utc_time;
            root.AppendChild(xgps_time);

            var xMCC = doc.CreateElement("MCC");
            xMCC.InnerText = MCC;
            root.AppendChild(xMCC);

            var xMNC = doc.CreateElement("MNC");
            xMNC.InnerText = MNC;
            root.AppendChild(xMNC);

            var xLAC = doc.CreateElement("LAC");
            xLAC.InnerText = LAC;
            root.AppendChild(xLAC);

            var xCellID = doc.CreateElement("cell_id");
            xCellID.InnerText = cellID;
            root.AppendChild(xCellID);

            var xMileage = doc.CreateElement("odometer");
            xMileage.InnerText = odometer.ToString();
            root.AppendChild(xMileage);


            var xSendTime = doc.CreateElement("send_time");
            xSendTime.InnerText = send_time;
            root.AppendChild(xSendTime);

            var xCount = doc.CreateElement("count");
            xCount.InnerText = count_num;
            root.AppendChild(xCount);

            var xTailChar = doc.CreateElement("tailchar");
            xTailChar.InnerText = tail_char;
            root.AppendChild(xTailChar);

            //text position
            var txtpos = string.Empty;



            var xTextPos = doc.CreateElement("TextPos");
            xTextPos.InnerText = txtpos.TrimStart(',');
            root.AppendChild(xTextPos);


            var xRawData = doc.CreateElement("raw_data");
            xRawData.InnerText = _strRpt;
            root.AppendChild(xRawData);


            var xmlOutput = doc.OuterXml;

            



        }


        private int makebin(string hex_num)
        {
            //return the position of the high bit for a given hex number
            int i;
            var i_pos = 0;
            char[] str_pos;
            var binaryval = "";
            binaryval = Convert.ToString(Convert.ToInt32(hex_num, 16), 2);
            str_pos = binaryval.ToCharArray();
            for (i = 0; i < binaryval.Length; i++)
            {
                if (str_pos[i] == '1')
                {
                    i_pos = i;
                }
            }
            return i_pos;
        }

        private DateTime convertstrDate(string date)
        {
            //convert from string - YYYYMMDDHHMMSS to datetime
            try
            {
                string year, month, day, hour, min, sec;
                year = date.Substring(0, 4);
                month = date.Substring(4, 2);
                day = date.Substring(6, 2);
                hour = date.Substring(8, 2);
                min = date.Substring(10, 2);
                sec = date.Substring(12, 2);

                return DateTime.Parse(day + "/" + month + "/" + year + " " + hour + ":" + min + ":" + sec);
            }
            catch
            {
                return DateTime.Now;
            }
        }



    }
}
