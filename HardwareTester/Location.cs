﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{   
    public class Location
    {
        public ulong Id { get; set; }
        public int DeviceId { get; set; }
        public bool Valid { get; set; }
        public DateTime FixTime { get; set; }
        public DateTime DeviceTime { get; set; }
        public string Protocol { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public double Altitude { get; set; }
        public double Speed { get; set; }
        public double Course { get; set; }
        public string StatusCode { get; set; }
        public bool Ignition { get; set; }
        public int Event { get; set; }
        public string type { get; set; }
        public int Excempt { set; get; }
    }
}
