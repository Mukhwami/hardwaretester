﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareTester
{
    public class clsCommon
    {
        #region Properties
        public string vSequenceID { get; set; }
        public string Attributes { get; set; }
        public string Deviceid { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int Heading { get; set; }
        public int ReportID { get; set; }
        public int PrevReportID { get; set; }
        public decimal Odometer { get; set; }
        public decimal VehicleSpeed { get; set; }
        public Nullable<int> count { get; set; }
        public string GeoZoneID { get; set; }
        public string TextMessage { get; set; }
        public string ifkDriverIDTraccar { get; set; }
        public bool bIsIgnitionOn { get; set; }
        public bool SettingsHardware_UseSoftwareOdo { get; set; } = false;
        public string EventName { get; set; }
        public float FuelVoltage { get; set; }
        //public bool bIsEngineOn { get; set; }
        public Nullable<bool> bIsEngineOn { get; set; }

        public bool FromTraccar { get; set; }
        public string IsAlert { get; set; }
        public string dGPSDateTime { get; set; }
        public string cInput1 { get; set; }
        public string cInput2 { get; set; }
        public string cInput3 { get; set; }
        public string cInput4 { get; set; }
        public string cInput5 { get; set; }
        public string cInput6 { get; set; }

        public string vTempSensor1 { get; set; }
        public string vTempSensor2 { get; set; }

        public string nAltitude { get; set; }
        public int iHdop { get; set; }
        public int iBatteryBackup { get; set; }
        public Nullable<int> EventID { get; set; }
        public Nullable<bool> MotionOn { get; set; }
        public string vFuelDigital { get; set; }
        public string vAnalog1 { get; set; }
        public string vAnalog2 { get; set; }
        public string vAnalog1Raw { get; set; }
        public string vAnalog2Raw { get; set; }

        public string cOutput1 { get; set; }
        public string cOutput2 { get; set; }
        public string iSoftwarePriority { get; set; }
        public string vRoadSpeed { get; set; }

        public bool bIsGsmLocation { get; set; }

        public int iRpm { get; set; }
        public string EngineCoolantTemp { get; set; }
        public string FuelConsumption { get; set; }
        public string ThrottlePosition { get; set; }
        public string EngineLoad { get; set; }
        public string DiagnosticCodes { get; set; }
        public string raw_data { get; set; }
        public string raw_data_xml { get; set; }
        public int Client_Id { get; set; }
        public int Reseller_Id { get; set; }


        public string Cell_MMC { get; set; }
        public string Cell_MNC { get; set; }
        public string Cell_LAC { get; set; }
        public string Cell_ID { get; set; }

        public string ObdStatus { get; set; }
        public string ObdRepType { get; set; }
        public string ObdPowerVoltage { get; set; }
        public string ObdsPid { get; set; }
        public string ObdEngineRpm { get; set; }

        public string ObdVehicleSpeed { get; set; }
        public string ObdEngineCoolantTemp { get; set; }
        public string ObdFuelConsumption { get; set; }
        public string ObdDtcCleared { get; set; }
        public string ObdMileActivatedDistance { get; set; }
        public string ObdMilStatus { get; set; }
        public string ObdNumDtc { get; set; }
        public string ObdDiagCodes { get; set; }
        public string ObdEngineLoad { get; set; }
        public string ObdFuelLevel { get; set; }
        public string ObdOilTemp { get; set; }

        public string ReptType { get; set; }
        public float? Fuel1_Litres { get; set; }
        public float? Fuel1_Raw { get; set; }

        public float? Fuel2_Litres { get; set; }
        public float? Fuel2_Raw { get; set; }
        public string SensorData { get; set; }
        public bool? IgnoreGeoCode { get; set; }
        public string ObdData { set; get; }
        public string additionalEventInfo { get; set; }
        public decimal TraccarOdometer { set; get; }

        #endregion
        public clsCommon()
        {

        }
        public clsCommon(clsCommon srce)
        {
            this.Attributes = srce.Attributes;
            this.TraccarOdometer = srce.TraccarOdometer;
            this.vSequenceID = srce.vSequenceID;
            this.Deviceid = srce.Deviceid;
            this.Latitude = srce.Latitude;
            this.Longitude = srce.Longitude;
            this.FromTraccar = FromTraccar;
            this.Heading = srce.Heading;
            this.ReportID = srce.ReportID;
            this.Odometer = srce.Odometer;
            this.VehicleSpeed = srce.VehicleSpeed;
            this.GeoZoneID = srce.GeoZoneID;
            this.TextMessage = srce.TextMessage;
            this.ifkDriverIDTraccar = srce.ifkDriverIDTraccar;
            this.bIsIgnitionOn = srce.bIsIgnitionOn;
            this.EventName = srce.EventName;
            this.FuelVoltage = srce.FuelVoltage;
            this.bIsEngineOn = srce.bIsEngineOn;
            this.IsAlert = srce.IsAlert;
            this.dGPSDateTime = srce.dGPSDateTime;
            this.cInput1 = srce.cInput1;
            this.cInput2 = srce.cInput2;
            this.cInput3 = srce.cInput3;
            this.cInput4 = srce.cInput4;
            this.cInput5 = srce.cInput5;
            this.cInput6 = srce.cInput6;
            this.vTempSensor1 = srce.vTempSensor1;
            this.vTempSensor2 = srce.vTempSensor2;
            this.nAltitude = srce.nAltitude;
            this.iHdop = srce.iHdop;
            this.iBatteryBackup = srce.iBatteryBackup;
            this.vFuelDigital = srce.vFuelDigital;
            this.vAnalog1 = srce.vAnalog1;
            this.vAnalog2 = srce.vAnalog2;
            this.vAnalog1Raw = srce.vAnalog1Raw;
            this.vAnalog2Raw = srce.vAnalog2Raw;
            this.cOutput1 = srce.cOutput1;
            this.cOutput2 = srce.cOutput2;
            this.iSoftwarePriority = srce.iSoftwarePriority;
            this.vRoadSpeed = srce.vRoadSpeed;
            this.bIsGsmLocation = srce.bIsGsmLocation;
            this.iRpm = srce.iRpm;
            this.EngineCoolantTemp = srce.EngineCoolantTemp;
            this.FuelConsumption = srce.FuelConsumption;
            this.ThrottlePosition = srce.ThrottlePosition;
            this.EngineLoad = srce.EngineLoad;
            this.DiagnosticCodes = srce.DiagnosticCodes;
            this.raw_data = srce.raw_data;
            this.raw_data_xml = srce.raw_data_xml;
            this.Client_Id = srce.Client_Id;
            this.Reseller_Id = srce.Reseller_Id;
            this.Cell_MMC = srce.Cell_MMC;
            this.Cell_MNC = srce.Cell_MNC;
            this.Cell_LAC = srce.Cell_LAC;
            this.Cell_ID = srce.Cell_ID;
            this.ObdStatus = srce.ObdStatus;
            this.ObdRepType = srce.ObdRepType;
            this.ObdPowerVoltage = srce.ObdPowerVoltage;
            this.ObdsPid = srce.ObdsPid;
            this.ObdEngineRpm = srce.ObdEngineRpm;
            this.ObdVehicleSpeed = srce.ObdVehicleSpeed;
            this.ObdEngineCoolantTemp = srce.ObdEngineCoolantTemp;
            this.ObdFuelConsumption = srce.ObdFuelConsumption;
            this.ObdDtcCleared = srce.ObdDtcCleared;
            this.EventID = srce.EventID;
            this.MotionOn = srce.MotionOn;
            this.ObdMileActivatedDistance = srce.ObdMileActivatedDistance;
            this.ObdMilStatus = srce.ObdMilStatus;
            this.ObdNumDtc = srce.ObdNumDtc;
            this.ObdDiagCodes = srce.ObdDiagCodes;
            this.ObdEngineLoad = srce.ObdEngineLoad;
            this.ObdFuelLevel = srce.ObdFuelLevel;
            this.ObdOilTemp = srce.ObdOilTemp;
            this.ReptType = srce.ReptType;
            this.Fuel1_Litres = srce.Fuel1_Litres;
            this.Fuel1_Raw = srce.Fuel1_Raw;
            this.Fuel2_Litres = srce.Fuel2_Litres;
            this.Fuel2_Raw = srce.Fuel2_Raw;
            this.SensorData = srce.SensorData;
            this.IgnoreGeoCode = srce.IgnoreGeoCode;
            this.SettingsHardware_UseSoftwareOdo = srce.SettingsHardware_UseSoftwareOdo;
            this.ObdData = srce.ObdData;

        }

    }
}
